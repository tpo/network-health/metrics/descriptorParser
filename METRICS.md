# Metrics exposed to VictoriaMetrics

## Source

### From bandwidth files

`bw`
The scaled bandwidth in kilobytes per second calculated as explained at [bandwidth-file-spec](https://spec.torproject.org/bandwidth-file-spec/scaling-bandwidths.html#torflow-aggregation).

`bw_mean`
The measured mean bandwidth in bytes per second.

`bw_median`
The measured bandwidth median in bytes per second.

`consensus_bandwidth`
The consensus bandwidth in bytes per second.

`bw_file_desc_bw_avg`
The descriptor average bandwidth in bytes per second.

`bw_file_desc_bw_bur`
The descriptor bandwidth burst in bytes per second.

`bw_file_desc_bw_obs_last`
The descriptor observed bandwidth in bytes per second.

`bw_file_desc_bw_obs_mean`
The descriptor observed bandwidth mean in bytes per second.

`error_circ`
The number of times that the bandwidth measurements failed because of circuit
failures.

`error_destination`
The number of times that the bandwidth measurements failed because the web
server was not available

`error_misc`
The number of times that the bandwidth measurements failed because of other
reasons.

`error_second_relay`
The number of times that the bandwidth measurements for this relay failed
because sbws could not find a second relay for the test circuit.

`error_stream`
The number of times that the bandwidth measurements failed because of stream
failures.

`r_strm`
The stream ratio calculated as explained at [bandwidth-file-spec](https://spec.torproject.org/bandwidth-file-spec/scaling-bandwidths.html#torflow-aggregation).

`r_strm_filt`
The filtered stream ratio calculated as explained at [bandwidth-file-spec](https://spec.torproject.org/bandwidth-file-spec/scaling-bandwidths.html#torflow-aggregation).

### From bridge network statuses

`desc_dirreq_v3_status`
Estimated number of intervals when the node was listed as running in the network
status published by either the directory authorities or bridge authority.

`bridge_bandwidth`
The measured bandwidth in bytes per second.

### From consensuses

`relay_bandwidth`
The measured bandwidth in bytes per second

`desc_status`
Estimated number of intervals when the node was listed as running in the network
status published by either the directory authorities or bridge authority.

`network_weight`
Metric of a relay that is based on bandwidth observed by the relay and bandwidth
measured by the directory authorities.

`network_fraction`
Metric of a relay that is based on bandwidth observed by the relay and bandwidth
measured by the directory authorities as a fraction of the total network weight.

`total_network_weight`
Metric based on bandwidth observed by relays and bandwidth measured by the
directory authorities.

`network_guard_weight`
Metric of a guard relay that is based on bandwidth observed by the relay and
bandwidth measured by the directory authorities.

`network_guard_fraction`
Metric of a guard relay that is based on bandwidth observed by the relay and
bandwidth measured by the directory authorities as a fraction of the total
network weight.

`total_network_guard_weight`
Metric based on bandwidth observed by guard relays and bandwidth measured by the
directory authorities.

`network_middle_weight`
Metric of a middle relay that is based on bandwidth observed by the relay and
bandwidth measured by the directory authorities.

`network_middle_fraction`
Metric of a middle relay that is based on bandwidth observed by the relay and
bandwidth measured by the directory authorities as a fraction of the total
network weight.

`total_network_middle_weight`
Metric based on bandwidth observed by middle relays and bandwidth measured by
the directory authorities.

`network_exit_weight`
Metric of a exit relay that is based on bandwidth observed by the relay and
bandwidth measured by the directory authorities.

`network_exit_fraction`
Metric of a exit relay that is based on bandwidth observed by the relay and
bandwidth measured by the directory authorities as a fraction of the total
network weight.

`total_network_exit_weight`
Metric based on bandwidth observed by exit relays and bandwidth measured by the
directory authorities.

## From extra-info descriptors

`read_bandwidth_history`
How much bandwidth the OR has used recently. Usage is divided into intervals of
NSEC seconds.

`write_bandwidth_history`
How much bandwidth the OR has used recently. Usage is divided into intervals of
NSEC seconds.

`ipv6_read_bandwidth_history`
How much bandwidth the OR has used recently on ipv6 connections. Usage is
divided into intervals of NSEC seconds.

`ipv6_write_bandwidth_history`
How much bandwidth the OR has used recently on ipv6 connections. Usage is
divided into intervals of NSEC seconds.

`dirreq_read_bandwidth_history`
Declare how much bandwidth the OR has spent on answering directory requests.
Usage is divided into intervals of NSEC seconds.

`dirreq_write_bandwidth_history`
Declare how much bandwidth the OR has spent on answering directory requests.
Usage is divided into intervals of NSEC seconds.

`desc_relay_dirreq_v3_responses`
Estimated number of v3 network status consensus requests that the node responded to.

`desc_bridge_dirreq_v3_responses`
Estimated number of v3 network status consensus requests that the node responded to.

### From server descriptors

`desc_bw_avg`
Estimated average bandwidth the the OR is willing to sustain over long period.
In bytes per second.

`desc_bw_bur`
Estimated average bandwidth the the OR is willing to sustain over short
intervals. In bytes per second.

`desc_bw_obs`
Estimated bandwidth capacity this relay can handle. In bytes per second.

`desc_bw_adv`
Estimated advertised bandwidth this relay can handle. In bytes per second.

### From web logs
`http_requests_total`
Total http requests

## How does it work?
There are a number of moving parts involved when exposing various metrics to VictoriaMetrics and using that to create visualizations, e.g. in Grafana. There are parts related to Tor and parts related to Grafana and it is not obvious to see how they work together.

For instance, if the idea were to count the amount of relays with a written bandwidth history above a threshold at particular points in time, how would that work? On the Tor side we'd have to deal with relays publishing bandwidth statistics at different times, each relay potentially covering different collection periods (although their lengths are currently the same, 24h). On the Grafana side we have a great flexibility to visualize our [time series]( https://grafana.com/docs/grafana/latest/fundamentals/timeseries/) allowing us to cover the last N minutes/hours/days etc. and having steps for the intended interval between data points. While we save the amount of written bytes per second and the end of the respective collection period (see: our [extra-info descriptor parser](https://gitlab.torproject.org/tpo/network-health/metrics/descriptorParser/-/blob/ca4580c3e9ea88810a7959f23d926dc685a3191f/src/main/java/org/torproject/metrics/descriptorparser/parsers/ExtraInfoDescriptorParser.java#L674), how does the visualization side look like, e.g. between 2024-10-02T00:00:00.000Z (the start date), 2024-10-05T00:00:00.000Z (the end date) and a 24h step? Given the diversity of bandwidth statistics publication times it is unlikely that many relays (if any at all) have a data point at exactly 2024-10-02T00:00:00.000Z. If not then VictoriaMetrics takes the closest data point in the lookbehind window providing an ephemeral data point (for those concepts see the [VictoriaMetrics documentation](https://docs.victoriametrics.com/keyconcepts/#query-data)). One gets ephemeral data points, too, in case a raw value is missing or the step part is lower than the actual inverval between data points.

Thus, one needs to be aware of the purpose for which VictoriaMetrics data is used. If it's crucial to work just with raw data points (instead of ephemeral ones) our Metrics database is the database to consult instead.

In order to help with creating the visualizations in Grafana it can be useful as well to query VictoriaMetrics directly, e.g. to double-check the query (result) on the command line. This can be done with `curl`, e.g. like:
```
  curl -u un:pw -G --data-urlencode "query=write_bandwidth_history" \
                   --data-urlencode "start=-7d" --data-urlencode "end=-1d" \
                   --data-urlencode "step=24h" \
                   "https://metrics-db.torproject.org/prometheus/api/v1/query_range"
```
