package org.torproject.metrics.descriptorparser.parsers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.torproject.metrics.descriptorparser.utils.PsqlConnector;

import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class BandwidthParserTest {

  @Test()
  public void testBandwidthParserDbUploader() throws Exception {
    BandwidthParser bp = new BandwidthParser();
    String bandwidthPath =
        "src/test/resources/2022-08-29-10-bandwidth";
    String confFile = "src/test/resources/config.properties.test";
    String bandwidthFileDigest = "5RfCCiDr0gekaJ7yxYF8Y4s29ZYThbjnhF2idjq2YL4";

    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    bp.run(bandwidthPath, conn);

    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM bandwidth_file WHERE digest='"
        + bandwidthFileDigest + "';");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("digest"), bandwidthFileDigest);
        assertEquals(rs.getString("destination_countries"), "DE");
        assertEquals(rs.getInt("mu"), 1143631);
        assertEquals(rs.getInt("muf"), 1776550);
      } else {
        fail("Descriptor not found");
      }
    }

    bandwidthPath = "src/test/resources/2023-11-30-bandwidth";
    bandwidthFileDigest = "ssG7Ea8X1iJXZKtfr4KojoZx5bUWSoxfp0zrO0ucevA";
    bp.run(bandwidthPath, conn);

    preparedStatement = conn.prepareStatement(
      "SELECT * FROM bandwidth_file WHERE digest='"
      + bandwidthFileDigest + "';");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("digest"), bandwidthFileDigest);
        assertEquals(rs.getString("dirauth_nickname"), "longclaw");
      } else {
        fail("Descriptor not found");
      }
    }

    String recordDigest = "RbmmUyvhLUEbiaTFI9h/xCaO963Mb1/tfkuLUDKdpLg";

    PreparedStatement preparedStatementRecord = conn.prepareStatement(
        "SELECT * FROM bandwidth_record WHERE digest='"
        + recordDigest + "';");
    try (ResultSet rsBw = preparedStatementRecord.executeQuery()) {
      if (rsBw.next()) {
        assertEquals(rsBw.getString("nick"), "harutohirrii1998");
        assertEquals(rsBw.getString("node_id"),
            "D99FAB04B17AC3057950EC9AD85D8F8ED1BE61F0");
        assertEquals(String.valueOf(rsBw.getDouble("r_strm")), "0.01");
        assertEquals(String.valueOf(rsBw.getDouble("r_strm_filt")), "0.01");
      } else {
        fail("Descriptor not found");
      }
    }

  }

}
