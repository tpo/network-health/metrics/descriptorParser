package org.torproject.metrics.descriptorparser.parsers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.torproject.metrics.descriptorparser.utils.PsqlConnector;

import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ExitListParserTest {

  @Test()
  public void testExitListParserDbUploader() throws Exception {
    ExitListParser bp = new ExitListParser();
    String exitListPath =
        "src/test/resources/2022-08-30-tordnsel";
    String confFile = "src/test/resources/config.properties.test";
    String exitListDigest = "nDA8EGF2TB/qKArzuhQKmUi0xO3PTA3DgtNCaDzYcIc";

    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    bp.run(exitListPath, conn);

    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM exit_list WHERE digest = '"
        + exitListDigest + "'");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("digest"), exitListDigest);
        assertEquals(rs.getString("header"),
            "@type tordnsel 1.0");
      } else {
        fail("Descriptor not found");
      }
    }
  }

}
