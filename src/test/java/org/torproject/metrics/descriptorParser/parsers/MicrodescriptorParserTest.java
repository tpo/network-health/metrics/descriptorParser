package org.torproject.metrics.descriptorparser.parsers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.torproject.metrics.descriptorparser.utils.PsqlConnector;

import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class MicrodescriptorParserTest {
  /**
   * Insert microdescriptor data into the database before doing tests.
   */
  @Before()
  public void insertMicrodescriptors() throws Exception {
    String confFile = "src/test/resources/config.properties.test";

    PsqlConnector psqlConn = new PsqlConnector();
    Connection conn = psqlConn.connect(confFile);
    String microPath = "src/test/resources/microdescriptors";
    MicrodescriptorParser mp = new MicrodescriptorParser();
    mp.run(microPath, conn);
  }

  @Test()
  public void testMicrodescriptorParserDbUploader() throws Exception {
    String confFile = "src/test/resources/config.properties.test";

    PsqlConnector psqlConn = new PsqlConnector();
    Connection conn = psqlConn.connect(confFile);

    String idEd25519 = "gzh2zfVYFrhRTZCQ9EdaGlYG34w/vc1hsqHJnBpiJnE";

    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM microdescriptor WHERE ed25519_identity = '"
        + idEd25519 + "'");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("ed25519_identity"), idEd25519);
        assertEquals(rs.getString("ntor_onion_key"),
            "C4o8JnCmw7pmGXtNekS+3OuifSjQNi0XfbJkj1ckmSk");
      } else {
        fail("Descriptor not found");
      }
    }
  }
}
