package org.torproject.metrics.descriptorparser.parsers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.torproject.metrics.descriptorparser.utils.PsqlConnector;

import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class BridgePoolAssignmentsParserTest {

  /**
   * Insert data in the DB to be able to run tests.
   */
  @Before
  public void addAssignmentsToDb() throws Exception {
    BridgePoolAssignmentsParser bp = new BridgePoolAssignmentsParser();
    String bridgePoolAssignmentsPath =
        "src/test/resources/2022-08-29-bridge-pool-assignment";
    String bridgePoolAssignmentsV11 =
        "src/test/resources/2024-04-23-bridge-pool-assignment";
    String confFile = "src/test/resources/config.properties.test";

    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    bp.run(bridgePoolAssignmentsPath, conn);
    bp.run(bridgePoolAssignmentsV11, conn);
  }

  @Test()
  public void testBridgePoolAssignmentsParserDbUploader() throws Exception {
    String confFile = "src/test/resources/config.properties.test";

    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    String bridgePoolDigest = "9hxKhoktOK8F4GsCo6+jkug/y1HudnNI6FCKXaKrLmU";

    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM bridge_pool_assignments_file WHERE digest = '"
        + bridgePoolDigest + "'");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("digest"), bridgePoolDigest);
        assertEquals(rs.getString("header"),
            "@type bridge-pool-assignment 1.1");
      } else {
        fail("Descriptor not found");
      }
    }
  }

  @Test()
  public void testAssignmentRecord() throws Exception {
    String confFile = "src/test/resources/config.properties.test";

    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    String bridgePoolDigest = "ePiJ98cdYqvYf0yGGMK0HVeRl8X52EZwEBfvcO5g1sw";

    PreparedStatement preparedStatement = conn.prepareStatement(
            "SELECT * FROM bridge_pool_assignment WHERE digest = '"
                    + bridgePoolDigest + "'");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("digest"), bridgePoolDigest);
        assertEquals(rs.getString("distribution_method"), "settings");
        assertEquals(rs.getString("blocklist"), null);
        assertEquals(rs.getString("ip"), "4");
        assertEquals(rs.getString("transport"), "obfs4");
        assertEquals(rs.getBoolean("distributed"), true);
        assertEquals(rs.getString("state"), "functional");
        assertEquals(rs.getString("bandwidth"), "untested");
        assertEquals(rs.getFloat("ratio"), 0f, 0.0f);
      } else {
        fail("Descriptor not found");
      }
    }

  }

}
