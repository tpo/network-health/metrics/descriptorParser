package org.torproject.metrics.descriptorparser.builders;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import org.torproject.metrics.descriptorparser.parsers.BridgeNetworkStatusParser;
import org.torproject.metrics.descriptorparser.parsers.ConsensusParser;
import org.torproject.metrics.descriptorparser.parsers.ExtraInfoDescriptorParser;
import org.torproject.metrics.descriptorparser.parsers.ServerDescriptorParser;
import org.torproject.metrics.descriptorparser.utils.PsqlConnector;

import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public class RouterStatusBuilderTest {
  /**
   * Insert server statuses data into the database before doing tests.
   */
  @Before()
  public void insertServerStatuses() throws Exception {
    String confFile = "src/test/resources/config.properties.test";

    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    ConsensusParser cp = new ConsensusParser();
    String consensusPathGeneralOverloaded =
        "src/test/resources/2022-08-31-consensus";
    String consensusPathOverloaded =
        "src/test/resources/2023-09-07-consensus";
    String consensusPath =
        "src/test/resources/2022-08-30-consensus";
    cp.run(consensusPath, conn);
    cp.run(consensusPathGeneralOverloaded, conn);
    cp.run(consensusPathOverloaded, conn);

    String bridgeStatusesPath =
        "src/test/resources/2022-08-30-bridges-statuses";
    BridgeNetworkStatusParser bp = new BridgeNetworkStatusParser();
    bp.run(bridgeStatusesPath, conn);

    ServerDescriptorParser sp = new ServerDescriptorParser();
    String serverPathOverloaded =
        "src/test/resources/2023-09-07-server-descriptors";
    String serverDescPath =
        "src/test/resources/2022-08-30-server-descriptors";
    String serverBridgePath =
        "src/test/resources/2022-08-30-bridges-server-desc";
    sp.run(serverPathOverloaded, conn);
    sp.run(serverBridgePath, conn);
    sp.run(serverDescPath, conn);

    ExtraInfoDescriptorParser ep = new ExtraInfoDescriptorParser();
    String extraInfoPathGeneralOverloaded =
        "src/test/resources/2022-08-30-extra-infos";
    String extraInfoPath =
        "src/test/resources/2023-09-07-extra-infos";
    String extrainfoBridgePath =
        "src/test/resources/bridge-extra-info";
    ep.run(extraInfoPathGeneralOverloaded, conn);
    ep.run(extraInfoPath, conn);
    ep.run(extrainfoBridgePath, conn);

    RouterStatusBuilder statusBuilder = new RouterStatusBuilder();
    long millis = 1661846400000L;
    statusBuilder.run(
        consensusPath, millis, conn);
    long generalOverloadedMillis = 1661947500000L;
    statusBuilder.run(
        consensusPathGeneralOverloaded, generalOverloadedMillis, conn);
    long overloadedMillis = 1694095200000L;
    statusBuilder.run(consensusPathOverloaded, overloadedMillis, conn);
    long bridgesMillis = 1661892889000L;
    statusBuilder.run(bridgeStatusesPath, bridgesMillis, conn);

  }

  @Test()
  public void testRouterStatusBuilder() throws Exception {
    String confFile = "src/test/resources/config.properties.test";
    String fingerprint = "2A82DD0F42F479F2F55006D3C7AA052A84071ECB";
    String sqlQuery = String.format(
        "SELECT * FROM server_status WHERE fingerprint='%s'", fingerprint);
    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    PreparedStatement preparedStatement = conn.prepareStatement(sqlQuery);

    try (ResultSet rs = preparedStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("nickname"), "absturztaubetor");
        assertEquals(rs.getString("fingerprint"),
            fingerprint);
        assertEquals(rs.getTimestamp("published"),
            Timestamp.valueOf("2023-09-07 14:00:00.0"));
        String orAddresses = "[]";
        String orAddressesDb = rs.getString("or_addresses");
        assertEquals(orAddressesDb, orAddresses);
        String address = "81.201.202.101";
        String addressDb = rs.getString("address");
        assertEquals(address, addressDb);
        String diffOrAddresses = rs.getString("diff_or_addresses");
        assertEquals(diffOrAddresses, null);
        assertEquals(rs.getString("unreachable_or_addresses"), "");
        assertEquals(rs.getBoolean("running"), true);
        assertEquals(rs.getTimestamp("last_restarted"),
            Timestamp.valueOf("2023-09-07 13:06:20.0"));
        assertEquals(rs.getTimestamp("last_seen"),
            Timestamp.valueOf("2023-09-07 14:00:00.0"));
        assertEquals(rs.getTimestamp("last_changed_address_or_port"),
            Timestamp.valueOf("2023-09-07 14:00:00.0"));
      } else {
        fail("Status not found");
      }
    }

  }

  @Test()
  public void testInsertingStatusesAtDifferentTime() throws Exception {
    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    String confFile = "src/test/resources/config.properties.test";
    conn = psqlConn.connect(confFile);

    String fingerprint = "22F74E176F803499D4F80D9CE7D325883A8C0E45";
    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM server_status WHERE fingerprint = '"
        + fingerprint + "' AND published<='2023-09-07 14:00:00' ORDER BY"
        + " published DESC LIMIT 1;");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("nickname"), "MakeSecure");
        assertEquals(rs.getString("fingerprint"),
            fingerprint);
        assertEquals(rs.getTimestamp("last_seen"),
            Timestamp.valueOf("2023-09-07 14:00:00"));
      } else {
        fail("Status not found");
      }
    }

  }

  @Test()
  public void testInsertingBridgesStatuses() throws Exception {
    String confFile = "src/test/resources/config.properties.test";
    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    String fingerprint = "00B5DE0522A6F215BF694DE36576AF1A3927D6D8";
    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM server_status WHERE fingerprint='"
        + fingerprint + "' AND published<='2022-08-30 23:54:51' ORDER BY"
        + " published DESC LIMIT 1;");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("nickname"), "wolf359");
        assertEquals(rs.getString("fingerprint"),
            fingerprint);
        assertEquals(rs.getTimestamp("last_seen"),
            Timestamp.valueOf("2022-08-30 13:54:07"));
      } else {
        fail("Status not found");
      }
    }
  }

  @Test()
  public void testRouterFamily() throws Exception {
    String confFile = "src/test/resources/config.properties.test";
    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    String fingerprint = "E42693F28ECCB5AFBC290371DF275C6FD0657F78";

    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM server_status WHERE fingerprint = '"
        + fingerprint + "' ORDER BY published DESC LIMIT 1;");
    try (ResultSet rs = preparedStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("fingerprint"), fingerprint);
        assertEquals(rs.getString("nickname"), "amatista233");
        String family = "\"[\\\"$043BC8636292695FF268455FDDE068D510A4E6EA\\\","
            + "\\\"$18EC7D1CBB8A22AED6A7408FDEDF985AC099C9D4\\\","
            + "\\\"$1F9BEE841B906441D255FE43EA4E1156F0E61188\\\","
            + "\\\"$42E360F5FCA139ADD7C01BC706D29FE617026F74\\\","
            + "\\\"$46D5AA822C1487262387B87348C3EAE6F940BEC9\\\","
            + "\\\"$580A37D555E016CD14AB8C471E6E446FEBC3D804\\\","
            + "\\\"$6AF21ACE616EC712110388B2134CC1A955180227\\\","
            + "\\\"$6DA9ECDE07B779B325A5A7275F8038E8F72A083F\\\","
            + "\\\"$6EECA1B9DD50FC69FC6432A1F7A2AD26C1212893\\\","
            + "\\\"$7282D0E6FB01E2B4BE550CB4EA433AE16DB65C17\\\","
            + "\\\"$794E6B42020F6F26286DC41B33A90BA1ED0DA710\\\","
            + "\\\"$7BD0DE299AFDF4DD8E1A82472FBC6F8015228B0B\\\","
            + "\\\"$7EECF9ED6303D79871C05669646922E16B23C090\\\","
            + "\\\"$95F9DD417C8A4FA717FC7D370389C345BF12A2EA\\\","
            + "\\\"$A227942C424EC58CBCB63580541EF764A8460B03\\\","
            + "\\\"$A44CA851FF0B01D8B76856C3A5140426E7CC5024\\\","
            + "\\\"$A4E0F37BAC3E38F503B42BC19B3C0BFADF8248DD\\\","
            + "\\\"$D2A06E75132502D4C318BA9F6A44099F485FD4EC\\\","
            + "\\\"$D59B3934082BAE3A653697094E80AA295E733952\\\","
            + "\\\"$DBBA116A7B97E58D8837D467F818270B1503CADC\\\","
            + "\\\"$E42693F28ECCB5AFBC290371DF275C6FD0657F78\\\","
            + "\\\"$E5F513E15CC2CD35DFC4ECEAD190D3B38DB61B9E\\\","
            + "\\\"$E92EAB61F50B40D935EB943910700E4C44DCA36E\\\","
            + "\\\"$EAF570DAEFA4CFEE04FE9AE3747CC9AC70B23A59\\\","
            + "\\\"$EEB1C2EC72096E79F5583B74074A16D14D5CD10B\\\","
            + "\\\"$FBC2D4967A73C8962ED7769F4F421C5D3EC9B594\\\"]\"";

        String effectiveFamily = "\"A4E0F37BAC3E38F503B42BC19B3C0BFADF8248DD "
            + "E42693F28ECCB5AFBC290371DF275C6FD0657F78\"";
        assertEquals(rs.getString("declared_family"), family);
        assertEquals(rs.getString("effective_family"), effectiveFamily);

        String familyDigest = "asEEWRekGZpmkT4RDAflFbkfcTnjaDWxtcb47jpZ9J4";
        assertEquals(rs.getString("family_digest"), familyDigest);
      } else {
        fail("Status not found");
      }
    }
  }

  @Test()
  public void testExitPolicy() throws Exception {
    String confFile = "src/test/resources/config.properties.test";
    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    String fingerprint = "E42693F28ECCB5AFBC290371DF275C6FD0657F78";

    PreparedStatement preparedStatement = conn.prepareStatement(
            "SELECT * FROM server_status WHERE fingerprint = '"
                    + fingerprint + "' ORDER BY published DESC LIMIT 1;");
    try (ResultSet rs = preparedStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("fingerprint"), fingerprint);
        assertEquals(rs.getString("nickname"), "amatista233");
        assertEquals(rs.getString("exit_policy"), "\"[\\\"reject *:*\\\"]\"");
      } else {
        fail("Status not found");
      }
    }
  }

  @Test()
  public void testGeneralOverloadedRouter() throws Exception {
    String confFile = "src/test/resources/config.properties.test";
    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);
    String fingerprint = "E11480F37550E11027718EE9FCADCDAD0B91C8BC";
    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM server_status WHERE fingerprint = '"
        + fingerprint + "';");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("fingerprint"), fingerprint);
        assertEquals(rs.getInt("overload_general_version"), 1);
        assertEquals(rs.getTimestamp("overload_general_timestamp"),
            Timestamp.valueOf("2023-09-07 09:00:00"));
      } else {
        fail("Status not found");
      }
    }
  }

  @Test()
  public void testOverloadedPropertiesRouter() throws Exception {
    String confFile = "src/test/resources/config.properties.test";
    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    String fingerprint = "7DFC512A8CD8A86F32EDC17FBF3D0379DE4193B1";
    PreparedStatement preparedStatement = conn.prepareStatement(
            "SELECT * FROM server_status WHERE fingerprint = '"
                    + fingerprint + "' LIMIT 1;");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("fingerprint"), fingerprint);
        assertEquals(rs.getInt("overload_ratelimits_version"), 1);
        assertEquals(rs.getTimestamp("overload_ratelimits_timestamp"),
                Timestamp.valueOf("2023-09-07 12:00:00"));
        assertEquals(rs.getInt("overload_ratelimits_ratelimit"), 2662400);
        assertEquals(rs.getInt("overload_ratelimits_burstlimit"), 2662400);
        assertEquals(rs.getInt("overload_ratelimits_read_count"), 170);
        assertEquals(rs.getInt("overload_ratelimits_write_count"), 133);
        assertFalse(rs.getBoolean("is_stale"));
      } else {
        fail("Status not found");
      }
    }
    PreparedStatement preparedExStatement = conn.prepareStatement(
        "SELECT * FROM extra_info_bandwidth_history "
        + "WHERE fingerprint = '" + fingerprint + "' LIMIT 1;");

    try (ResultSet rs = preparedExStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("fingerprint"), fingerprint);
        assertEquals(String.valueOf(rs.getDouble("write_bandwidth_value")),
            String.valueOf(759427.0));
      } else {
        fail("Bw not found");
      }
    }
  }

  @Test
  public void testLastChangedAddressOrPort() throws Exception {
    String confFile = "src/test/resources/config.properties.test";
    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    String fingerprint = "000A10D43011EA4928A35F610405F92B4433B4DC";
    PreparedStatement preparedStatement = conn.prepareStatement(
            "SELECT * FROM server_status WHERE fingerprint = '"
                    + fingerprint + "' LIMIT 1;");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("fingerprint"), fingerprint);
        assertEquals(rs.getTimestamp("last_changed_address_or_port"),
            Timestamp.valueOf("2023-09-07 14:00:00.0"));

      } else {
        fail("Status not found");
      }
    }
  }

}
