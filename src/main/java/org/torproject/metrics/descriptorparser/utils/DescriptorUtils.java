package org.torproject.metrics.descriptorparser.utils;

import org.torproject.descriptor.DescriptorParseException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

public class DescriptorUtils {
  private static ObjectMapper objectMapper = new ObjectMapper();

  private static final Logger logger = LoggerFactory.getLogger(
      DateTimeHelper.class);

  /**
   * Return a string representation of an object.
   */
  public String fieldAsString(Object field) {
    String out = "";
    try {
      out = this.objectMapper.writeValueAsString(field);
    } catch (JsonProcessingException e) {
      logger.warn("Exception. {}".format(e.getMessage()));
    }
    return out;
  }

  /**
   * Convert a list to array object.
   */
  public Object[] listToArray(List listArray) {
    if (listArray == null) {
      listArray = new ArrayList();
    }
    return listArray.toArray();
  }

  /**
   * Calculate a SHA-256 digest of a descriptor and return its representation
   * in Base64.
   */
  public String calculateDigestSha256Base64(byte[] rawDescriptorBytes)
       throws DescriptorParseException {

    String digestSha256Base64 = null;
    digestSha256Base64 = Base64.encodeBase64String(
        messageDigest("SHA-256", rawDescriptorBytes))
        .replaceAll("=", "");

    if (null == digestSha256Base64) {
      throw new DescriptorParseException("Could not calculate descriptor "
          + "digest.");
    }
    return digestSha256Base64;
  }

  /**
   * Calculate descriptor digest sha1 from bytes.
   */
  public String calculateDescDigestSha1(String hexString) {
    byte[] sha1Hash = null;
    try {
      byte[] originalBytes = Hex.decodeHex(hexString);
      sha1Hash = messageDigest("SHA-1", originalBytes);

    } catch (Exception ex) {
      logger.warn("Exception: {}", ex.getMessage());
    }

    // Return the hex-encoded SHA-1 hash
    if (sha1Hash != null) {
      return Hex.encodeHexString(sha1Hash);
    } else {
      return null;
    }
  }

  /**
   * Helper function computing the message digest.
   */
  private byte[] messageDigest(String alg, byte[] rawDescriptorBytes) {
    try {
      MessageDigest md = MessageDigest.getInstance(alg);
      md.update(rawDescriptorBytes);
      return md.digest();
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Parse a date and time string and return a time object.
   */
  private static long parseDatetimeString(String datetimeString) {
    SimpleDateFormat dateTimeFormat = new SimpleDateFormat(
        "yyyy-MM-dd HH:mm:ss");
    long timeInterval = 0L;
    try {
      timeInterval = dateTimeFormat.parse(datetimeString).getTime();
    } catch (ParseException e) {
      logger.debug("Bandwidth history line does not have valid interval "
          + "length '{}'. Ignoring this line.", datetimeString);
    }
    return timeInterval;
  }

  /**
   * Check that the considered time interval is valid.
   */
  private static boolean checkInvalidTimeIntervalLength(
      long published, long intervalEnd) {
    SimpleDateFormat dateTimeFormat = new SimpleDateFormat(
        "yyyy-MM-dd HH:mm:ss");
    if (Math.abs(published - intervalEnd)
        > 7L * 24L * 60L * 60L * 1000L) {
      String intervalEndTime = dateTimeFormat.format(intervalEnd);
      String publishedTime = dateTimeFormat.format(published);
      logger.debug("Extra-info descriptor publication time {} and last "
          + "interval time {} in line differ by more than 7 days! Not "
          + "adding this line!", publishedTime, intervalEndTime);
      return true;
    }
    return false;
  }

  /**
   * Parse an interval length String in seconds and return its
   * value as numerical long type in milliseconds.
   */
  private static long parseIntervalString(String timeInterval) {
    long intervalLength = 0L;
    try {
      /* Parse intervalLength and convert it to millis */
      intervalLength = Long.parseLong(timeInterval) * 1000;
    } catch (NumberFormatException e) {
      logger.debug("Bandwidth history line does not have valid interval "
          + "length '{}'. Ignoring this line.", timeInterval);
    }
    return intervalLength;
  }

  /**
   * Return a bandwidth value in bytes/s.
   */
  private double extractBandwidthValue(long intervalLength, String value) {
    double bandwidthValue = 0;
    try {
      bandwidthValue = Double.parseDouble(value) * 1000 / intervalLength;
    } catch (NumberFormatException e) {
      logger.debug("Number format exception while parsing "
          + "bandwidth history line. Ignoring this line.");
      bandwidthValue = -1;
    }
    return bandwidthValue;
  }

  /**
   * Compute bandwidth values and interval ends.
   */
  public SortedMap<Long, Double> computeBandwidthValues(
      String bandwidthHistory) {
    String[] parts = bandwidthHistory.split(" ");
    if (parts.length != 6) {
      logger.debug("Bandwidth history line does not have expected "
          + "number of elements. Ignoring this line.");
      return null;
    }
    SortedMap<Long, Double> bandwidthValues = new TreeMap<>();
    Long intervalLength = this.parseIntervalString(parts[3].substring(1));
    if (intervalLength == 0L) {
      return null;
    }
    String intervalEndTimeString = parts[1] + " " + parts[2];
    Long intervalEnd = this.parseDatetimeString(intervalEndTimeString);
    if (intervalEnd == 0L) {
      return null;
    }

    long currentIntervalEnd = intervalEnd;
    String[] values = parts[5].split(",");
    for (int i = values.length - 1; i > -1; i--) {
      double bandwidthValue = this.extractBandwidthValue(intervalLength,
          values[i]);
      if (bandwidthValue == -1) {
        return bandwidthValues;
      } else {
        bandwidthValues.put(currentIntervalEnd, bandwidthValue);
      }
      currentIntervalEnd -= intervalLength;
    }
    if (this.checkInvalidTimeIntervalLength(intervalEnd, currentIntervalEnd)) {
      return null;
    }

    return bandwidthValues;
  }
}
