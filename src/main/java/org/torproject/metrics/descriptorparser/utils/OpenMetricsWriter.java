package org.torproject.metrics.descriptorparser.utils;

import org.torproject.metrics.descriptorparser.utils.Gauge;
import org.torproject.metrics.descriptorparser.utils.VictoriaMetricsHttpConnectionFactory;

import io.prometheus.client.CollectorRegistry;

import io.prometheus.client.exporter.PushGateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class OpenMetricsWriter {
  private static final Logger logger = LoggerFactory.getLogger(
      OpenMetricsWriter.class);

  /**
   * Process various types of metrics with flexible labeling.
   * This unified method can handle any number of labels and
   * replaces multiple specific functions.
   *
   * @param gauge The Gauge to which metrics will be recorded.
   * @param timestamp The timestamp for when the metric value is set.
   * @param value The value of the metric.
   * @param labels Varargs string parameters representing labels.
   */
  public void processMetrics(Gauge gauge, Long timestamp,
      double value, String... labels) {
    try {
      boolean allNull = true;

      // Check each label and replace null with an empty string
      for (int i = 0; i < labels.length; i++) {
        if (labels[i] == null) {
          labels[i] = "";
        } else {
          allNull = false;
        }
      }

      // Create a child instance for detailed metric management, if needed.
      Gauge.Child child = new Gauge.Child();

      gauge.labels(labels);

      child.setTimestampMs(timestamp);
      child.set(value);
      gauge.setChild(child, labels);

    } catch (Exception ex) {
      logger.warn("Exception while processing metrics: {}", ex.getMessage());
    }
  }

  /**
   * Push metrics to a push gateway.
   */
  public void pushToGateway(CollectorRegistry registry) {
    PushGateway pushgateway
          = new PushGateway("localhost:8428/api/v1/import/prometheus");
    /* It is possible to add a http auth layer as:
     * pushgateway.setConnectionFactory(
         new BasicAuthHttpConnectionFactory("my_user", "my_password"));
     */
    try {
      pushgateway.setConnectionFactory(
          new VictoriaMetricsHttpConnectionFactory());
      pushgateway.pushAdd(registry, "");
    } catch (IOException ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }

  }

}
