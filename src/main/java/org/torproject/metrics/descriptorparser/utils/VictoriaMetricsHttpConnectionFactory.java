package org.torproject.metrics.descriptorparser.utils;

import io.prometheus.client.exporter.HttpConnectionFactory;

import java.io.IOException;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Redefine the connection to prometheus because we are using VictoriaMetrics
 * so the URL strcture is hijacked.
 */
public class VictoriaMetricsHttpConnectionFactory implements
    HttpConnectionFactory {
  @Override
  public HttpURLConnection create(String url) throws IOException {
    // Here we change the endpoint url since prometheus java client adds
    // the /metrics/job/ path by default
    String vmUrl = url.replace("/metrics/job/","");
    HttpURLConnection connection = (HttpURLConnection)
        new URL(vmUrl).openConnection();
    // add any connection preparation logic you need

    return connection;
  }
}
