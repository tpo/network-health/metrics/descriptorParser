package org.torproject.metrics.descriptorparser;

import org.torproject.metrics.descriptorparser.builders.RouterFamilyBuilder;
import org.torproject.metrics.descriptorparser.builders.RouterStatusBuilder;
import org.torproject.metrics.descriptorparser.parsers.BandwidthParser;
import org.torproject.metrics.descriptorparser.parsers.BridgeNetworkStatusParser;
import org.torproject.metrics.descriptorparser.parsers.BridgePoolAssignmentsParser;
import org.torproject.metrics.descriptorparser.parsers.BridgedbMetricsParser;
import org.torproject.metrics.descriptorparser.parsers.BridgestrapParser;
import org.torproject.metrics.descriptorparser.parsers.ConsensusParser;
import org.torproject.metrics.descriptorparser.parsers.ExitListParser;
import org.torproject.metrics.descriptorparser.parsers.ExtraInfoDescriptorParser;
import org.torproject.metrics.descriptorparser.parsers.MicrodescriptorParser;
import org.torproject.metrics.descriptorparser.parsers.ServerDescriptorParser;
import org.torproject.metrics.descriptorparser.parsers.VoteParser;
import org.torproject.metrics.descriptorparser.parsers.WebStatsParser;
import org.torproject.metrics.descriptorparser.utils.PsqlConnector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class Main implements Runnable {

  private static final Logger logger = LoggerFactory.getLogger(
      Main.class);

  private Main() {}

  public static void main(String[] args) {
    Main main = new Main();
    main.exec();
  }

  private void exec() {
    this.run();
  }

  private static final String CONF_FILE = "config.properties";

  @Override
  public void run() {
    PsqlConnector psqlConn = new PsqlConnector();
    Connection conn = null;

    try {
      conn = psqlConn.connect(CONF_FILE);
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }

    BandwidthParser bandwidthParser =
        new BandwidthParser();
    try {
      String bandwidthPath = "./descriptors/relay-descriptors/bandwidths";
      bandwidthParser.run(bandwidthPath, conn);
    } catch (Exception e) {
      logger.warn("BandwidthParser run().", e);
    }

    BridgedbMetricsParser bridgedbMetricsParser =
        new BridgedbMetricsParser();
    try {
      String bridgedbMetricsPath =
          "./descriptors/bridgedb-metrics";
      bridgedbMetricsParser.run(bridgedbMetricsPath, conn);
    } catch (Exception e) {
      logger.warn("BridgedbMetricsParser run().", e);
    }

    BridgeNetworkStatusParser bridgeNetworkStatusParser =
        new BridgeNetworkStatusParser();
    try {
      String bridgeStatusPath = "./descriptors/bridge-descriptors/statuses";
      bridgeNetworkStatusParser.run(bridgeStatusPath, conn);
    } catch (Exception e) {
      logger.warn("BridgeNetworkStatusParser run().", e);
    }

    BridgePoolAssignmentsParser bridgePoolAssignmentsParser =
        new BridgePoolAssignmentsParser();
    try {
      String bridgePoolAssignmentsPath =
          "./descriptors/bridge-pool-assignments";
      bridgePoolAssignmentsParser.run(bridgePoolAssignmentsPath, conn);
    } catch (Exception e) {
      logger.warn("BridgePoolAssinmentsParser run().", e);
    }

    BridgestrapParser bridgestrapParser = new BridgestrapParser();
    try {
      String bridgestrapPath =
          "./descriptors/bridgestrap";
      bridgestrapParser.run(bridgestrapPath, conn);
    } catch (Exception e) {
      logger.warn("BridgestrapParser run().", e);
    }

    ConsensusParser consensusParser = new ConsensusParser();
    try {
      String consensusPath =
          "./descriptors/relay-descriptors/consensuses";
      consensusParser.run(consensusPath, conn);
      String relayConsensusMicroPath =
          "./descriptors/relay-descriptors/microdescs/consensus-microdesc/";
      consensusParser.run(relayConsensusMicroPath, conn);
    } catch (Exception e) {
      logger.warn("ConsensusParser run().", e);
    }

    ExitListParser exitListParser = new ExitListParser();
    try {
      String exitListPath =
          "./descriptors/exit-lists";
      exitListParser.run(exitListPath, conn);
    } catch (Exception e) {
      logger.warn("ExitListParser run().", e);
    }

    ExtraInfoDescriptorParser extraInfoParser = new ExtraInfoDescriptorParser();
    try {
      String bridgeExtraInfoPath =
          "./descriptors/bridge-descriptors/extra-infos";
      String relayExtraInfoPath = "./descriptors/relay-descriptors/extra-infos";
      extraInfoParser.run(bridgeExtraInfoPath, conn);
      extraInfoParser.run(relayExtraInfoPath, conn);
    } catch (Exception e) {
      logger.warn("ExtraInfoDescriptorParser run().", e);
    }

    ServerDescriptorParser serverParser = new ServerDescriptorParser();
    try {
      String bridgeServerPath =
          "./descriptors/bridge-descriptors/server-descriptors";
      String relayServerPath =
          "./descriptors/relay-descriptors/server-descriptors";
      serverParser.run(bridgeServerPath, conn);
      serverParser.run(relayServerPath, conn);
    } catch (Exception e) {
      logger.warn("ServerDescriptorParser run().", e);
    }

    long millis = System.currentTimeMillis();

    RouterFamilyBuilder familyBuilder = new RouterFamilyBuilder();
    try {
      familyBuilder.build(conn, millis);
    } catch (Exception e) {
      logger.warn("RouterFamilyBuilder run().", e);
    }

    RouterStatusBuilder statusBuilder = new RouterStatusBuilder();
    try {
      String relayStatusPath =
          "./descriptors/relay-descriptors/consensuses";
      String bridgeStatusPath =
          "./descriptors/bridge-descriptors/statuses";
      statusBuilder.run(relayStatusPath, millis, conn);
      statusBuilder.run(bridgeStatusPath, millis, conn);
    } catch (Exception e) {
      logger.warn("RouterStatusBuilder run().", e);
    }

    VoteParser voteParser = new VoteParser();
    try {
      String relayVotesPath =
          "./descriptors/relay-descriptors/votes/";
      voteParser.run(relayVotesPath, conn);
    } catch (Exception e) {
      logger.warn("VoteParser run().", e);
    }

    MicrodescriptorParser microdescriptorParser = new MicrodescriptorParser();
    try {
      String relayMicrodescriptorPath =
          "./descriptors/relay-descriptors/microdescs/micro/";
      microdescriptorParser.run(relayMicrodescriptorPath, conn);
    } catch (Exception e) {
      logger.warn("MicrodescriptorParser run().", e);
    }

    WebStatsParser webStatsParser = new WebStatsParser();
    try {
      String webStatsPath = "./descriptors/webstats/";
      webStatsParser.run(webStatsPath);
    } catch (Exception e) {
      logger.warn("WebStatsParser run().", e);
    }

    try {
      String refreshSql = "REFRESH MATERIALIZED VIEW server_status_latest";
      try (PreparedStatement stmt = conn.prepareStatement(refreshSql)) {
        stmt.execute();
        logger.info("Materialized view 'server_status_latest' "
            + "refreshed successfully.");
      }
    } catch (Exception e) {
      logger.warn("Refreshing materialized views run().", e);
    }
  }

}
