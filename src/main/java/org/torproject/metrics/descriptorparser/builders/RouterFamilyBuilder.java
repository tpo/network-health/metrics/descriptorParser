package org.torproject.metrics.descriptorparser.builders;

import org.torproject.metrics.descriptorparser.utils.DescriptorUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

/**
 * This class is responsible for building and updating sets of router families
 * based on server descriptors in the Tor network. It fetches family data from
 * the server_descriptor table and updates the server_families table.
 */
public class RouterFamilyBuilder {
  // Logger for this class
  private static final Logger logger = LoggerFactory.getLogger(
      RouterFamilyBuilder.class);

  // SQL query to select family sets from the server_descriptor table
  private static final String SELECT_FAMILY_SETS_SQL
      = "SELECT fingerprint, family FROM server_descriptor "
      + "WHERE published>=? AND published<=?;";

  // SQL query to insert or update family sets in the server_families table
  private static final String INSERT_FAMILY_SET_SQL
      = "INSERT INTO server_families"
      + " (digest, effective_family, members, published, updated)"
      + " VALUES (?, ?, ?, ?, ?) ON CONFLICT (digest)"
      + " DO UPDATE SET updated=?";

  private DescriptorUtils descUtils = new DescriptorUtils();

  /**
   * Builds and updates router family sets in the database.
   *
   * @param conn  The database connection.
   * @param millis The current time in milliseconds.
   * @throws Exception If an SQL or other exception occurs.
   */
  public void build(Connection conn, long millis) throws Exception {
    // Calculating time boundaries for the query
    long millisPerDay = 24 * 60 * 60 * 1000;
    long millisSinceGmtMidnight = millis % millisPerDay;
    long millisAtGmtMidnight = millis - millisPerDay - millisSinceGmtMidnight;
    long millisUntilGmtMidnight = millisPerDay - millisSinceGmtMidnight;
    long millisAtNextGmtMidnight = millis + millisUntilGmtMidnight;

    // Preparing the SQL statement for fetching family data
    PreparedStatement preparedQueryStatement = conn.prepareStatement(
        SELECT_FAMILY_SETS_SQL);
    preparedQueryStatement.clearParameters();
    preparedQueryStatement.setTimestamp(1, new Timestamp(millisAtGmtMidnight));
    preparedQueryStatement.setTimestamp(2,
        new Timestamp(millisAtNextGmtMidnight));

    // Map to hold the family data
    Map<String, Set<String>> familyDataMap = new HashMap<>();

    // Execute the query and process the results
    try (ResultSet rs = preparedQueryStatement.executeQuery()) {
      while (rs.next()) {
        String family = rs.getString("family");
        String fingerprint = rs.getString("fingerprint");
        if (family != null) {
          family = family.substring(1, family.length() - 1);

          String[] parts = family.split(",");
          Set<String> declarations = new HashSet<>();

          // The family declaration contains \" characters that
          // we want to clean. So first we clean up the declarations
          // and then add declarations to the set
          for (String part : parts) {
            part = part.substring(1, part.length() - 1);
            part = part.startsWith("$")
                ? part.substring(1, part.length()) : part;
            declarations.add(part);
          }
          familyDataMap.put(fingerprint, declarations);
        }
      }
    } catch (SQLException ex) {
      logger.warn("Exception: {}", ex.getMessage());
    }

    // Building family groups from the data
    List<Set<String>> groups = buildFamilies(familyDataMap);

    // Inserting or updating family groups in the database
    for (Set<String> group : groups) {
      // Use TreeSet to ensure the group is sorted
      TreeSet<String> sortedGroup = new TreeSet<>(group);

      StringBuilder familyString = new StringBuilder();
      for (String familyElement : sortedGroup) {
        familyString.append(familyElement).append(" ");
      }

      // Remove the trailing space
      if (familyString.length() > 0) {
        familyString.setLength(familyString.length() - 1);
      }

      // Calculating digest and preparing the insert statement
      String digest = descUtils.calculateDigestSha256Base64(
          familyString.toString().getBytes());
      int cnt = group.size();
      try (PreparedStatement preparedStatement =
          conn.prepareStatement(INSERT_FAMILY_SET_SQL)) {
        preparedStatement.setString(1, digest);
        preparedStatement.setString(2, familyString.toString());
        preparedStatement.setInt(3, cnt);
        preparedStatement.setTimestamp(4, new Timestamp(millis));
        preparedStatement.setTimestamp(5, new Timestamp(millis));
        preparedStatement.setTimestamp(6, new Timestamp(millis));
        preparedStatement.executeUpdate();
      } catch (Exception ex) {
        logger.warn("Exception: {}", ex.getMessage());
      }
    }
  }

  /**
   * Builds family groups based on the family data map.
   *
   * @param dataMap A map of fingerprints to their declared family members.
   * @return A list of sets, each representing a group of related routers.
   */
  private static List<Set<String>> buildFamilies(Map<String,
      Set<String>> dataMap) {
    List<Set<String>> groups = new ArrayList<>();
    Set<String> visited = new HashSet<>();

    for (String key : dataMap.keySet()) {
      if (!visited.contains(key)) {
        Set<String> group = new HashSet<>();
        Queue<String> queue = new LinkedList<>();
        queue.add(key);
        visited.add(key);

        while (!queue.isEmpty()) {
          String currentKey = queue.poll();
          group.add(currentKey);

          Set<String> declarations = dataMap.get(currentKey);
          for (String declaration : declarations) {
            if (dataMap.containsKey(declaration)
                && !visited.contains(declaration)) {
              queue.add(declaration);
              visited.add(declaration);
            }
          }
        }

        groups.add(group);
      }
    }

    return groups;
  }
}
