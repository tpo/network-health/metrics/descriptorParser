package org.torproject.metrics.descriptorparser.builders;

import org.torproject.descriptor.BridgeNetworkStatus;
import org.torproject.descriptor.Descriptor;
import org.torproject.descriptor.DescriptorReader;
import org.torproject.descriptor.DescriptorSourceFactory;
import org.torproject.descriptor.NetworkStatusEntry;
import org.torproject.descriptor.RelayNetworkStatusConsensus;

import org.torproject.metrics.descriptorparser.impl.RouterStatus;
import org.torproject.metrics.descriptorparser.stores.RouterStatusStore;
import org.torproject.metrics.descriptorparser.utils.DescriptorUtils;
import org.torproject.metrics.descriptorparser.utils.LookupResult;
import org.torproject.metrics.descriptorparser.utils.LookupService;
import org.torproject.metrics.descriptorparser.utils.ReverseDomainNameResolver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeSet;

public class RouterStatusBuilder {
  private static final Logger logger = LoggerFactory.getLogger(
      RouterStatusBuilder.class);

  private RouterStatusStore routerStatusStore = new RouterStatusStore();

  private ReverseDomainNameResolver reverseDomainNameResolver =
      new ReverseDomainNameResolver();

  private LookupService lookupService = new LookupService(new File("geoip"));

  private DescriptorUtils descUtils = new DescriptorUtils();

  private String versions;

  /**
   * Iterates through server descriptors and build server statuses.
   */
  public void run(String path, long millis, Connection conn) throws Exception {
    // Read network statuses from disk.
    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();

    this.versions = this.getRecommendedVersions(conn);

    for (Descriptor descriptor :
        descriptorReader.readDescriptors(new File(path))) {
      if (descriptor instanceof RelayNetworkStatusConsensus) {
        RelayNetworkStatusConsensus desc =
            (RelayNetworkStatusConsensus) descriptor;
        this.buildRelay(desc, millis, conn);
      } else if (descriptor instanceof BridgeNetworkStatus) {
        BridgeNetworkStatus desc = (BridgeNetworkStatus) descriptor;
        this.buildBridge(desc, millis, conn, versions);
      }  else {
        logger.warn("Invalid descriptor. Continuining...");
      }
    }
    routerStatusStore.updateNetworkStatuses(conn);
  }

  /**
   * Fetch the latest consensus and get the recommended server versions.
   *
   * @param conn The database connection object
   * @return versions The versions String
   */
  public String getRecommendedVersions(Connection conn) {

    String versions = "";
    try {
      PreparedStatement preparedStatement = conn.prepareStatement(
              "SELECT * FROM network_status ORDER BY valid_after DESC LIMIT 1;"
      );
      ResultSet rs = preparedStatement.executeQuery();
      if (rs.next()) {
        versions = String.join(", ",
                rs.getString("recommended_server_versions"));

      }
    } catch (SQLException ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
    return versions;
  }

  /**
   * Add a record to the server_status table.
   * A record represents the current status of a network node.
   */
  public void buildRelay(RelayNetworkStatusConsensus desc, long millis,
      Connection conn) throws Exception {
    /* Iterates through the network status consensus entries */
    for (Map.Entry<String, NetworkStatusEntry> e :
        desc.getStatusEntries().entrySet()) {
      NetworkStatusEntry entry = e.getValue();

      /* Initializes and returns a RouterStatus object  */
      RouterStatus routerStatus = routerStatusStore.initializeRelayStatus(
          desc, entry, millis, conn);

      /**
       * Retrieves the first network status entry for a specific router and
       * updates the provided RouterStatus object.
       * The first network status is always retrieved because we might have
       * ingested some prior data in the network that we didn't have available
       * at that time.
       */

      routerStatus = routerStatusStore.processFirstNetworkStatusEntry(
          routerStatus, conn);

      String[] familyFields = routerStatusStore.getEffectiveFamily(
          routerStatus.getFingerprint(), conn);
      routerStatus.setFamilyDigest(familyFields[0]);
      routerStatus.setEffectiveFamily(familyFields[1]);

      routerStatus = this.processRunningStatus(routerStatus, conn);

      routerStatus = routerStatusStore.getLastServerStatus(
          routerStatus, conn);

      String serverDescDigest = entry.getDescriptor().toLowerCase();
      routerStatus = routerStatusStore.processLastServerDescriptor(
          routerStatus, serverDescDigest, conn);

      if (routerStatus.getLastExtraInfoDescriptorDigest() != null) {
        routerStatus = routerStatusStore.processLastExtraInfoDescriptor(
            routerStatus, conn);
      }

      LookupResult lookupResult = new LookupResult();

      if (routerStatus.getAddress() != null) {
        routerStatus = this.processAddresses(routerStatus);

        SortedMap<String, LookupResult> lookupResults =
            this.lookupService.lookup(routerStatus.getAddressStrings());
        lookupResult = lookupResults.get(
            routerStatus.getAddress());
      }

      routerStatus = routerStatusStore.getLastNetworkStatusWeights(
          routerStatus, conn);

      routerStatus = routerStatusStore.getLastNetworkStatusTotals(
          routerStatus, conn);

      routerStatusStore.addRouterStatus(routerStatus, lookupResult, conn);
    }
    return;
  }

  /**
   * Build a bridge status.
   */
  public void buildBridge(BridgeNetworkStatus desc, long millis,
      Connection conn, String versions) throws Exception {
    for (Map.Entry<String, NetworkStatusEntry> e :
        desc.getStatusEntries().entrySet()) {

      NetworkStatusEntry entry = e.getValue();

      /**
       * Initializes and returns a RouterStatus object.
       */
      RouterStatus routerStatus = routerStatusStore.initializeBridgeStatus(
          desc, entry, millis, conn, versions);
      routerStatus = routerStatusStore.processBridgeFirstNetworkStatusEntry(
          routerStatus, conn);

      String serverDescDigest = entry.getDescriptor();

      routerStatus = routerStatusStore.processLastServerDescriptor(
          routerStatus, serverDescDigest, conn);

      if (routerStatus.getLastExtraInfoDescriptorDigest() != null) {
        routerStatus = routerStatusStore.processLastExtraInfoDescriptor(
            routerStatus, conn);
      }

      routerStatusStore.processBridgeServerInfo(routerStatus, conn);
      routerStatusStore.addRouterStatus(routerStatus, null, conn);
    }
    return;
  }

  private RouterStatus processRunningStatus(RouterStatus routerStatus,
      Connection conn) {
    /*
     * For relays we check that the relay contains the running flags to mark
     * it as online.
     *
     */
    String flags = "" + routerStatus.getFlags();
    if (flags.contains("Running")) {
      routerStatus.setRunning(true);
    } else {
      routerStatus.setRunning(false);
    }

    return routerStatus;
  }

  private RouterStatus processAddresses(RouterStatus routerStatus) {
    SortedSet<String> addressStrings = new TreeSet<>();
    addressStrings.add(routerStatus.getAddress());
    Map<String, Long> addressLastLookupTimes = new HashMap<>();
    addressLastLookupTimes.put(routerStatus.getAddress(),
        routerStatus.getLastSeenMillis());
    this.reverseDomainNameResolver.setAddresses(addressLastLookupTimes);
    this.reverseDomainNameResolver.startReverseDomainNameLookups();
    Map<String, SortedSet<String>> verifiedLookupResults =
        this.reverseDomainNameResolver.getVerifiedLookupResults();
    Map<String, SortedSet<String>> unverifiedLookupResults =
        this.reverseDomainNameResolver.getUnverifiedLookupResults();
    routerStatus.setVerifiedHostNames(
        verifiedLookupResults.get(routerStatus.getAddress()));
    routerStatus.setUnverifiedHostNames(
        unverifiedLookupResults.get(routerStatus.getAddress()));
    routerStatus.setAddressStrings(addressStrings);
    return routerStatus;

  }

}
