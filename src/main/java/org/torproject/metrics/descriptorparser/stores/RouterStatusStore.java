package org.torproject.metrics.descriptorparser.stores;

import org.torproject.descriptor.BridgeNetworkStatus;
import org.torproject.descriptor.NetworkStatusEntry;
import org.torproject.descriptor.RelayNetworkStatusConsensus;

import org.torproject.metrics.descriptorparser.impl.BwEntry;
import org.torproject.metrics.descriptorparser.impl.RouterStatus;
import org.torproject.metrics.descriptorparser.utils.DateTimeHelper;
import org.torproject.metrics.descriptorparser.utils.DescriptorUtils;
import org.torproject.metrics.descriptorparser.utils.LookupResult;

import org.apache.commons.lang3.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.UUID;

public class RouterStatusStore {
  private static final Logger logger = LoggerFactory.getLogger(
      RouterStatusStore.class);

  private String selectLastStatus;
  private String selectFirstStatus;

  private DescriptorUtils descUtils = new DescriptorUtils();

  private static final String INSERT_ROUTER_STATUS_SQL
      = "INSERT INTO server_status"
      + " (is_bridge, published, nickname, fingerprint,"
      + " or_addresses, last_seen, first_seen, running, flags,"
      + " country, country_name, autonomous_system, as_name,"
      + " verified_host_names, last_restarted,"
      + " exit_policy, contact, platform,"
      + " version, version_status, effective_family, declared_family,"
      + " transport, bridgedb_distributor, blocklist,"
      + " last_changed_address_or_port, diff_or_addresses,"
      + " unverified_host_names, unreachable_or_addresses,"
      + " overload_ratelimits_version, overload_ratelimits_timestamp,"
      + " overload_ratelimits_ratelimit, overload_ratelimits_burstlimit,"
      + " overload_ratelimits_read_count, overload_ratelimits_write_count,"
      + " overload_fd_exhausted_version, overload_fd_exhausted_timestamp,"
      + " overload_general_timestamp, overload_general_version,"
      + " first_network_status_digest, first_network_status_entry_digest,"
      + " last_network_status_digest, last_network_status_entry_digest,"
      + " first_bridge_network_status_digest,"
      + " first_bridge_network_status_entry_digest,"
      + " last_bridge_network_status_digest,"
      + " last_bridge_network_status_entry_digest,"
      + " last_network_status_weights_id, last_network_status_totals_id,"
      + " last_server_descriptor_digest, last_extrainfo_descriptor_digest,"
      + " last_bridgestrap_stats_digest, last_bridgestrap_test_digest,"
      + " last_bridge_pool_assignments_file_digest,"
      + " last_bridge_pool_assignment_digest, family_digest,"
      + " advertised_bandwidth, bandwidth, network_weight_fraction,"
      + " bandwidth_avg, bandwidth_burst, bandwidth_observed,"
      + " ipv6_default_policy, ipv6_port_list, socks_port,"
      + " dir_port, or_port, is_hibernating, address, is_stale)"
      + " VALUES"
      + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final String INSERT_BW_SQL
      = "INSERT INTO extra_info_bandwidth_history"
      + " (published, nickname, fingerprint, flags, time,"
      + " write_bandwidth_value, read_bandwidth_value,"
      + " ipv6_write_bandwidth_value, ipv6_read_bandwidth_value,"
      + " dirreq_write_bandwidth_value, dirreq_read_bandwidth_value,"
      + " extra_info_digest)"
      + " VALUES"
      + " (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
      + " ON CONFLICT (nickname, fingerprint, time)"
      + " DO UPDATE SET extra_info_digest = EXCLUDED.extra_info_digest,"
      + " published = EXCLUDED.published;";

  private static final String SELECT_LAST_ROUTER_STATUS_SQL
      = "SELECT * FROM server_status WHERE fingerprint=?"
      + " AND nickname=? AND published<=? ORDER BY published DESC LIMIT 1;";

  private static final String SELECT_FIRST_NETWORK_STATUS_ENTRY_SQL
      = "SELECT nse.*, ns.valid_after as valid_after,"
      + " ns.digest as network_status_digest"
      + " FROM network_status_entry nse"
      + " JOIN network_status_document nsd"
      + " ON nse.digest = nsd.network_status_entry_digest"
      + " JOIN network_status ns"
      + " ON nsd.network_status_digest = ns.digest"
      + " WHERE nse.fingerprint=?"
      + " AND nse.published<=?"
      + " ORDER BY ns.valid_after ASC"
      + " LIMIT 1;";

  private static final String SELECT_FIRST_BRIDGE_NETWORK_STATUS_ENTRY_SQL
      = "SELECT nse.*, ns.published as ns_published,"
      + " ns.digest as bridge_network_status_digest"
      + " FROM bridge_network_status_entry nse"
      + " JOIN bridge_network_status_document nsd"
      + " ON nse.digest = nsd.bridge_network_status_entry_digest"
      + " JOIN bridge_network_status ns"
      + " ON nsd.bridge_network_status_digest = ns.digest"
      + " WHERE nse.fingerprint=?"
      + " AND nse.nickname=?"
      + " AND nse.published<=?"
      + " ORDER BY ns.published ASC"
      + " LIMIT 1;";

  private static final String SELECT_LAST_BRIDGESTRAP_TEST_SQL
      = "SELECT * FROM bridgestrap_test WHERE fingerprint=?"
      + " AND published<=? ORDER BY published DESC LIMIT 1;";

  private static final String SELECT_LAST_BRIDGE_POOL_SQL
      = "SELECT * FROM bridge_pool_assignment WHERE fingerprint=?"
      + " AND published<=? ORDER BY published DESC LIMIT 1;";

  private static final String SELECT_NETWORK_STATUS_SQL
      = "SELECT * FROM network_status WHERE digest=?;";

  private static final String SELECT_BRIDGE_NETWORK_STATUS_SQL
      = "SELECT * FROM bridge_network_status WHERE digest=?;";

  private static final String SELECT_FAMILY_BY_FINGERPRINT
      = "SELECT * FROM server_families WHERE effective_family LIKE "
      + "? ORDER BY updated DESC LIMIT 1;";

  private static final String SELECT_LAST_EXTRA_INFO_SQL
      = "SELECT * FROM extra_info_descriptor WHERE digest_sha1_hex=? LIMIT 1;";

  private static final String SELECT_LAST_SERVER_DESCRIPTOR_SQL
      = "SELECT * FROM server_descriptor WHERE digest_sha1_hex=? LIMIT 1;";

  private static final String SELECT_LAST_NETWORK_STATUS_TOTALS_SQL
      = "SELECT * FROM network_status_totals WHERE network_status=?;";

  private static final String SELECT_LAST_NETWORK_STATUS_ENTRY_WEIGHTS_SQL
      = "SELECT * FROM network_status_entry_weights "
      + "WHERE network_status_entry=?;";

  // SQL query to find missing fingerprints and
  // valid_after from previous network status
  private static final String FIND_OFFLINE_FINGERPRINTS_SQL
      = "WITH latest_status AS ( "
      + "   SELECT ns.digest "
      + "   FROM network_status ns "
      + "   ORDER BY ns.valid_after DESC "
      + "   LIMIT 1 "
      + "), previous_status AS ( "
      + "   SELECT ns.digest, ns.valid_after "
      + "   FROM network_status ns "
      + "   ORDER BY ns.valid_after DESC "
      + "   OFFSET 1 LIMIT 1 "
      + "), previous_fingerprints AS ( "
      + "   SELECT nse.fingerprint, ps.valid_after "
      + "   FROM network_status_entry nse "
      + "   JOIN network_status_document nsd ON "
      + "   nse.digest = nsd.network_status_entry_digest "
      + "   JOIN previous_status ps ON "
      + "   nsd.network_status_digest = ps.digest "
      + "), latest_fingerprints AS ( "
      + "   SELECT nse.fingerprint "
      + "   FROM network_status_entry nse "
      + "   JOIN network_status_document nsd ON "
      + "   nse.digest = nsd.network_status_entry_digest "
      + "   JOIN latest_status ls ON "
      + "   nsd.network_status_digest = ls.digest "
      + "), missing_fingerprints AS ( "
      + "   SELECT pf.fingerprint, pf.valid_after "
      + "   FROM previous_fingerprints pf "
      + "   LEFT JOIN latest_fingerprints lf ON "
      + "   pf.fingerprint = lf.fingerprint "
      + "   WHERE lf.fingerprint IS NULL "
      + ") SELECT fingerprint, valid_after FROM "
      + "  missing_fingerprints;";

  private static final String UPDATE_SERVER_STATUS_SQL
      = "UPDATE server_status "
      + "SET running = false "
      + "WHERE fingerprint = ?;";

  /**
   * Gets the first status selection query.
   *
   * @return The first status selection query.
   */
  public String getSelectFirstStatus() {
    return selectFirstStatus;
  }

  /**
   * Sets the first status selection query.
   *
   * @param selectFirstStatus The query to set.
   */
  public void setSelectFirstStatus(String selectFirstStatus) {
    this.selectFirstStatus = selectFirstStatus;
  }

  /**
   * Initialize a relay status with information extracted from the consensus
   * and network status entry.
   *
   * @param desc the RelayNetworkStatusConsensus.
   * @param entry the NetworkStatusEntry.
   * @param millis the milliseconds timestamp.
   * @param conn the connection to the postgresql database.
   *
   * @return routerStatus the routerStatus object.
   */
  public RouterStatus initializeRelayStatus(RelayNetworkStatusConsensus desc,
      NetworkStatusEntry entry,
      long millis, Connection conn) {

    RouterStatus routerStatus = new RouterStatus();
    routerStatus.setNow(millis);
    this.setSelectFirstStatus(SELECT_FIRST_NETWORK_STATUS_ENTRY_SQL);
    routerStatus.setType(false);
    String fingerprint = entry.getFingerprint();
    String nickname = entry.getNickname();
    routerStatus.setNickname(nickname);
    routerStatus.setFingerprint(fingerprint);
    try {
      String digest = descUtils.calculateDigestSha256Base64(
          desc.getRawDescriptorBytes());
      routerStatus.setLastNetworkStatusDigest(digest);
      String entryDigest = this.descUtils.calculateDigestSha256Base64(
          entry.getStatusEntryBytes());
      routerStatus.setLastNetworkStatusEntryDigest(entryDigest);
    } catch (Exception ex) {
      logger.warn("Invalid descriptor. Skipping...");
      return null;
    }
    routerStatus.setFlags(descUtils.fieldAsString(entry.getFlags()));
    routerStatus.setVersion(entry.getVersion());
    String orAddressesAndPorts =
        descUtils.fieldAsString(entry.getOrAddresses())
        + "," + entry.getAddress()
        + "," + String.valueOf(entry.getOrPort());
    routerStatus.setDirPort(entry.getDirPort());
    routerStatus.setOrAddressesAndPorts(orAddressesAndPorts);
    routerStatus.setOrAddresses(
        descUtils.fieldAsString(entry.getOrAddresses()));
    routerStatus.setAddress(entry.getAddress());
    /* We do not know at this point if the address has changed compared to
     * the previous saved status, so we set the last changed address or port
     * timestamp to the valid after time in milliseconds from the consensus.
     *
     * We will check later on if the address is the same as the previous status
     * and if this is the case we will reset this timestamp to the previous
     * saved value.
     */
    routerStatus.setLastChangedAddressesMillis(
        desc.getValidAfterMillis());
    String versions = String.join(", ", desc.getRecommendedServerVersions());
    String[] recommendedVersions = routerStatus.computeRecommendedVersions(
        versions);
    routerStatus.setRecommendedVersions(recommendedVersions);
    routerStatus.setLastRecommendedServerVersions();
    routerStatus.setVersionStatus();
    routerStatus.setLastSeenMillis(desc.getValidAfterMillis());
    routerStatus.setBandwidth(entry.getBandwidth());
    return routerStatus;
  }

  /**
   * Initialize a bridge status.
   *
   * @param desc the BridgeNetworkStatus.
   * @param entry the NetworkStatusEntry.
   * @param millis the milliseconds timestamp.
   * @param conn the connection to the postgresql database.
   *
   * @return routerStatus the routerStatus object.
   */
  public RouterStatus initializeBridgeStatus(BridgeNetworkStatus desc,
      NetworkStatusEntry entry,
      long millis, Connection conn, String versions) {
    RouterStatus routerStatus = new RouterStatus();
    routerStatus.setNow(millis);
    this.setSelectFirstStatus(SELECT_FIRST_BRIDGE_NETWORK_STATUS_ENTRY_SQL);
    routerStatus.setType(true);
    String fingerprint = entry.getFingerprint();
    String nickname = entry.getNickname();
    routerStatus.setNickname(nickname);
    routerStatus.setFingerprint(fingerprint);
    try {
      String digest = descUtils.calculateDigestSha256Base64(
          desc.getRawDescriptorBytes());
      routerStatus.setLastNetworkStatusDigest(digest);
      String entryDigest = this.descUtils.calculateDigestSha256Base64(
          entry.getStatusEntryBytes());
      routerStatus.setLastNetworkStatusEntryDigest(entryDigest);
    } catch (Exception ex) {
      logger.warn("Invalid descriptor. Skipping...");
      return null;
    }

    String[] recommendedVersions = routerStatus.computeRecommendedVersions(
            versions);
    routerStatus.setRecommendedVersions(recommendedVersions);
    routerStatus.setLastRecommendedServerVersions();
    routerStatus.setVersionStatus();
    routerStatus.setFlags(descUtils.fieldAsString(entry.getFlags()));
    routerStatus.setLastSeenMillis(desc.getPublishedMillis());

    return routerStatus;
  }

  /**
   * Process the first NetworkStatusEntry present in the DB for the router.
   *
   * @param routerStatus the RouterStatus object.
   * @param conn the postgresql DB connection.
   *
   * @return routerStatus the RouterStatus object updated.
   */
  public RouterStatus processFirstNetworkStatusEntry(RouterStatus routerStatus,
      Connection conn) {
    try {
      PreparedStatement preparedQueryStatement = conn.prepareStatement(
              this.getSelectFirstStatus());
      preparedQueryStatement.clearParameters();
      preparedQueryStatement.setString(1, routerStatus.getFingerprint());
      preparedQueryStatement.setTimestamp(2,
          new Timestamp(routerStatus.getLastSeenMillis()));
      ResultSet rs = preparedQueryStatement.executeQuery();
      if (rs.next()) {
        routerStatus.setFirstNetworkStatusEntryDigest(
            rs.getString("digest"));
        routerStatus.setFirstNetworkStatusDigest(
            rs.getString("network_status_digest"));
        routerStatus.setFirstSeenMillis(
                rs.getTimestamp("valid_after").getTime());
      } else {
        /**
         * Return if the router has not been included in a consensus document
         * just yet.
         */
        return routerStatus;
      }
    } catch (SQLException ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
    return routerStatus;

  }

  /**
   * Process the first NetworkStatusEntry present in the DB for the bridge.
   *
   * @param routerStatus the RouterStatus object.
   * @param conn the postgresql DB connection.
   *
   * @return routerStatus the RouterStatus object updated.
   */
  public RouterStatus processBridgeFirstNetworkStatusEntry(
      RouterStatus routerStatus, Connection conn) {
    try {
      PreparedStatement preparedQueryStatement = conn.prepareStatement(
              this.getSelectFirstStatus());
      preparedQueryStatement.clearParameters();
      preparedQueryStatement.setString(1, routerStatus.getFingerprint());
      preparedQueryStatement.setString(2, routerStatus.getNickname());
      preparedQueryStatement.setTimestamp(3,
              new Timestamp(routerStatus.getLastSeenMillis()));
      ResultSet rs = preparedQueryStatement.executeQuery();
      if (rs.next()) {
        routerStatus.setFirstNetworkStatusEntryDigest(rs.getString("digest"));
        routerStatus.setFirstNetworkStatusDigest(
            rs.getString("bridge_network_status_digest"));
        routerStatus.setFirstSeenMillis(
            rs.getTimestamp("ns_published").getTime());
      } else {
        /**
         * Return if the router has not been included in a consensus document
         * just yet.
         */
        return routerStatus;
      }
    } catch (SQLException ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
    return routerStatus;

  }

  /**
   * Retrieves the effective family and its digest for a router identified by
   * its fingerprint. This method queries the database for family information
   * associated with the given fingerprint. If a matching record is found, it
   * extracts the family digest and effective family details. In case no record
   * is found, the effective family is set to the provided fingerprint. Any
   * SQLExceptions encountered during database operations are caught and logged
   * as warnings.
   *
   * @param fingerprint The fingerprint of the router whose family information
   *                    is to be retrieved.
   * @param conn        The database Connection object used for preparing and
   *                    executing the SQL query.
   * @return An array of two Strings, where the first element is the family
   *         digest and the second element is the effective family. If no
   *         matching record is found, the second element is set to the provided
   *         fingerprint.
   *
   * @throws SQLException If there is a database access error or other errors
   *                      related to the database query. This exception is
   *                      caught within the method and logged, but not rethrown.
   */

  public String[] getEffectiveFamily(String fingerprint,
        Connection conn) {
    String familyDigest = "";
    String effectiveFamily = "";
    try {
      PreparedStatement selectFamilyByFingerprint = conn.prepareStatement(
          SELECT_FAMILY_BY_FINGERPRINT);
      selectFamilyByFingerprint.clearParameters();
      selectFamilyByFingerprint.setString(1, "%" + fingerprint + "%");
      ResultSet rs = selectFamilyByFingerprint.executeQuery();
      if (rs.next()) {
        familyDigest = rs.getString("digest");
        effectiveFamily = rs.getString("effective_family");
      } else {
        effectiveFamily = fingerprint;
        familyDigest = null;
      }
    } catch (SQLException ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }

    String[] familyFields = {familyDigest, effectiveFamily};

    return familyFields;

  }

  /**
   * Fetches the most recent status of a router from the database and updates
   * the provided {@code RouterStatus} object with the latest information.
   * This method uses the router's fingerprint and nickname to query the
   * database for the latest OR addresses, the timestamp when the addresses or
   * ports last changed, and the differences in OR addresses since the last
   * update.
   * The method updates the {@code routerStatus} object's OR addresses, last
   * changed timestamp for addresses or ports, and differences in OR addresses
   * directly. If no newer information is found in the database than what is
   * already present in the {@code routerStatus} object, the object remains
   * unchanged.
   *
   * @param routerStatus The {@code RouterStatus} object containing the router's
   *                     current status information. This object will be updated
   *                     with the latest information from the database.
   *
   * @param conn The {@code Connection} object to the database used for
   *             executing the SQL query.
   *
   * @return The updated {@code RouterStatus} object. This object is the same
   *         instance that was passed in but potentially updated with new
   *         information from the database.
   *
   * @throws SQLException If an SQL error occurs during the execution of the
   *                      query. This exception is caught within the method and
   *                      logged, but not rethrown. The method returns the
   *                      {@code routerStatus} object in its current state,
   *                      which may or may not have been updated, depending on
   *                      when the exception occurred.
   */
  public RouterStatus getLastServerStatus(RouterStatus routerStatus,
      Connection conn) {
    try {
      PreparedStatement preparedQueryStatement = conn.prepareStatement(
          SELECT_LAST_ROUTER_STATUS_SQL);
      preparedQueryStatement.clearParameters();
      String fingerprint = routerStatus.getFingerprint();
      String nickname = routerStatus.getNickname();
      preparedQueryStatement.setString(1, fingerprint);
      preparedQueryStatement.setString(2, nickname);
      preparedQueryStatement.setTimestamp(3,
          new Timestamp(routerStatus.getLastSeenMillis()));
      ResultSet rs = preparedQueryStatement.executeQuery();
      if (rs.next()) {
        String statusOrAddress = rs.getString("or_addresses");
        String statusAddress = rs.getString("address");
        String statusOrPort = rs.getString("or_port");
        int statusDirPort = rs.getInt("dir_port");
        String orAddressesAndPorts = statusOrAddress
            + "," + statusAddress + "," + statusOrPort;
        String orAddressDiff = StringUtils.difference(
                routerStatus.getOrAddressesAndPorts(), orAddressesAndPorts);
        boolean dirPortDiff = (routerStatus.getDirPort() == statusDirPort);
        if (orAddressDiff.isEmpty() && dirPortDiff) {
          routerStatus.setLastChangedAddressesMillis(
              rs.getTimestamp("last_changed_address_or_port").getTime());
        }
        routerStatus.setDiffOrAddresses(orAddressDiff);

      }

    } catch (SQLException ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
    return routerStatus;
  }

  /**
   * Sets the last extra info digest for the provided RouterStatus object based
   * on the given fingerprint.
   * This method queries the database for extra information related to a router
   * identified by the specified fingerprint.
   * If found, it updates the RouterStatus object with the last extra info
   * descriptor digest and various overload rate limits and file descriptor
   * exhaustion data if the router is a bridge. The method handles any
   * SQLExceptions by logging a warning.
   *
   * @param routerStatus The RouterStatus object to be updated with the last
   *                     extra info descriptor digest and potentially
   *                     overload-related data.
   *
   * @param conn The database Connection object used for preparing and executing
   *             the SQL query.
   *
   * @return The RouterStatus object, updated with the last extra info
   *         descriptor digest and additional data if the router is a bridge and
   *         such data is found. If no data is found or in case of an SQL
   *         exception, the original RouterStatus object is returned.
   *
   * @throws SQLException If there is a database access error or other errors
   *         related to the database query. This exception is caught within the
   *         method and logged, but not rethrown.
   */
  public RouterStatus processLastExtraInfoDescriptor(RouterStatus routerStatus,
      Connection conn) {
    try {
      PreparedStatement preparedExtraInfoStatement =
          conn.prepareStatement(SELECT_LAST_EXTRA_INFO_SQL);
      preparedExtraInfoStatement.clearParameters();
      String digestSha1Hex = routerStatus.getLastExtraInfoDescriptorDigest();
      preparedExtraInfoStatement.setString(1, digestSha1Hex);
      ResultSet rs = preparedExtraInfoStatement.executeQuery();
      if (rs.next()) {
        routerStatus.setLastExtraInfoDescriptorDigest(
            rs.getString("digest_sha1_hex"));
        if (!routerStatus.isBridge()) {
          routerStatus.setOverloadRatelimitsVersion(
              rs.getInt("overload_ratelimits_version"));
          routerStatus.setOverloadRatelimitsTimestamp(
              rs.getTimestamp("overload_ratelimits_timestamp"));
          routerStatus.setOverloadRatelimitsRatelimit(
              rs.getInt("overload_ratelimits_ratelimit"));
          routerStatus.setOverloadRatelimitsBurstlimit(
              rs.getInt("overload_ratelimits_burstlimit"));
          routerStatus.setOverloadRatelimitsReadCount(
              rs.getInt("overload_ratelimits_read_count"));
          routerStatus.setOverloadRatelimitsWriteCount(
              rs.getInt("overload_ratelimits_write_count"));
          routerStatus.setOverloadFdExhaustedVersion(
              rs.getInt("overload_fd_exhausted_version"));
          routerStatus.setOverloadFdExhaustedTimestamp(
              rs.getTimestamp("overload_fd_exhausted_timestamp"));
          processBw(conn, rs.getString("write_history"),
              rs.getString("read_history"),
              rs.getString("dirreq_write_history"),
              rs.getString("dirreq_read_history"),
              rs.getString("ipv6_write_history"),
              rs.getString("ipv6_read_history"),
              rs.getString("digest_sha1_hex"),
              rs.getString("nickname"),
              rs.getString("fingerprint"),
              routerStatus.getFlags(),
              routerStatus.getLastSeenMillis());
        }
      } else {
        routerStatus.setLastExtraInfoDescriptorDigest(null);
        routerStatus.setStaleStatus(true);
      }
    } catch (SQLException ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
    return routerStatus;
  }

  /**
   * Process bandwidth histories and add values to the DB.
   *
   * @param conn The database Connection object used for
   *             preparing and executing the SQL query.
   * @param writeHistory Write bandwidth history from the
   *        ExtraInfo Descriptor
   * @param readHistory Read bandwidth history from the
   *        ExtraInfo Descriptor
   * @param dirreqWriteHistory Directory request write
   *        bandwidth history from the ExtraInfo Descriptor
   * @param dirreqReadHistory Directory request read
   *        bandwidth history from the ExtraInfo Descriptor
   * @param ipv6WriteHistory Ipv6 write bandwidth history
   *        from the ExtraInfo Descriptor
   * @param ipv6ReadHistory Ipv6 read bandwidth history
   *        from the ExtraInfo Descriptor
   * @param digest The ExtraInfo digest
   * @param nickname The relay nickname
   * @param fingerprint The relay fingerprint
   * @param flags The relay flags
   * @param publishedMillis The published Millis from the
   *        server status document
   *
   */
  public void processBw(Connection conn, String writeHistory,
      String readHistory, String dirreqWriteHistory,
      String dirreqReadHistory, String ipv6WriteHistory,
      String ipv6ReadHistory,
      String digest, String nickname, String fingerprint,
      String flags, long publishedMillis) {
    // Create a map to store bandwidth values by timestamp
    Map<Long, BwEntry> bandwidthMap = new HashMap<>();

    // Process each history field (writeHistory,
    // readHistory, etc.) and populate the map
    processHistory(bandwidthMap,
        writeHistory, "write_history");
    processHistory(bandwidthMap,
        readHistory, "read_history");
    processHistory(bandwidthMap,
        dirreqWriteHistory, "dirreq_write_history");
    processHistory(bandwidthMap,
        dirreqReadHistory, "dirreq_read_history");
    processHistory(bandwidthMap,
        ipv6WriteHistory, "ipv6_write_history");
    processHistory(bandwidthMap,
        ipv6ReadHistory, "ipv6_read_history");

    // Insert the data into the database
    insertBandwidthData(conn, bandwidthMap, digest, nickname,
        fingerprint, flags, new Timestamp(publishedMillis));
  }

  // Helper function to process each bandwidth history
  // and populate the map
  private void processHistory(Map<Long, BwEntry> bandwidthMap,
      String history, String historyType) {

    if (history == null || history.equals("null")) {
      return;
    }

    try {
      // Assume descUtils.computeBandwidthValues takes `history` as a parameter
      SortedMap<Long, Double> bandwidthValues =
          descUtils.computeBandwidthValues(history);

      for (Map.Entry<Long, Double> entry : bandwidthValues.entrySet()) {
        Long timestamp = entry.getKey();
        Double bandwidth = entry.getValue();

        // Get or create a BwEntry object for this timestamp
        BwEntry bwEntry = bandwidthMap.getOrDefault(
            timestamp, new BwEntry(timestamp));

        // Set the appropriate value based on the historyType
        switch (historyType) {
          case "write_history":
            bwEntry.setWriteValue(bandwidth);
            break;
          case "read_history":
            bwEntry.setReadValue(bandwidth);
            break;
          case "dirreq_write_history":
            bwEntry.setDirreqWriteValue(bandwidth);
            break;
          case "dirreq_read_history":
            bwEntry.setDirreqReadValue(bandwidth);
            break;
          case "ipv6_write_history":
            bwEntry.setIpv6WriteValue(bandwidth);
            break;
          case "ipv6_read_history":
            bwEntry.setIpv6ReadValue(bandwidth);
            break;
          default:
            // Optionally handle unknown history types if necessary
            break;
        }

        // Update the map with the BwEntry object
        bandwidthMap.put(timestamp, bwEntry);
      }
    } catch (Exception ex) {
      logger.warn("Exception: " + ex.getMessage());
    }
  }

  // Insert bandwidth data into the database
  private void insertBandwidthData(Connection conn, Map<Long,
      BwEntry> bandwidthMap, String digest, String nickname,
      String fingerprint, String flags, Timestamp published) {
    try (PreparedStatement ps = conn.prepareStatement(INSERT_BW_SQL)) {
      for (Map.Entry<Long, BwEntry> entry : bandwidthMap.entrySet()) {
        Long timestamp = entry.getKey();
        BwEntry bwEntry = entry.getValue();
        ps.setTimestamp(1, published);
        ps.setString(2, nickname);
        ps.setString(3, fingerprint);
        ps.setString(4, flags);
        ps.setTimestamp(5, new Timestamp(timestamp));
        ps.setObject(6, bwEntry.getWriteValue());
        ps.setObject(7, bwEntry.getReadValue());
        ps.setObject(8, bwEntry.getIpv6WriteValue());
        ps.setObject(9, bwEntry.getIpv6ReadValue());
        ps.setObject(10, bwEntry.getDirreqWriteValue());
        ps.setObject(11, bwEntry.getDirreqReadValue());
        ps.setString(12, digest);

        // Add batch for batch processing
        ps.addBatch();
      }
      ps.executeBatch(); // Execute the batch
    } catch (SQLException e) {
      e.printStackTrace(); // Handle SQL exceptions
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }

  /**
   * Process the latest Server Descriptor for the router.
   *
   * @param routerStatus the RouterStatus object.
   *
   * @param conn the postgresql database connection object.
   *
   * @return the updated RouterStatus object.
   */
  public RouterStatus processLastServerDescriptor(RouterStatus routerStatus,
      String serverDescSha1, Connection conn) {
    try {
      PreparedStatement preparedStatement =
          conn.prepareStatement(SELECT_LAST_SERVER_DESCRIPTOR_SQL);
      preparedStatement.clearParameters();
      preparedStatement.setString(1, serverDescSha1);
      ResultSet rs = preparedStatement.executeQuery();
      if (rs.next()) {
        routerStatus.setLastServerDescriptorDigest(serverDescSha1);
        routerStatus.setContact(rs.getString("contact"));
        routerStatus.setPlatform(rs.getString("platform"));
        // Fetch digest values from the result set
        String extraInfoDigest = null;
        if (rs.getString("extra_info_sha1_digest") != null) {
          extraInfoDigest =
              rs.getString("extra_info_sha1_digest").toLowerCase();
        }
        routerStatus.setLastExtraInfoDescriptorDigest(extraInfoDigest);
        routerStatus.setOverloadGeneralVersion(
            rs.getInt("overload_general_version"));
        if (rs.getTimestamp("overload_general_timestamp") != null) {
          routerStatus.setOverloadGeneralTimestamp(
              rs.getTimestamp("overload_general_timestamp").getTime());
        } else {
          routerStatus.setOverloadGeneralTimestamp(-1L);
        }
        if (rs.getString("exit_policy") != null) {
          routerStatus.setExitPolicyLines(
              rs.getString("exit_policy"));
        } else {
          routerStatus.setExitPolicyLines(null);
        }
        routerStatus.getExitPolicyLines();
        routerStatus.setUptime(rs.getLong("uptime"));
        routerStatus.setFamilyEntries(rs.getString("family"));
        long advBw = Math.min(
            Math.min(rs.getLong("bandwidth_avg"),
            rs.getLong("bandwidth_burst")),
            rs.getLong("bandwidth_observed"));
        routerStatus.setAdvertisedBandwidth(advBw);
        routerStatus.setBandwidthRate(rs.getLong("bandwidth_avg"));
        routerStatus.setBandwidthBurst(rs.getLong("bandwidth_burst"));
        routerStatus.setBandwidthObserved(rs.getLong("bandwidth_observed"));
        routerStatus.setIpv6DefaultPolicy(
            rs.getString("ipv6_default_policy"));
        routerStatus.setIpv6PortList(rs.getString("ipv6_port_list"));
        routerStatus.setIsHibernating(rs.getBoolean("is_hibernating"));
        routerStatus.setSocksPort(rs.getInt("socks_port"));
        routerStatus.setDirPort(rs.getInt("dir_port"));
        routerStatus.setOrPort(rs.getInt("or_port"));
        routerStatus.setLastServerDescriptorPublishedMillis(
            rs.getTimestamp("published").getTime());

      } else {
        routerStatus.setStaleStatus(true);
      }
    } catch (SQLException ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
    return routerStatus;

  }

  /**
   * Updates the RouterStatus object with bridge-specific flags and status
   * based on the given ServerDescriptor.
   * This method performs database queries to fetch the results of the latest
   * Bridgestrap tests and bridge pool assignments for a router identified by
   * its fingerprint and published publishedstamp. It updates the RouterStatus
   * object with information about the router's operational status (running or
   * not), Bridgestrap test results, transport types, distribution methods,
   * blocklist, and related digests. SQLExceptions are caught and logged as
   * warnings.
   *
   * @param routerStatus The RouterStatus object to be updated with
   *        bridge-specific flags and status.
   *
   * @param conn The database Connection object used for preparing and executing
   *        the SQL queries.

   * @return The RouterStatus object, updated with bridge-specific information.
   *         If no relevant data is found or in case of an SQL exception,
   *         the original RouterStatus object is returned with default or
   *         previously set values.
   *
   * @throws SQLException If there is a database access error or other errors
   *         related to the database queries. These exceptions are caught within
   *         the method and logged, but not rethrown.
   *
   */
  public RouterStatus processBridgeServerInfo(RouterStatus routerStatus,
      Connection conn) {
    try {
      PreparedStatement preparedQueryStatement = conn.prepareStatement(
          SELECT_LAST_BRIDGESTRAP_TEST_SQL);
      preparedQueryStatement.clearParameters();
      preparedQueryStatement.setString(1, routerStatus.getFingerprint());
      preparedQueryStatement.setTimestamp(2,
          new Timestamp(routerStatus.getLastSeenMillis()));
      ResultSet rs = preparedQueryStatement.executeQuery();
      if (rs.next()) {
        boolean result = rs.getBoolean("result");
        long bridgestrapLastEndMillis
            = rs.getTimestamp("published").getTime();
        if (!result) {
          routerStatus.setRunning(false);
        } else if (bridgestrapLastEndMillis
            <= (routerStatus.getNow() - DateTimeHelper.ONE_WEEK)) {
          routerStatus.setRunning(false);
        } else {
          routerStatus.setRunning(true);
        }
        routerStatus.setLastBridgestrapTestDigest(rs.getString("digest"));
        routerStatus.setLastBridgestrapStatsDigest(
            rs.getString("bridgestrap_stats"));
      } else {
        routerStatus.setRunning(false);
      }
    } catch (SQLException ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
    try {
      PreparedStatement preparedQueryStatement = conn.prepareStatement(
          SELECT_LAST_BRIDGE_POOL_SQL);
      preparedQueryStatement.clearParameters();
      preparedQueryStatement.setString(1, routerStatus.getFingerprint());
      preparedQueryStatement.setTimestamp(2,
          new Timestamp(routerStatus.getLastSeenMillis()));
      ResultSet rs = preparedQueryStatement.executeQuery();
      if (rs.next()) {
        routerStatus.setTransports(rs.getString("transport"));
        routerStatus.setDistributionMethod(
            rs.getString("distribution_method"));
        routerStatus.setBlocklist(rs.getString("blocklist"));
        routerStatus.setLastBridgepoolAssignmentDigest(rs.getString("digest"));
        routerStatus.setLastBridgepoolAssignmentsFileDigest(
            rs.getString("bridge_pool_assignments"));
      }
    } catch (SQLException ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
    return routerStatus;
  }

  /**
   * Retrieves the last network status weights for a given RouterStatus object
   * and updates it.
   * This method queries the database using the last network status entry of
   * the RouterStatus object
   * to find the corresponding weights. If found, it updates the RouterStatus
   * object with the weights ID.
   * In case of an SQL exception, it logs a warning with the exception message.
   *
   * @param routerStatus The RouterStatus object which is to be updated with
   *        the weights ID.
   *
   * @param conn The database Connection object used for preparing and
   *        executing the SQL query.
   *
   * @return The RouterStatus object, updated with the last network status
   *          weights ID if it is found. If no weights are found or in case of
   *          an SQL exception, the original RouterStatus object is returned.

   * @throws SQLException If there is a database access error or other
   *         errors related to the database query. This exception is caught
   *         within the method and logged, but not rethrown.
   */
  public RouterStatus getLastNetworkStatusWeights(RouterStatus routerStatus,
      Connection conn) {
    try {
      PreparedStatement preparedQueryStatement = conn.prepareStatement(
          SELECT_LAST_NETWORK_STATUS_ENTRY_WEIGHTS_SQL);
      preparedQueryStatement.clearParameters();
      preparedQueryStatement.setString(1,
          routerStatus.getLastNetworkStatusEntryDigest());
      ResultSet rs = preparedQueryStatement.executeQuery();
      if (rs.next()) {
        routerStatus.setLastNetworkStatusWeightsId(rs.getString("weights_id"));
        routerStatus.setWeightFraction(rs.getFloat("network_weight_fraction"));
      }
    } catch (SQLException ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
    return routerStatus;
  }

  /**
   * Retrieves the last network status totals for the specified RouterStatus
   * object and updates it.
   * This method executes a database query using the last network status
   * digest of the RouterStatus object
   * to find the corresponding totals. If the totals are found, the method
   * updates the RouterStatus object with the totals ID.
   * In the case of an SQL exception, it logs a warning message with the
   * exception details.
   *
   * @param routerStatus The RouterStatus object to be updated with the last
   *        network status totals ID.
   *
   * @param conn The database Connection object used for preparing and
   *        executing the SQL query.
   *
   * @return The RouterStatus object, updated with the last network status
   *         totals ID if it is found. If no totals are found or in the case of
   *         an SQL exception, the original RouterStatus object is returned.
   *
   * @throws SQLException If there is a database access error or other errors
   *         related to the database query. This exception is caught within the
   *         method and logged, but not rethrown.
   */
  public RouterStatus getLastNetworkStatusTotals(RouterStatus routerStatus,
      Connection conn) {
    try {
      PreparedStatement preparedQueryStatement = conn.prepareStatement(
          SELECT_LAST_NETWORK_STATUS_TOTALS_SQL);
      preparedQueryStatement.clearParameters();
      preparedQueryStatement.setString(1,
          routerStatus.getLastNetworkStatusDigest());
      ResultSet rs = preparedQueryStatement.executeQuery();
      if (rs.next()) {
        routerStatus.setLastNetworkStatusTotalsId(rs.getString("totals_id"));
      }
    } catch (SQLException ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
    return routerStatus;
  }

  /**
   * Add the routerStatus to the database.
   *
   * @param routerStatus The RouterStatus object to be updated with the last
   *        network status totals ID.
   *
   * @param lookupResult the dns lookup results.
   *
   * @param conn The database Connection object used for preparing and
   *        executing the SQL query.
   *
   * @throws SQLException If there is a database access error or other errors
   *         related to the database query. This exception is caught within the
   *         method and logged, but not rethrown.
   */
  public void addRouterStatus(RouterStatus routerStatus,
      LookupResult lookupResult, Connection conn) {
    try (
      PreparedStatement preparedStatement =
          conn.prepareStatement(INSERT_ROUTER_STATUS_SQL);
    ) {
      preparedStatement.setBoolean(1, routerStatus.isBridge());
      preparedStatement.setTimestamp(2,
          new Timestamp(routerStatus.getLastSeenMillis()));
      preparedStatement.setString(3, routerStatus.getNickname());
      preparedStatement.setString(4, routerStatus.getFingerprint());
      if (routerStatus.getOrAddresses() != null) {
        preparedStatement.setString(5, routerStatus.getOrAddresses());
      } else {
        preparedStatement.setString(5, null);
      }
      preparedStatement.setTimestamp(6,
          new Timestamp(routerStatus.getLastSeenMillis()));
      preparedStatement.setTimestamp(7,
          new Timestamp(routerStatus.getFirstSeenMillis()));
      preparedStatement.setBoolean(8, routerStatus.isRunning());
      if (routerStatus.getFlags() != null) {
        preparedStatement.setString(9, routerStatus.getFlags());
      } else {
        preparedStatement.setString(9, null);
      }
      if (lookupResult != null) {
        preparedStatement.setString(10, lookupResult.getCountryCode());
        preparedStatement.setString(11, lookupResult.getCountryName());
        preparedStatement.setString(12, lookupResult.getAsNumber());
        preparedStatement.setString(13, lookupResult.getAsName());
      } else {
        preparedStatement.setString(10, null);
        preparedStatement.setString(11, null);
        preparedStatement.setString(12, null);
        preparedStatement.setString(13, null);
      }
      if (routerStatus.getVerifiedHostNames() != null) {
        preparedStatement.setString(14, descUtils.fieldAsString(
            routerStatus.getVerifiedHostNames()));
      } else {
        preparedStatement.setString(14, null);
      }
      preparedStatement.setTimestamp(15,
          new Timestamp(this.calculateLastRestartedMillis(routerStatus)));
      if (routerStatus.getExitPolicyLines() != null) {
        preparedStatement.setString(16,
            descUtils.fieldAsString(routerStatus.getExitPolicyLines()));
      } else {
        preparedStatement.setString(16, null);
      }
      if (routerStatus.getContact() != null) {
        preparedStatement.setString(17, routerStatus.getContact());
      } else {
        preparedStatement.setString(17, null);
      }
      preparedStatement.setString(18, routerStatus.getPlatform());
      if (routerStatus.getVersion() != null) {
        preparedStatement.setString(19, routerStatus.getVersion());
        preparedStatement.setString(20, routerStatus.getVersionStatus());
      } else {
        preparedStatement.setString(19, null);
        preparedStatement.setString(20, null);
      }
      if (routerStatus.getFamilyEntries() != null) {
        preparedStatement.setString(21,
            descUtils.fieldAsString(routerStatus.getEffectiveFamily()));
        preparedStatement.setString(22,
            descUtils.fieldAsString(routerStatus.getFamilyEntries()));
      } else {
        preparedStatement.setString(21, routerStatus.getFingerprint());
        preparedStatement.setString(22, routerStatus.getFingerprint());
      }
      preparedStatement.setString(23, routerStatus.getTransports());
      preparedStatement.setString(24, routerStatus.getDistributionMethod());
      preparedStatement.setString(25, routerStatus.getBlocklist());
      preparedStatement.setTimestamp(26,
          new Timestamp(routerStatus.getLastChangedAddressesMillis()));
      preparedStatement.setString(27, routerStatus.getDiffOrAddresses());
      if (routerStatus.getUnverifiedHostNames() != null) {
        preparedStatement.setString(28, descUtils.fieldAsString(
            routerStatus.getUnverifiedHostNames()));
      } else {
        preparedStatement.setString(28, null);
      }
      if (routerStatus.getOrAddressesAndPorts() == null) {
        preparedStatement.setString(29, routerStatus.getOrAddresses());
      } else if (routerStatus.getOrAddresses() == null ) {
        preparedStatement.setString(29, null);
      } else {
        String unreachable = routerStatus.getOrAddresses();
        String[] addresses = routerStatus.getOrAddressesAndPorts().split(",");
        String unreachableIpv4 = routerStatus.getAddress();
        boolean isUnreachableFound =
            Arrays.asList(addresses).contains(unreachable)
            || Arrays.asList(addresses).contains(unreachableIpv4);
        // If unreachable is found within addresses, make it an empty string
        if (isUnreachableFound) {
          unreachable = ""; // Set unreachable to an empty string
        }
        preparedStatement.setString(29, unreachable);
      }
      preparedStatement.setInt(30, routerStatus.getOverloadRatelimitsVersion());
      if (routerStatus.getOverloadRatelimitsTimestamp() != null) {
        preparedStatement.setTimestamp(31,
            routerStatus.getOverloadRatelimitsTimestamp());
      } else {
        preparedStatement.setTimestamp(31, null);
      }
      preparedStatement.setLong(32,
          routerStatus.getOverloadRatelimitsRatelimit());
      preparedStatement.setLong(33,
          routerStatus.getOverloadRatelimitsBurstlimit());
      preparedStatement.setLong(34,
          routerStatus.getOverloadRatelimitsReadCount());
      preparedStatement.setLong(35,
          routerStatus.getOverloadRatelimitsWriteCount());
      preparedStatement.setInt(36,
          routerStatus.getOverloadFdExhaustedVersion());
      if (routerStatus.getOverloadFdExhaustedTimestamp() != null) {
        preparedStatement.setTimestamp(37,
            routerStatus.getOverloadFdExhaustedTimestamp());
      } else {
        preparedStatement.setTimestamp(37, null);
      }
      if (routerStatus.getOverloadGeneralTimestamp() != -1L) {
        preparedStatement.setTimestamp(38,
            new Timestamp(routerStatus.getOverloadGeneralTimestamp()));
      } else {
        preparedStatement.setTimestamp(38, null);
      }

      preparedStatement.setInt(39, routerStatus.getOverloadGeneralVersion());
      if (routerStatus.isBridge()) {
        preparedStatement.setString(40, null);
        preparedStatement.setString(41, null);
        preparedStatement.setString(42, null);
        preparedStatement.setString(43, null);
        preparedStatement.setString(44,
            routerStatus.getFirstNetworkStatusDigest());
        preparedStatement.setString(45,
            routerStatus.getFirstNetworkStatusEntryDigest());
        preparedStatement.setString(46,
            routerStatus.getLastNetworkStatusDigest());
        preparedStatement.setString(47,
            routerStatus.getLastNetworkStatusEntryDigest());

      } else {
        preparedStatement.setString(40,
            routerStatus.getFirstNetworkStatusDigest());
        preparedStatement.setString(41,
            routerStatus.getFirstNetworkStatusEntryDigest());
        preparedStatement.setString(42,
            routerStatus.getLastNetworkStatusDigest());
        preparedStatement.setString(43,
            routerStatus.getLastNetworkStatusEntryDigest());
        preparedStatement.setString(44, null);
        preparedStatement.setString(45, null);
        preparedStatement.setString(46, null);
        preparedStatement.setString(47, null);
      }
      if (routerStatus.isBridge()) {
        preparedStatement.setObject(48, null);
        preparedStatement.setObject(49, null);
      } else {
        if (routerStatus.getLastNetworkStatusWeightsId() != null) {
          UUID weightsId = UUID.fromString(
              routerStatus.getLastNetworkStatusWeightsId());
          preparedStatement.setObject(48, weightsId);
        } else {
          preparedStatement.setObject(48, null);
        }
        if (routerStatus.getLastNetworkStatusTotalsId() != null) {
          UUID totalsId = UUID.fromString(
              routerStatus.getLastNetworkStatusTotalsId());
          preparedStatement.setObject(49, totalsId);
        } else {
          preparedStatement.setObject(49, null);
        }
      }
      preparedStatement.setString(50,
          routerStatus.getLastServerDescriptorDigest());
      preparedStatement.setString(51,
          routerStatus.getLastExtraInfoDescriptorDigest());
      preparedStatement.setString(52,
          routerStatus.getLastBridgestrapStatsDigest());
      preparedStatement.setString(53,
          routerStatus.getLastBridgestrapTestDigest());
      preparedStatement.setString(54,
          routerStatus.getLastBridgepoolAssignmentsFileDigest());
      preparedStatement.setString(55,
          routerStatus.getLastBridgepoolAssignmentDigest());
      if (routerStatus.getFamilyDigest() == "") {
        preparedStatement.setString(56, null);
      } else {
        preparedStatement.setString(56, routerStatus.getFamilyDigest());
      }
      preparedStatement.setLong(57, routerStatus.getAdvertisedBandwidth());
      preparedStatement.setLong(58, routerStatus.getBandwidth());
      preparedStatement.setFloat(59, routerStatus.getWeightFraction());
      preparedStatement.setLong(60, routerStatus.getBandwidthRate());
      preparedStatement.setLong(61, routerStatus.getBandwidthBurst());
      preparedStatement.setLong(62, routerStatus.getBandwidthObserved());
      preparedStatement.setString(63, routerStatus.getIpv6DefaultPolicy());
      preparedStatement.setString(64, routerStatus.getIpv6PortList());
      preparedStatement.setInt(65, routerStatus.getSocksPort());
      preparedStatement.setInt(66, routerStatus.getDirPort());
      preparedStatement.setInt(67, routerStatus.getOrPort());
      preparedStatement.setBoolean(68, routerStatus.getIsHibernating());
      preparedStatement.setString(69, routerStatus.getAddress());
      preparedStatement.setBoolean(70, routerStatus.isStale());
      preparedStatement.executeUpdate();
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
      ex.printStackTrace(System.out);
    }
  }

  /**
   * Find missing relays by comparing the previous and latest
   * network statuses, and updates the server_status table to reflect
   * that these relays are no longer running.
   *
   * @param conn The database Connection object used for preparing and
   *        executing the SQL queries.
   *
   * @throws SQLException If there is a database access error or other errors
   *         related to the database query. This exception is caught within the
   *         method and logged, but not rethrown.
   */
  public void updateNetworkStatuses(Connection conn) {
    try {
      // Step 1: Find missing fingerprints and their valid_after timestamp
      PreparedStatement findFingerprintsStmt = conn.prepareStatement(
          FIND_OFFLINE_FINGERPRINTS_SQL);
      ResultSet resultSet = findFingerprintsStmt.executeQuery();

      // Step 2: Prepare the update statement
      PreparedStatement updateServerStatusStmt = conn.prepareStatement(
          UPDATE_SERVER_STATUS_SQL);

      // Step 3: Iterate through each fingerprint and valid_after timestamp
      while (resultSet.next()) {
        String fingerprint = resultSet.getString("fingerprint");

        // Step 4: Execute update statement
        updateServerStatusStmt.setString(1, fingerprint);
        updateServerStatusStmt.executeUpdate();
      }

      // Step 5: Clean up
      resultSet.close();
      findFingerprintsStmt.close();
      updateServerStatusStmt.close();
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
      ex.printStackTrace(System.out);
    }
  }

  private Long calculateLastRestartedMillis(RouterStatus routerStatus) {
    Long lastRestartedMillis = 0L;
    if (routerStatus.getUptime() != -1L) {
      lastRestartedMillis =
          routerStatus.getLastServerDescriptorPublishedMillis()
          - routerStatus.getUptime() * DateTimeHelper.ONE_SECOND;
    }
    return lastRestartedMillis;
  }

}
