package org.torproject.metrics.descriptorparser.impl;

import org.torproject.metrics.descriptorparser.utils.TorVersion;
import org.torproject.metrics.descriptorparser.utils.TorVersionStatus;

import java.sql.Timestamp;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Represents the status of a router in a network, encapsulating various details
 * such as its bridge status, operational timestamps, network addresses,
 * version information, and additional characteristics.
 */
public class RouterStatus {

  private boolean isTypeBridge;
  private boolean running;
  private long now;
  private String fingerprint;
  private String nickname;
  private long firstSeenMillis;
  private long lastSeenMillis;
  private String address;
  private String orAddresses;
  private SortedSet<String> addressStrings;
  private long lastChangedAddressesMillis;
  private String diffOrAddresses;
  private String orAddressesAndPorts;
  private String version;
  private String[] recommendedVersions;
  private String networkStatusEntry;
  private String transports;
  private String distributionMethod;
  private String blocklist;
  private long bridgestrapLastEndMillis;
  private String flags;
  private SortedMap<String, Integer> lastBandwidthWeights;
  private SortedSet<TorVersion> lastRecommendedServerVersions;
  private String versionStatus;
  private int overloadRatelimitsVersion;
  private Timestamp overloadRatelimitsTimestamp;
  private int overloadRatelimitsRatelimit;
  private int overloadRatelimitsBurstlimit;
  private int overloadRatelimitsReadCount;
  private int overloadRatelimitsWriteCount;
  private int overloadFdExhaustedVersion;
  private Timestamp overloadFdExhaustedTimestamp;
  private String familyDigest;
  private String effectiveFamily;
  private String familyEntries;
  private String firstNetworkStatusEntryDigest;
  private String firstNetworkStatusDigest;
  private String lastNetworkStatusEntryDigest;
  private String lastNetworkStatusDigest;
  private String lastNetworkStatusWeightsId;
  private String lastNetworkStatusTotalsId;
  private String lastServerDescriptorDigest;
  private String lastExtraInfoDescriptorDigest;
  private String lastBridgestrapStatsDigest;
  private String lastBridgestrapTestDigest;
  private String lastBridgepoolAssignmentsFileDigest;
  private String lastBridgepoolAssignmentDigest;
  private SortedSet<String> verifiedHostNames;
  private SortedSet<String> unverifiedHostNames;
  private String exitPolicyLines;
  private String platform;
  private long overloadGeneralTimestamp;
  private int overloadGeneralVersion;
  private String contact;
  private long uptime;
  private long advertisedBandwidth;
  private long bandwidth;
  private float weightFraction;
  private String ipv6DefaultPolicy;
  private String ipv6PortList;
  private boolean isHibernating;
  private long bandwidthRate;
  private long bandwidthBurst;
  private long bandwidthObserved;
  private int socksPort;
  private int dirPort;
  private int orPort;
  private long lastServerDescriptorPublishedMillis;
  private boolean stale;

  /**
   * Checks if the router is a bridge.
   *
   * @return true if the router is a bridge, false otherwise.
   */
  public boolean isBridge() {
    return isTypeBridge;
  }

  /**
   * Sets the bridge status of the router.
   *
   * @param isTypeBridge The bridge status to set.
   */
  public void setType(boolean isTypeBridge) {
    this.isTypeBridge = isTypeBridge;
  }

  /**
   * Checks if the router is a running.
   *
   * @return true if the router is running, false otherwise.
   */
  public boolean isRunning() {
    return running;
  }

  /**
   * Sets the running status of the router.
   *
   * @param running The running status to set.
   */
  public void setRunning(boolean running) {
    this.running = running;
  }

  /**
   * Gets the current system time in milliseconds.
   *
   * @return The current system time in milliseconds.
   */
  public long getNow() {
    return now;
  }

  /**
   * Sets the current system time in milliseconds.
   */
  public void setNow(long timeNow) {
    this.now = timeNow;
  }

  /**
   * Gets the first seen timestamp in milliseconds of the first consensus
   * where the router has been found.
   *
   * @return The first seen timestamp in milliseconds.
   */
  public long getFirstSeenMillis() {
    return firstSeenMillis;
  }

  /**
   * Sets the first seen timestamp in milliseconds of the first consensus
   * where the router has been found.
   *
   * @param firstSeenMillis The first seen timestamp to set.
   */
  public void setFirstSeenMillis(long firstSeenMillis) {
    this.firstSeenMillis = firstSeenMillis;
  }

  /**
   * Gets the last seen timestamp in milliseconds of the last consensus
   * where the router has been found.
   *
   * @return The last seen timestamp in milliseconds.
   */
  public long getLastSeenMillis() {
    return lastSeenMillis;
  }

  /**
   * Sets the last seen timestamp in millisecondsof the last consensus
   * where the router has been found.
   *
   * @param lastSeenMillis The last seen timestamp to set.
   */
  public void setLastSeenMillis(long lastSeenMillis) {
    this.lastSeenMillis = lastSeenMillis;
  }

  public String getFingerprint() {
    return fingerprint;
  }

  public void setFingerprint(String fingerprint) {
    this.fingerprint = fingerprint;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public SortedSet<String> getAddressStrings() {
    return addressStrings;
  }

  public void setAddressStrings(SortedSet<String>  addressStrings) {
    this.addressStrings = addressStrings;
  }

  public String getExitPolicyLines() {
    return exitPolicyLines;
  }

  public void setExitPolicyLines(String exitPolicyLines) {
    this.exitPolicyLines = exitPolicyLines;
  }

  public String getContact() {
    return contact;
  }

  public void setContact(String contact) {
    this.contact = contact;
  }

  public String getPlatform() {
    return platform;
  }

  public void setPlatform(String platform) {
    this.platform = platform;
  }

  public int getOverloadGeneralVersion() {
    return overloadGeneralVersion;
  }

  public void setOverloadGeneralVersion(int overloadGeneralVersion) {
    this.overloadGeneralVersion = overloadGeneralVersion;
  }

  public long getOverloadGeneralTimestamp() {
    return overloadGeneralTimestamp;
  }

  public void setOverloadGeneralTimestamp(long overloadGeneralTimestamp) {
    this.overloadGeneralTimestamp = overloadGeneralTimestamp;
  }

  /**
   * Gets the OR (Onion Router) addresses.
   *
   * @return The OR addresses.
   */
  public String getOrAddresses() {
    return orAddresses;
  }

  /**
   * Sets the OR (Onion Router) addresses.
   *
   * @param orAddresses The OR addresses to set.
   */
  public void setOrAddresses(String orAddresses) {
    this.orAddresses = orAddresses;
  }

  /**
   * Gets the diff OR (Onion Router) addresses.
   *
   * @return The diff OR addresses.
   */
  public String getDiffOrAddresses() {
    return diffOrAddresses;
  }

  /**
   * Sets the diff OR (Onion Router) addresses.
   *
   * @param diffOrAddresses The diff in OR addresses to set.
   */
  public void setDiffOrAddresses(String diffOrAddresses) {
    this.diffOrAddresses = diffOrAddresses;
  }

  /**
   * Gets the timestamp of the last change in addresses in milliseconds.
   *
   * @return The timestamp of the last change in addresses.
   */
  public long getLastChangedAddressesMillis() {
    return lastChangedAddressesMillis;
  }

  /**
   * Sets the timestamp of the last change in addresses in milliseconds.
   *
   * @param lastChangedAddressesMillis The timestamp to set.
   */
  public void setLastChangedAddressesMillis(long lastChangedAddressesMillis) {
    this.lastChangedAddressesMillis = lastChangedAddressesMillis;
  }

  /**
   * Gets the differences in OR addresses.
   *
   * @return The differences in OR addresses.
   */
  public String getOrAddressesAndPorts() {
    return orAddressesAndPorts;
  }

  /**
   * Sets the OR addresses and ports.
   *
   * @param orAddressesAndPorts The OR addresses and ports to set.
   */
  public void setOrAddressesAndPorts(String orAddressesAndPorts) {
    this.orAddressesAndPorts = orAddressesAndPorts;
  }

  /**
   * Gets the version of the router software.
   *
   * @return a string representing the Tor version.
   */
  public String getVersion() {
    return version;
  }

  /**
   * Sets the version of the router software.
   *
   * @param version a string representing the Tor version.
   */
  public void setVersion(String version) {
    this.version = version;
  }

  /**
   * Gets the first network status entry digest.
   *
   * @return the first network status entry digest.
   */
  public String getFirstNetworkStatusEntryDigest() {
    return firstNetworkStatusEntryDigest;
  }

  /**
   * Sets the first network status.
   *
   * @param networkStatusEntry The network status to set.
   */
  public void setFirstNetworkStatusEntryDigest(String networkStatusEntry) {
    this.firstNetworkStatusEntryDigest = networkStatusEntry;
  }

  /**
   * Gets the last network status entry digest.
   *
   * @return the last network status entry digest.
   */
  public String getLastNetworkStatusEntryDigest() {
    return lastNetworkStatusEntryDigest;
  }

  /**
   * Sets the last network status.
   *
   * @param networkStatusEntry The network status to set.
   */
  public void setLastNetworkStatusEntryDigest(String networkStatusEntry) {
    this.lastNetworkStatusEntryDigest = networkStatusEntry;
  }

  /**
   * Gets the last network status entry.
   *
   * @return the last network status entry digest.
   */
  public String getLastNetworkStatusDigest() {
    return lastNetworkStatusDigest;
  }

  /**
   * Sets the last network status.
   *
   * @param networkStatusDigest The network status to set.
   */
  public void setLastNetworkStatusDigest(String networkStatusDigest) {
    this.lastNetworkStatusDigest = networkStatusDigest;
  }


  /**
   * Gets the transport methods used by the router.
   *
   * @return A string representing the transport methods.
   */
  public String getTransports() {
    return transports;
  }

  /**
   * Sets the transport methods used by the router.
   *
   * @param transports The transport methods to set.
   */
  public void setTransports(String transports) {
    this.transports = transports;
  }

  /**
   * Gets the distribution method of the router.
   *
   * @return A string representing the distribution method.
   */
  public String getDistributionMethod() {
    return distributionMethod;
  }

  /**
   * Sets the distribution method of the router.
   *
   * @param distributionMethod The distribution method to set.
   */
  public void setDistributionMethod(String distributionMethod) {
    this.distributionMethod = distributionMethod;
  }

  /**
   * Gets the blocklist status of the router.
   *
   * @return A string representing the blocklist status.
   */
  public String getBlocklist() {
    return blocklist;
  }

  /**
   * Sets the blocklist status of the router.
   *
   * @param blocklist The blocklist status to set.
   */
  public void setBlocklist(String blocklist) {
    this.blocklist = blocklist;
  }

  /**
   * Gets the time of the last bridgestrap test end in milliseconds.
   *
   * @return The time of the last bridgestrap test end in milliseconds.
   */
  public long getBridgestrapLastEndMillis() {
    return bridgestrapLastEndMillis;
  }

  /**
   * Sets the time of the last bridgestrap test end in milliseconds.
   *
   * @param bridgestrapLastEndMillis The time to set.
   */
  public void setBridgestrapLastEndMillis(long bridgestrapLastEndMillis) {
    this.bridgestrapLastEndMillis = bridgestrapLastEndMillis;
  }

  /**
   * Gets the flags associated with the router.
   *
   * @return A string representing the flags.
   */
  public String getFlags() {
    return flags;
  }

  /**
   * Sets the flags associated with the router.
   *
   * @param flags The flags to set.
   */
  public void setFlags(String flags) {
    this.flags = flags;
  }

  /**
   * Gets the last recorded bandwidth weights.
   *
   * @return The last bandwidth weights as a sorted map.
   */
  public SortedMap<String, Integer> getLastBandwidthWeights() {
    return lastBandwidthWeights;
  }

  /**
   * Sets the last recorded bandwidth weights.
   *
   * @param lastBandwidthWeights The bandwidth weights to set.
   */
  public void setLastBandwidthWeights(SortedMap<String, Integer>
      lastBandwidthWeights) {
    this.lastBandwidthWeights = lastBandwidthWeights;
  }

  /**
   * Gets the last recommended server versions.
   *
   * @return A sorted set of the last recommended server versions.
   */
  public SortedSet<TorVersion> getLastRecommendedServerVersions() {
    return lastRecommendedServerVersions;
  }

  /**
   * Sets the last recommended server versions.
   *
   */
  public void setLastRecommendedServerVersions() {
    SortedSet<TorVersion> lastRecommendedServerVersions = new TreeSet<>();
    if (recommendedVersions != null) {
      for (String recommendedServerVersion :
          recommendedVersions) {
        TorVersion recommendedTorServerVersion
            = TorVersion.of(recommendedServerVersion);
        if (null != recommendedTorServerVersion) {
          lastRecommendedServerVersions.add(recommendedTorServerVersion);
        }
      }
    }
    this.lastRecommendedServerVersions = lastRecommendedServerVersions;
  }

  /**
   * Gets the version of the overload rate limits.
   *
   * @return The version number of the overload rate limits.
   */
  public int getOverloadRatelimitsVersion() {
    return overloadRatelimitsVersion;
  }

  /**
   * Sets the version of the overload rate limits.
   *
   * @param overloadRatelimitsVersion The version number to set.
   */
  public void setOverloadRatelimitsVersion(int overloadRatelimitsVersion) {
    this.overloadRatelimitsVersion = overloadRatelimitsVersion;
  }

  /**
   * Gets the timestamp for the overload rate limits.
   *
   * @return The timestamp of the overload rate limits.
   */
  public Timestamp getOverloadRatelimitsTimestamp() {
    return overloadRatelimitsTimestamp;
  }

  /**
   * Sets the timestamp for the overload rate limits.
   *
   * @param overloadRatelimitsTimestamp The timestamp to set.
   */
  public void setOverloadRatelimitsTimestamp(
      Timestamp overloadRatelimitsTimestamp) {
    this.overloadRatelimitsTimestamp = overloadRatelimitsTimestamp;
  }

  /**
    * Gets the rate limit of the overload rate limits.
    *
    * @return The rate limit of the overload rate limits.
    */
  public int getOverloadRatelimitsRatelimit() {
    return overloadRatelimitsRatelimit;
  }


  /**
   * Sets the rate limit of the overload rate limits.
   *
   * @param overloadRatelimitsRatelimit The rate limit to set.
   */
  public void setOverloadRatelimitsRatelimit(
      int overloadRatelimitsRatelimit) {
    this.overloadRatelimitsRatelimit = overloadRatelimitsRatelimit;
  }

  /**
   * Gets the burst limit of the overload rate limits.
   *
   * @return The burst limit of the overload rate limits.
   */
  public int getOverloadRatelimitsBurstlimit() {
    return overloadRatelimitsBurstlimit;
  }

  /**
   * Sets the burst of the overload rate limits.
   *
   * @param overloadRatelimitsBurstlimit The burst limit to set.
   */
  public void setOverloadRatelimitsBurstlimit(
      int overloadRatelimitsBurstlimit) {
    this.overloadRatelimitsBurstlimit = overloadRatelimitsBurstlimit;
  }

  /**
   * Gets the read count of the overload rate limits.
   *
   * @return The read count of the overload rate limits.
   */
  public int getOverloadRatelimitsReadCount() {
    return overloadRatelimitsReadCount;
  }

  /**
   * Sets the read count of the overload rate limits.
   *
   * @param overloadRatelimitsReadCount The read count to set.
   */
  public void setOverloadRatelimitsReadCount(
      int overloadRatelimitsReadCount) {
    this.overloadRatelimitsReadCount = overloadRatelimitsReadCount;
  }

  /**
   * Gets the write count of the overload rate limits.
   *
   * @return The write count of the overload rate limits.
   */
  public int getOverloadRatelimitsWriteCount() {
    return overloadRatelimitsWriteCount;
  }

  /**
   * Sets the write count of the overload rate limits.
   *
   * @param overloadRatelimitsWriteCount The write count to set.
   */
  public void setOverloadRatelimitsWriteCount(
      int overloadRatelimitsWriteCount) {
    this.overloadRatelimitsWriteCount = overloadRatelimitsWriteCount;
  }

  /**
   * Gets the version of the overload fd exhausted.
   *
   * @return The version number of the overload fd exhausted.
   */
  public int getOverloadFdExhaustedVersion() {
    return overloadFdExhaustedVersion;
  }

  /**
   * Sets the version of the overload fd exhausted limits.
   *
   * @param overloadFdExhaustedVersion The version number to set.
   */
  public void setOverloadFdExhaustedVersion(int overloadFdExhaustedVersion) {
    this.overloadFdExhaustedVersion = overloadFdExhaustedVersion;
  }

  /**
   * Gets the timestamp for the overload fd exhausted.
   *
   * @return The timestamp of the overload fd exhausted.
   */
  public Timestamp getOverloadFdExhaustedTimestamp() {
    return overloadFdExhaustedTimestamp;
  }

  /**
   * Sets the timestamp for the overload fd exhausted.
   *
   * @param overloadFdExhaustedTimestamp The timestamp to set.
   */
  public void setOverloadFdExhaustedTimestamp(
      Timestamp overloadFdExhaustedTimestamp) {
    this.overloadFdExhaustedTimestamp = overloadFdExhaustedTimestamp;
  }

  /**
   * Gets the family digest.
   *
   * @return The family digest.
   */
  public String getFamilyDigest() {
    return familyDigest;
  }

  /**
   * Sets the family digest.
   *
   * @param familyDigest The family digest to set.
   */
  public void setFamilyDigest(String familyDigest) {
    this.familyDigest = familyDigest;
  }

  /**
   * Gets the effective family.
   *
   * @return The effective family.
   */
  public String getEffectiveFamily() {
    return effectiveFamily;
  }

  /**
   * Sets the effective family.
   *
   * @param effectiveFamily The effective family to set.
   */
  public void setEffectiveFamily(String effectiveFamily) {
    this.effectiveFamily = effectiveFamily;
  }

  /**
   * Gets the effective family.
   *
   * @return The effective family.
   */
  public String getFamilyEntries() {
    return familyEntries;
  }

  /**
   * Sets family entries.
   *
   * @param familyEntries The effective family to set.
   */
  public void setFamilyEntries(String familyEntries) {
    this.familyEntries = familyEntries;
  }

  /**
   * Gets the first network status digest.
   *
   * @return The first network status digest.
   */
  public String getFirstNetworkStatusDigest() {
    return firstNetworkStatusDigest;
  }

  /**
   * Sets the first network status digest.
   *
   * @param firstNetworkStatusDigest The first network status digest to set.
   */
  public void setFirstNetworkStatusDigest(String firstNetworkStatusDigest) {
    this.firstNetworkStatusDigest = firstNetworkStatusDigest;
  }

  public SortedSet<String> getVerifiedHostNames() {
    return verifiedHostNames;
  }

  public void setVerifiedHostNames(SortedSet<String> verifiedHostNames) {
    this.verifiedHostNames = verifiedHostNames;
  }

  public SortedSet<String> getUnverifiedHostNames() {
    return unverifiedHostNames;
  }

  public void setUnverifiedHostNames(SortedSet<String> unverifiedHostNames) {
    this.unverifiedHostNames = unverifiedHostNames;
  }

  public String getVersionStatus() {
    return versionStatus;
  }

  /**
   * Set the version status.
   */
  public void setVersionStatus() {
    String stringTorVersion = null;
    /* Extract tor software version  */
    if (version != null
        && version.startsWith("Tor ")) {
      stringTorVersion = version.split(" ")[1];
    }

    TorVersion torVersion = TorVersion.of(stringTorVersion);
    String versionStatus = "";

    versionStatus = (null != torVersion
        ? torVersion.determineVersionStatus(
            lastRecommendedServerVersions)
        : TorVersionStatus.UNRECOMMENDED).toString();
    this.versionStatus = versionStatus;
  }


  public String getLastNetworkStatusWeightsId() {
    return lastNetworkStatusWeightsId;
  }

  public void setLastNetworkStatusWeightsId(
      String lastNetworkStatusWeightsId) {
    this.lastNetworkStatusWeightsId = lastNetworkStatusWeightsId;
  }

  public String getLastNetworkStatusTotalsId() {
    return lastNetworkStatusTotalsId;
  }

  public void setLastNetworkStatusTotalsId(
      String lastNetworkStatusTotalsId) {
    this.lastNetworkStatusTotalsId = lastNetworkStatusTotalsId;
  }


  public String getLastServerDescriptorDigest() {
    return lastServerDescriptorDigest;
  }

  public void setLastServerDescriptorDigest(
      String lastServerDescriptorDigest) {
    this.lastServerDescriptorDigest = lastServerDescriptorDigest;
  }

  public String getLastExtraInfoDescriptorDigest() {
    return lastExtraInfoDescriptorDigest;
  }

  public void setLastExtraInfoDescriptorDigest(
      String lastExtraInfoDescriptorDigest) {
    this.lastExtraInfoDescriptorDigest = lastExtraInfoDescriptorDigest;
  }

  public String getLastBridgestrapStatsDigest() {
    return lastBridgestrapStatsDigest;
  }

  public void setLastBridgestrapStatsDigest(
      String lastBridgestrapStatsDigest) {
    this.lastBridgestrapStatsDigest = lastBridgestrapStatsDigest;
  }

  public String getLastBridgestrapTestDigest() {
    return lastBridgestrapTestDigest;
  }

  public void setLastBridgestrapTestDigest(
      String lastBridgestrapTestDigest) {
    this.lastBridgestrapTestDigest = lastBridgestrapTestDigest;
  }

  public String getLastBridgepoolAssignmentsFileDigest() {
    return lastBridgepoolAssignmentsFileDigest;
  }

  public void setLastBridgepoolAssignmentsFileDigest(
      String lastBridgepoolAssignmentsFileDigest) {
    this.lastBridgepoolAssignmentsFileDigest =
        lastBridgepoolAssignmentsFileDigest;
  }

  public String getLastBridgepoolAssignmentDigest() {
    return lastBridgepoolAssignmentDigest;
  }

  public void setLastBridgepoolAssignmentDigest(
      String lastBridgepoolAssignmentDigest) {
    this.lastBridgepoolAssignmentDigest = lastBridgepoolAssignmentDigest;
  }

  /**
   * Compute the bridgeServerVersion.
   *
   * @param platform the bridge platform string.
   */
  public String getBridgeServerVersion(String platform) {
    if (platform != null) {
      String[] platformParts =
        platform.split(" on ");
      return platformParts[0].toLowerCase();
    }
    return null;
  }

  /**
   * Compute the server recommendedVersions.
   *
   * @param versions.
   */
  public String[] computeRecommendedVersions(String versions) {
    if (versions != null) {
      return versions.split(" ")[1].split(",");
    }
    return null;
  }

  public String[] getRecommendedVersions() {
    return this.recommendedVersions;
  }

  public void setRecommendedVersions(String[] recommendedVersions) {

    this.recommendedVersions = recommendedVersions;
  }

  public long getUptime() {
    return uptime;
  }

  public void setUptime(long uptime) {
    this.uptime = uptime;
  }

  /**
   * Get the server advertisedBandwidth.
   */
  public long getAdvertisedBandwidth() {
    return advertisedBandwidth;
  }

  /**
   * Set the server advertisedBandwidth obtained by parsing the three
   * values from the "bandwidth" line in a server descriptor. These
   * values stand for the average bandwidth, burst bandwidth, and
   * observed bandwidth. The advertised bandwidth is the minimum of
   * these values.
   *
   * @param bw the server's advertised bandwidth.
   */
  public void setAdvertisedBandwidth(long bw) {
    this.advertisedBandwidth = bw;
  }

  /**
   * Get the server bandwidth.
   */
  public long getBandwidth() {
    return bandwidth;
  }

  /**
   * Set the server bandwidth as it is parsed from the latest
   * consensus entry.
   *
   * @param bw the router bandwidth.
   */
  public void setBandwidth(long bw) {
    this.bandwidth = bw;
  }

  /**
   * Get the server network weight fraction.
   */
  public float getWeightFraction() {
    return weightFraction;
  }

  /**
   * Set the server network weight fraction parsed from the
   * latest consensus entry.
   *
   * @param weight the weight network weight fraction of the router.
   */
  public void setWeightFraction(float weight) {
    this.weightFraction = weight;
  }

  public void setIpv6DefaultPolicy(String policy) {
    this.ipv6DefaultPolicy = policy;
  }

  public String getIpv6DefaultPolicy() {
    return ipv6DefaultPolicy;
  }

  public void setIpv6PortList(String portList) {
    this.ipv6PortList = portList;
  }

  public String getIpv6PortList() {
    return ipv6PortList;
  }

  public void setIsHibernating(boolean isHibernating) {
    this.isHibernating = isHibernating;
  }

  public boolean getIsHibernating() {
    return isHibernating;
  }

  public void setBandwidthRate(long bandwidthRate) {
    this.bandwidthRate = bandwidthRate;
  }

  public long getBandwidthRate() {
    return bandwidthRate;
  }

  public void setBandwidthBurst(long bandwidthBurst) {
    this.bandwidthBurst = bandwidthBurst;
  }

  public long getBandwidthBurst() {
    return bandwidthBurst;
  }

  public void setBandwidthObserved(long bandwidthObserved) {
    this.bandwidthObserved = bandwidthObserved;
  }

  public long getBandwidthObserved() {
    return bandwidthObserved;
  }

  public void setSocksPort(int port) {
    this.socksPort = port;
  }

  public int getSocksPort() {
    return socksPort;
  }

  public void setDirPort(int port) {
    this.dirPort = port;
  }

  public int getDirPort() {
    return dirPort;
  }

  public void setOrPort(int port) {
    this.orPort = port;
  }

  public int getOrPort() {
    return orPort;
  }

  public long getLastServerDescriptorPublishedMillis() {
    return lastServerDescriptorPublishedMillis;
  }

  public void setLastServerDescriptorPublishedMillis(long millis) {
    this.lastServerDescriptorPublishedMillis = millis;
  }

  public boolean isStale() {
    return stale;
  }

  public void setStaleStatus(boolean status) {
    this.stale = status;
  }

}
