package org.torproject.metrics.descriptorparser.impl;

// Class to store bandwidth entries by timestamp
public class BwEntry {
  private final long timestamp;
  private Double writeValue;
  private Double readValue;
  private Double ipv6WriteValue;
  private Double ipv6ReadValue;
  private Double dirreqWriteValue;
  private Double dirreqReadValue;
  
  public BwEntry(long timestamp) {
    this.timestamp = timestamp;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public Double getWriteValue() {
    return writeValue;
  }

  public void setWriteValue(Double writeValue) {
    this.writeValue = writeValue;
  }

  public Double getReadValue() {
    return readValue;
  }

  public void setReadValue(Double readValue) {
    this.readValue = readValue;
  }

  public Double getIpv6WriteValue() {
    return ipv6WriteValue;
  }

  public void setIpv6WriteValue(Double ipv6WriteValue) {
    this.ipv6WriteValue = ipv6WriteValue;
  }

  public Double getIpv6ReadValue() {
    return ipv6ReadValue;
  }

  public void setIpv6ReadValue(Double ipv6ReadValue) {
    this.ipv6ReadValue = ipv6ReadValue;
  }

  public Double getDirreqWriteValue() {
    return dirreqWriteValue;
  }

  public void setDirreqWriteValue(Double dirreqWriteValue) {
    this.dirreqWriteValue = dirreqWriteValue;
  }

  public Double getDirreqReadValue() {
    return dirreqReadValue;
  }

  public void setDirreqReadValue(Double dirreqReadValue) {
    this.dirreqReadValue = dirreqReadValue;
  }
}
