package org.torproject.metrics.descriptorparser.parsers;


import org.torproject.descriptor.Descriptor;
import org.torproject.descriptor.DescriptorParseException;
import org.torproject.descriptor.DescriptorReader;
import org.torproject.descriptor.DescriptorSourceFactory;
import org.torproject.descriptor.NetworkStatusEntry;
import org.torproject.descriptor.RelayNetworkStatusConsensus;
import org.torproject.metrics.descriptorparser.utils.DescriptorUtils;
import org.torproject.metrics.descriptorparser.utils.Gauge;
import org.torproject.metrics.descriptorparser.utils.OpenMetricsWriter;

import io.prometheus.client.CollectorRegistry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ConsensusParser {
  private static final Logger logger = LoggerFactory.getLogger(
      ConsensusParser.class);

  private static final String INSERT_NETWORK_STATUS_SQL
      = "INSERT INTO"
      + " network_status (header, network_status_version, vote_status,"
      + " consensus_method, consensus_flavor, valid_after, fresh_until,"
      + " valid_until, vote_seconds, dist_seconds, known_flags,"
      + " recommended_client_versions, recommended_server_versions,"
      + " recommended_client_protocols, recommended_relay_protocols,"
      + " required_client_protocols, required_relay_protocols,"
      + " params, package_lines, shared_rand_previous_value,"
      + " shared_rand_current_value,"
      + " shared_rand_previous_num, shared_rand_current_num, dir_sources,"
      + " digest, bandwidth_weights, directory_signatures) VALUES "
      + "(?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final String INSERT_NETWORK_STATUS_TOTALS_SQL
      = "INSERT INTO"
      + " network_status_totals (total_consensus_weight, total_guard_weight,"
      + " total_middle_weight, total_exit_weight, published, network_status)"
      + " VALUES (?, ?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final String INSERT_NETWORK_STATUS_ENTRY_SQL
      = "INSERT INTO network_status_entry"
      + " (nickname, fingerprint, digest, published, ip, or_port,"
      + " dir_port, or_addresses, flags, version,"
      + " bandwidth_unmeasured, bandwidth_weight, proto, policy,"
      + " port_list, flavor, microdescriptor_digest,"
      + " server_descriptor_digest) VALUES "
      + "(?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final String INSERT_NETWORK_STATUS_ENTRY_WEIGHTS_SQL
      = "INSERT INTO network_status_entry_weights"
      + " (guard_weight, middle_weight, exit_weight, guard_weight_fraction,"
      + " middle_weight_fraction, exit_weight_fraction,"
      + " network_weight_fraction, published, network_status_entry) VALUES "
      + "(?, ?, ?, ?, ?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final String INSERT_NETWORK_STATUS_DOCUMENT_SQL
      = "INSERT INTO network_status_document"
      + " (network_status_digest,"
      + " network_status_entry_digest)"
      + " VALUES "
      + "(?, ?) ON CONFLICT DO NOTHING;";

  private static CollectorRegistry registry = new CollectorRegistry();
  private static OpenMetricsWriter opWriter = new OpenMetricsWriter();

  private static final long ONE_HOUR_MILLIS = 60L * 60L * 1000L;

  private static final long ONE_DAY_MILLIS = 24L * ONE_HOUR_MILLIS;

  private static final long ONE_WEEK_MILLIS = 7L * ONE_DAY_MILLIS;

  private static Gauge bwGauge = Gauge.build()
      .name("relay_bandwidth")
      .help("The measured bandwidth in bytes per second.")
      .labelNames("fingerprint", "nickname").register(registry);

  private static Gauge descStatusGauge = Gauge.build()
      .name("desc_status")
      .help("Estimated number of intervals when the node was listed as "
          + "running in the network status published by either the "
          + "directory authorities or bridge authority")
      .labelNames("fingerprint", "nickname", "node", "country",
          "transport", "version").register(registry);

  private static Gauge networkWeightGauge = Gauge.build()
      .name("network_weight")
      .help("Metric of a relay that is based on bandwidth observed "
          + "by the relay and bandwidth measured by the directory "
          + "authorities.")
      .labelNames("fingerprint", "nickname").register(registry);

  private static Gauge networkFractionGauge = Gauge.build()
      .name("network_fraction")
      .help("Metric of a relay that is based on bandwidth observed "
          + "by the relay and bandwidth measured by the directory "
          + "authorities as a fraction of the total network weight.")
      .labelNames("fingerprint", "nickname").register(registry);

  private static Gauge totalNetworkWeightGauge = Gauge.build()
      .name("total_network_weight")
      .help("Metric based on bandwidth observed by relays "
          + "and bandwidth measured by the directory "
          + "authorities.").register(registry);

  private static Gauge networkGuardWeightGauge = Gauge.build()
      .name("network_guard_weight")
      .help("Metric of a guard relay that is based on bandwidth observed "
          + "by the relay and bandwidth measured by the directory "
          + "authorities.")
      .labelNames("fingerprint", "nickname").register(registry);

  private static Gauge networkGuardFractionGauge = Gauge.build()
      .name("network_guard_fraction")
      .help("Metric of a guard relay that is based on bandwidth observed "
          + "by the relay and bandwidth measured by the directory "
          + "authorities as a fraction of the total network weight.")
      .labelNames("fingerprint", "nickname").register(registry);

  private static Gauge totalNetworkGuardWeightGauge = Gauge.build()
      .name("total_network_guard_weight")
      .help("Metric based on bandwidth observed by guard relays "
          + "and bandwidth measured by the directory "
          + "authorities.").register(registry);

  private static Gauge networkMiddleWeightGauge = Gauge.build()
      .name("network_middle_weight")
      .help("Metric of a middle relay that is based on bandwidth observed "
          + "by the relay and bandwidth measured by the directory "
          + "authorities.")
      .labelNames("fingerprint", "nickname").register(registry);

  private static Gauge networkMiddleFractionGauge = Gauge.build()
      .name("network_middle_fraction")
      .help("Metric of a middle relay that is based on bandwidth observed "
          + "by the relay and bandwidth measured by the directory "
          + "authorities as a fraction of the total network weight.")
      .labelNames("fingerprint", "nickname").register(registry);

  private static Gauge totalNetworkMiddleWeightGauge = Gauge.build()
      .name("total_network_middle_weight")
      .help("Metric based on bandwidth observed by middle relays "
          + "and bandwidth measured by the directory "
          + "authorities.").register(registry);

  private static Gauge networkExitWeightGauge = Gauge.build()
      .name("network_exit_weight")
      .help("Metric of a exit relay that is based on bandwidth observed "
          + "by the relay and bandwidth measured by the directory "
          + "authorities.")
      .labelNames("fingerprint", "nickname").register(registry);

  private static Gauge networkExitFractionGauge = Gauge.build()
      .name("network_exit_fraction")
      .help("Metric of a exit relay that is based on bandwidth observed "
          + "by the relay and bandwidth measured by the directory "
          + "authorities as a fraction of the total network weight.")
      .labelNames("fingerprint", "nickname").register(registry);

  private static Gauge totalNetworkExitWeightGauge = Gauge.build()
      .name("total_network_exit_weight")
      .help("Metric based on bandwidth observed by exit relays "
          + "and bandwidth measured by the directory "
          + "authorities.").register(registry);

  private static final Set<String> WEIGHT_KEYS = new HashSet<>(Arrays.asList(
      "Wgg", "Wgd", "Wmg", "Wmm", "Wme", "Wmd", "Wee", "Wed"));

  private static Map<String, ArrayList>
        relayWeights = new HashMap<>();

  private static Map<String, ArrayList>
        relayWeightsFractions = new HashMap<>();

  private double totalConsensusWeight = 0.0;
  private double totalGuardWeight = 0.0;
  private double totalMiddleWeight = 0.0;
  private double totalExitWeight = 0.0;

  /**
   * Parse consensus files and add fields to the database.
   */
  public void run(String path, Connection conn) throws Exception {
    DescriptorUtils descUtils = new DescriptorUtils();
    // Read descriptors from disk.
    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();

    for (Descriptor descriptor : descriptorReader.readDescriptors(
        new File(path))) {
      if (descriptor instanceof RelayNetworkStatusConsensus) {
        RelayNetworkStatusConsensus desc =
            (RelayNetworkStatusConsensus) descriptor;

        double wgg = 0.0;
        double wgd = 0.0;
        double wmg = 0.0;
        double wme = 0.0;
        double wmm = 0.0;
        double wee = 0.0;
        double wmd = 0.0;
        double wed = 0.0;

        boolean consensusContainsBandwidthWeights = false;
        if (desc.getBandwidthWeights() != null
            && (desc.getBandwidthWeights().keySet().containsAll(WEIGHT_KEYS))) {
          consensusContainsBandwidthWeights = true;

          wgg = ((double) desc.getBandwidthWeights().get("Wgg")) / 10000.0;
          wgd = ((double) desc.getBandwidthWeights().get("Wgd")) / 10000.0;
          wmg = ((double) desc.getBandwidthWeights().get("Wmg")) / 10000.0;
          wme = ((double) desc.getBandwidthWeights().get("Wme")) / 10000.0;
          wmm = ((double) desc.getBandwidthWeights().get("Wmm")) / 10000.0;
          wee = ((double) desc.getBandwidthWeights().get("Wee")) / 10000.0;
          wmd = ((double) desc.getBandwidthWeights().get("Wmd")) / 10000.0;
          wed = ((double) desc.getBandwidthWeights().get("Wed")) / 10000.0;
        } else {
          logger.debug("Not calculating new path selection probabilities, "
              + "because we could not determine most recent Wxx parameter "
              + "values, probably because we didn't parse a consensus in "
              + "this execution.");
          continue;
        }

        for (Map.Entry<String, NetworkStatusEntry> e :
            desc.getStatusEntries().entrySet()) {
          String fingerprint = e.getKey();
          NetworkStatusEntry entry = e.getValue();
          boolean isRunning = entry.getFlags().contains("Running");
          double guardWeight = 0.0;
          double middleWeight = 0.0;
          double exitWeight = 0.0;
          double consensusWeight = entry.getBandwidth();
          if (isRunning) {
            boolean isExit = entry.getFlags().contains("Exit")
                && !entry.getFlags().contains("BadExit");
            boolean isGuard = entry.getFlags().contains("Guard");
            this.totalConsensusWeight += consensusWeight;
            if (consensusContainsBandwidthWeights) {
              if (isGuard && isExit) {
                guardWeight = consensusWeight * wgd;
                middleWeight = consensusWeight * wmd;
                exitWeight = consensusWeight * wed;
              } else if (isGuard) {
                guardWeight = consensusWeight * wgg;
                middleWeight = consensusWeight * wmg;
              } else if (isExit) {
                middleWeight = consensusWeight * wme;
                exitWeight = consensusWeight * wee;
              } else {
                middleWeight = consensusWeight * wmm;
              }

            }
            this.totalGuardWeight += guardWeight;
            this.totalMiddleWeight += middleWeight;
            this.totalExitWeight += exitWeight;

            ArrayList tuple = new ArrayList<>();
            tuple.add(consensusWeight);
            tuple.add(guardWeight);
            tuple.add(middleWeight);
            tuple.add(exitWeight);

            this.relayWeights.put(fingerprint, tuple);
          }

        }

        for (Map.Entry<String, ArrayList> e :
            this.relayWeights.entrySet()) {
          String fingerprint = e.getKey();
          ArrayList entry = e.getValue();
          ArrayList tuple = new ArrayList<>();

          double consensusWeight = (double) entry.get(0);
          double guardWeight = (double) entry.get(1);
          double middleWeight = (double) entry.get(2);
          double exitWeight = (double) entry.get(3);

          if (consensusContainsBandwidthWeights) {

            tuple.add((float) (consensusWeight / totalConsensusWeight));
            tuple.add((float) (guardWeight / totalGuardWeight));
            tuple.add((float) (middleWeight / totalMiddleWeight));
            tuple.add((float) (exitWeight / totalExitWeight));
          } else {
            tuple.add((float) consensusWeight / totalConsensusWeight);
            tuple.add((float) 0.0);
            tuple.add((float) 0.0);
            tuple.add((float) 0.0);
          }
          this.relayWeightsFractions.put(
              fingerprint, tuple);
        }

        String digest = descUtils.calculateDigestSha256Base64(
            desc.getRawDescriptorBytes());

        this.addNetworkStatus(desc, digest, conn, totalConsensusWeight,
            totalGuardWeight, totalMiddleWeight, totalExitWeight);

        try (
          PreparedStatement preparedStatusStatement = conn.prepareStatement(
              INSERT_NETWORK_STATUS_ENTRY_SQL);
          PreparedStatement preparedWeightsStatement = conn.prepareStatement(
              INSERT_NETWORK_STATUS_ENTRY_WEIGHTS_SQL);
          PreparedStatement preparedDocStatement = conn.prepareStatement(
              INSERT_NETWORK_STATUS_DOCUMENT_SQL);
        ) {
          for (Map.Entry<String, NetworkStatusEntry> e :
              desc.getStatusEntries().entrySet()) {
            NetworkStatusEntry entry = e.getValue();
            String flavor = "unflavored";
            if (desc.getConsensusFlavor() != null) {
              flavor = "microdesc";
            }
            preparedStatusStatement.setString(1, entry.getNickname());
            preparedStatusStatement.setString(2, entry.getFingerprint());
            String entryDigest = descUtils.calculateDigestSha256Base64(
                entry.getStatusEntryBytes());
            preparedStatusStatement.setString(3, entryDigest);
            preparedStatusStatement.setTimestamp(4,
                new Timestamp(entry.getPublishedMillis()));
            preparedStatusStatement.setString(5, entry.getAddress());
            preparedStatusStatement.setInt(6, entry.getOrPort());
            preparedStatusStatement.setInt(7, entry.getDirPort());
            preparedStatusStatement.setString(8,
                entry.getOrAddresses().toString());
            preparedStatusStatement.setString(9,
                descUtils.fieldAsString(entry.getFlags()));
            preparedStatusStatement.setString(10, entry.getVersion());
            preparedStatusStatement.setBoolean(11, entry.getUnmeasured());
            preparedStatusStatement.setLong(12, entry.getBandwidth());
            preparedStatusStatement.setString(13,
                descUtils.fieldAsString(entry.getProtocols()));
            preparedStatusStatement.setString(14, entry.getDefaultPolicy());
            preparedStatusStatement.setString(15, entry.getPortList());
            preparedStatusStatement.setString(16, flavor);
            preparedStatusStatement.setString(17, descUtils.fieldAsString(
                entry.getMicrodescriptorDigestsSha256Base64()));
            preparedStatusStatement.setString(18, entry.getDescriptor());
            preparedStatusStatement.addBatch();

            preparedDocStatement.setString(1, digest);
            preparedDocStatement.setString(2, entryDigest);
            preparedDocStatement.addBatch();
            if (desc.getConsensusFlavor() != "microdesc") {
              String fingerprint = e.getKey();
              ArrayList weightsFractions =
                      this.relayWeightsFractions.get(fingerprint);
              ArrayList weights = this.relayWeights.get(fingerprint);
              double guardWeight = (double) weights.get(1);
              double middleWeight = (double) weights.get(2);
              double exitWeight = (double) weights.get(3);
              float networkFraction = (float) weightsFractions.get(0);
              float guardFraction = (float) weightsFractions.get(1);
              float middleFraction = (float) weightsFractions.get(2);
              float exitFraction = (float) weightsFractions.get(3);
              preparedWeightsStatement.setDouble(1, guardWeight);
              preparedWeightsStatement.setDouble(2, middleWeight);
              preparedWeightsStatement.setDouble(3, exitWeight);
              preparedWeightsStatement.setFloat(4, guardFraction);
              preparedWeightsStatement.setFloat(5, middleFraction);
              preparedWeightsStatement.setFloat(6, exitFraction);
              preparedWeightsStatement.setFloat(7, networkFraction);
              preparedWeightsStatement.setTimestamp(8,
                      new Timestamp(entry.getPublishedMillis()));
              preparedWeightsStatement.setString(9, entryDigest);
              preparedWeightsStatement.addBatch();

              opWriter.processMetrics(this.bwGauge,
                      entry.getPublishedMillis(), entry.getBandwidth(),
                      entry.getFingerprint(), entry.getNickname());
              if (entry.getFlags().contains("Running")) {
                opWriter.processMetrics(this.descStatusGauge,
                        entry.getPublishedMillis(), 0.0,
                        entry.getFingerprint(), entry.getNickname(),
                        "relay", "", "", "");
                opWriter.processMetrics(this.networkWeightGauge,
                        entry.getPublishedMillis(), entry.getBandwidth(),
                        entry.getFingerprint(), entry.getNickname());
                opWriter.processMetrics(this.networkGuardWeightGauge,
                        entry.getPublishedMillis(), guardWeight,
                        entry.getFingerprint(), entry.getNickname());
                opWriter.processMetrics(this.networkMiddleWeightGauge,
                        entry.getPublishedMillis(), middleWeight,
                        entry.getFingerprint(), entry.getNickname());
                opWriter.processMetrics(this.networkExitWeightGauge,
                        entry.getPublishedMillis(), exitWeight,
                        entry.getFingerprint(), entry.getNickname());
                opWriter.processMetrics(this.networkFractionGauge,
                        entry.getPublishedMillis(), networkFraction,
                        entry.getFingerprint(), entry.getNickname());
                opWriter.processMetrics(this.networkGuardFractionGauge,
                        entry.getPublishedMillis(), guardFraction,
                        entry.getFingerprint(), entry.getNickname());
                opWriter.processMetrics(this.networkMiddleFractionGauge,
                        entry.getPublishedMillis(), middleFraction,
                        entry.getFingerprint(), entry.getNickname());
                opWriter.processMetrics(this.networkExitFractionGauge,
                        entry.getPublishedMillis(), exitFraction,
                        entry.getFingerprint(), entry.getNickname());
              }
            }

          }
          preparedStatusStatement.executeBatch();
          preparedWeightsStatement.executeBatch();
          preparedDocStatement.executeBatch();

        } catch (SQLException ex) {
          logger.warn("SQL Exception. {}".format(ex.getMessage()));
        } catch (DescriptorParseException ex) {
          logger.warn("Parsing Exception. {}".format(ex.getMessage()));
        } catch (Exception ex) {
          logger.warn("Exception. {}".format(ex.getMessage()));
        }
      } else {
        continue;
      }
    }
    try {
      opWriter.pushToGateway(registry);
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }

  private void addNetworkStatus(RelayNetworkStatusConsensus desc, String digest,
      Connection conn, double totalConsensusWeight, double totalGuardWeight,
      double totalMiddleWeight, double totalExitWeight) {
    DescriptorUtils descUtils = new DescriptorUtils();
    try (
      PreparedStatement preparedStatement = conn.prepareStatement(
          INSERT_NETWORK_STATUS_SQL);
    ) {
      String consensusType = null;
      if (desc.getConsensusFlavor() != null ) {
        consensusType = "@type network-status-"
            + desc.getConsensusFlavor()
            + "-consensus-";
      } else {
        consensusType = "@type network-status-consensus-";
      }
      consensusType += String.valueOf(desc.getNetworkStatusVersion());
      consensusType += " 1.0";
      preparedStatement.setString(1, consensusType);
      preparedStatement.setInt(2, desc.getNetworkStatusVersion());
      preparedStatement.setString(3, "consensus");
      preparedStatement.setInt(4, desc.getConsensusMethod());
      if (desc.getConsensusFlavor() != null) {
        preparedStatement.setString(5, desc.getConsensusFlavor());
      } else {
        preparedStatement.setString(5, "unflavored");
      }
      preparedStatement.setTimestamp(6,
          new Timestamp(desc.getValidAfterMillis()));
      preparedStatement.setTimestamp(7,
          new Timestamp(desc.getFreshUntilMillis()));
      preparedStatement.setTimestamp(8,
          new Timestamp(desc.getValidUntilMillis()));
      preparedStatement.setLong(9, desc.getVoteSeconds());
      preparedStatement.setLong(10, desc.getDistSeconds());
      preparedStatement.setString(11,
          descUtils.fieldAsString(desc.getKnownFlags()));
      preparedStatement.setString(12,
          String.join(", ", desc.getRecommendedClientVersions()));
      preparedStatement.setString(13,
          String.join(", ", desc.getRecommendedServerVersions()));
      preparedStatement.setString(14,
          descUtils.fieldAsString(desc.getRecommendedClientProtocols()));
      preparedStatement.setString(15,
          descUtils.fieldAsString(desc.getRecommendedRelayProtocols()));
      preparedStatement.setString(16,
          descUtils.fieldAsString(desc.getRequiredClientProtocols()));
      preparedStatement.setString(17,
          descUtils.fieldAsString(desc.getRequiredRelayProtocols()));
      preparedStatement.setString(18,
          descUtils.fieldAsString(desc.getConsensusParams()));
      preparedStatement.setString(19,
          descUtils.fieldAsString(desc.getPackageLines()));
      preparedStatement.setString(20,
          desc.getSharedRandPreviousValue());
      preparedStatement.setString(21,
          desc.getSharedRandCurrentValue());
      preparedStatement.setInt(22,
          desc.getSharedRandPreviousNumReveals());
      preparedStatement.setInt(23,
          desc.getSharedRandCurrentNumReveals());
      preparedStatement.setString(24,
          descUtils.fieldAsString(desc.getDirSourceEntries()));
      preparedStatement.setString(25, digest);
      preparedStatement.setString(26,
          descUtils.fieldAsString(desc.getBandwidthWeights()));
      preparedStatement.setString(27,
          descUtils.fieldAsString(desc.getSignatures()));
      preparedStatement.executeUpdate();

    } catch (SQLException ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }

    try (
      PreparedStatement preparedStatement = conn.prepareStatement(
          INSERT_NETWORK_STATUS_TOTALS_SQL);
    ) {
      preparedStatement.setDouble(1, totalConsensusWeight);
      preparedStatement.setDouble(2, totalGuardWeight);
      preparedStatement.setDouble(3, totalMiddleWeight);
      preparedStatement.setDouble(4, totalExitWeight);
      preparedStatement.setTimestamp(5,
          new Timestamp(desc.getValidAfterMillis()));
      preparedStatement.setString(6, digest);
      preparedStatement.executeUpdate();
    }  catch (SQLException ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }

    try {
      opWriter.processMetrics(this.totalNetworkWeightGauge,
          desc.getValidAfterMillis(), totalConsensusWeight);
      opWriter.processMetrics(this.totalNetworkGuardWeightGauge,
          desc.getValidAfterMillis(), totalGuardWeight);
      opWriter.processMetrics(this.totalNetworkMiddleWeightGauge,
          desc.getValidAfterMillis(), totalMiddleWeight);
      opWriter.processMetrics(this.totalNetworkExitWeightGauge,
          desc.getValidAfterMillis(), totalExitWeight);
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }
}
