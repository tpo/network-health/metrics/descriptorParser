package org.torproject.metrics.descriptorparser.parsers;

import org.torproject.descriptor.BridgeServerDescriptor;
import org.torproject.descriptor.Descriptor;
import org.torproject.descriptor.DescriptorReader;
import org.torproject.descriptor.DescriptorSourceFactory;
import org.torproject.descriptor.RelayServerDescriptor;
import org.torproject.descriptor.ServerDescriptor;
import org.torproject.metrics.descriptorparser.utils.DescriptorUtils;
import org.torproject.metrics.descriptorparser.utils.Gauge;
import org.torproject.metrics.descriptorparser.utils.OpenMetricsWriter;

import io.prometheus.client.CollectorRegistry;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;

public class ServerDescriptorParser {
  private boolean isBridge = true;

  private static final String INSERT_SERVER_SQL
      = "INSERT INTO server_descriptor"
      + " (is_bridge, published, nickname, fingerprint,"
      + " digest_sha1_hex, identity_ed25519, master_key_ed25519, address,"
      + " or_port, socks_port, dir_port, or_addresses,"
      + " bandwidth_avg, bandwidth_burst, bandwidth_observed, platform,"
      + " overload_general_timestamp, overload_general_version,"
      + " protocols, is_hibernating,"
      + " uptime, onion_key, signing_key, exit_policy,"
      + " contact, bridge_distribution_request, family, read_history,"
      + " write_history, uses_enhanced_dns_logic,"
      + " caches_extra_info, extra_info_sha1_digest,"
      + " extra_info_sha256_digest, is_hidden_service_dir,"
      + " link_protocol_version, circuit_protocol_version,"
      + " allow_single_hop_exits, ipv6_default_policy,"
      + " ipv6_port_list, ntor_onion_key,"
      + " onion_key_cross_cert, ntor_onion_key_cross_cert,"
      + " ntor_onion_key_cross_cert_sign, tunnelled_dir_server,"
      + " router_sig_ed25519, router_signature, header,"
      + " digest_sha256) VALUES"
      + "(?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final Logger logger = LoggerFactory.getLogger(
      ServerDescriptorParser.class);

  private static OpenMetricsWriter opWriter = new OpenMetricsWriter();
  private static CollectorRegistry registry = new CollectorRegistry();

  private static Gauge descBwAvgGauge = Gauge.build()
      .name("desc_bw_avg")
      .help("Estimated average bandwidth the OR is willing to "
          + "sustain over long period. In bytes per second.")
      .labelNames("fingerprint", "nickname", "node").register(registry);

  private static Gauge descBwBurGauge = Gauge.build()
      .name("desc_bw_bur")
      .help("Estimated average bandwidth the OR is willing to "
          + "sustain over short intervals. In bytes per second.")
      .labelNames("fingerprint", "nickname", "node").register(registry);

  private static Gauge descBwObsGauge = Gauge.build()
      .name("desc_bw_obs")
      .help("Estimated bandwidth capacity this relay can handle. "
          + "In bytes per second.")
      .labelNames("fingerprint", "nickname", "node").register(registry);

  private static Gauge descBwAdvGauge = Gauge.build()
      .name("desc_bw_adv")
      .help("Estimated advertised bandwidth this relay can handle. "
          + "In bytes per second.")
      .labelNames("fingerprint", "nickname", "node").register(registry);

  /**
   * Parse server descriptors and add fields to the database.
   */
  public void run(String path, Connection conn) throws Exception {
    // Read descriptors from disk.
    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();

    for (Descriptor descriptor :
        descriptorReader.readDescriptors(new File(path))) {
      if ((descriptor instanceof RelayServerDescriptor)
          || (descriptor instanceof BridgeServerDescriptor)) {
        ServerDescriptor desc;
        String node = "bridge";
        if (descriptor instanceof RelayServerDescriptor) {
          desc = (RelayServerDescriptor) descriptor;
          this.isBridge = false;
          node = "relay";
        } else {
          desc = (BridgeServerDescriptor) descriptor;
          this.isBridge = true;
        }

        long advertisedBandwidth = Math.min(Math.min(
            desc.getBandwidthRate(),
            desc.getBandwidthBurst()),
            desc.getBandwidthObserved());

        long publishedMillis = desc.getPublishedMillis();
        String fingerprint = desc.getFingerprint().toUpperCase();
        String nickname = desc.getNickname();

        try {
          opWriter.processMetrics(descBwAvgGauge, publishedMillis,
              desc.getBandwidthRate(), fingerprint,
              nickname, node);

          opWriter.processMetrics(descBwBurGauge, publishedMillis,
              desc.getBandwidthBurst(), fingerprint,
              nickname, node);

          opWriter.processMetrics(descBwObsGauge, publishedMillis,
              desc.getBandwidthObserved(), fingerprint,
              nickname, node);

          opWriter.processMetrics(descBwAdvGauge, publishedMillis,
              advertisedBandwidth, fingerprint,
              nickname, node);

        } catch (Exception ex) {
          logger.warn("Exception. {}".format(ex.getMessage()));
        }

        // Add server descriptor to database
        this.addDescriptor(desc, conn);

      } else {
        logger.warn("Invalid descriptor. Continuining...");
      }
      opWriter.pushToGateway(registry);
    }


  }

  private void addDescriptor(ServerDescriptor desc, Connection conn) {
    DescriptorUtils descUtils = new DescriptorUtils();
    try (
      PreparedStatement preparedStatement =
          conn.prepareStatement(INSERT_SERVER_SQL);
    ) {
      preparedStatement.setBoolean(1, this.isBridge);
      preparedStatement.setTimestamp(2,
          new Timestamp(desc.getPublishedMillis()));
      preparedStatement.setString(3, desc.getNickname());
      preparedStatement.setString(4, desc.getFingerprint());
      preparedStatement.setString(5, desc.getDigestSha1Hex());
      preparedStatement.setString(6, desc.getIdentityEd25519());
      preparedStatement.setString(7, desc.getMasterKeyEd25519());
      preparedStatement.setString(8,
          descUtils.fieldAsString(desc.getAddress()));
      preparedStatement.setString(9,
          descUtils.fieldAsString(desc.getOrPort()));
      preparedStatement.setString(10,
          descUtils.fieldAsString(desc.getSocksPort()));
      preparedStatement.setString(11,
          descUtils.fieldAsString(desc.getDirPort()));
      preparedStatement.setString(12,
          StringUtils.join(desc.getOrAddresses(), ","));
      preparedStatement.setLong(13, desc.getBandwidthRate());
      preparedStatement.setLong(14, desc.getBandwidthBurst());
      preparedStatement.setLong(15, desc.getBandwidthObserved());
      preparedStatement.setString(16, desc.getPlatform());
      preparedStatement.setTimestamp(17,
          new Timestamp(desc.getOverloadGeneralTimestamp()));
      if (desc.getOverloadGeneralVersion() != -1L) {
        preparedStatement.setInt(18, (int)desc.getOverloadGeneralVersion());
      } else {
        preparedStatement.setInt(18, -1);
      }
      preparedStatement.setString(19,
          descUtils.fieldAsString(desc.getProtocols()));
      preparedStatement.setBoolean(20, desc.isHibernating());
      preparedStatement.setLong(21, desc.getUptime());
      preparedStatement.setString(22, desc.getOnionKey());
      preparedStatement.setString(23, desc.getSigningKey());
      preparedStatement.setString(24,
          descUtils.fieldAsString(desc.getExitPolicyLines()));
      preparedStatement.setString(25, desc.getContact());
      if (this.isBridge == true) {
        preparedStatement.setString(26, desc.getBridgeDistributionRequest());
      } else {
        preparedStatement.setString(26, null);
      }
      preparedStatement.setString(27,
          descUtils.fieldAsString(desc.getFamilyEntries()));
      preparedStatement.setString(28,
          descUtils.fieldAsString(desc.getReadHistory()));
      preparedStatement.setString(29,
          descUtils.fieldAsString(desc.getWriteHistory()));
      preparedStatement.setBoolean(30,
          desc.getUsesEnhancedDnsLogic());
      preparedStatement.setBoolean(31, desc.getCachesExtraInfo());
      preparedStatement.setString(32, desc.getExtraInfoDigestSha1Hex());
      preparedStatement.setString(33, desc.getExtraInfoDigestSha256Base64());
      preparedStatement.setBoolean(34, desc.isHiddenServiceDir());
      Array arrayLinkProtocolVersions =
          conn.createArrayOf("int",
          descUtils.listToArray(desc.getLinkProtocolVersions()));
      preparedStatement.setArray(35, arrayLinkProtocolVersions);
      Array arrayCircuitProtocolVersions =
          conn.createArrayOf("int",
          descUtils.listToArray(desc.getCircuitProtocolVersions()));
      preparedStatement.setArray(36, arrayCircuitProtocolVersions);
      preparedStatement.setBoolean(37, desc.getAllowSingleHopExits());
      preparedStatement.setString(38,
          descUtils.fieldAsString(desc.getIpv6DefaultPolicy()));
      preparedStatement.setString(39,
          descUtils.fieldAsString(desc.getIpv6PortList()));
      preparedStatement.setString(40, desc.getNtorOnionKey());
      preparedStatement.setString(41, desc.getOnionKeyCrosscert());
      preparedStatement.setString(42, desc.getNtorOnionKeyCrosscert());
      preparedStatement.setInt(43, desc.getNtorOnionKeyCrosscertSign());
      preparedStatement.setBoolean(44, desc.getTunnelledDirServer());
      preparedStatement.setString(45, desc.getRouterSignatureEd25519());
      preparedStatement.setString(46, desc.getRouterSignature());
      if (this.isBridge) {
        preparedStatement.setString(47, "@type bridge-server-descriptor 1.2");
      } else {
        preparedStatement.setString(47, "@type server-descriptor 1.0");
      }
      preparedStatement.setString(48, desc.getDigestSha256Base64());
      preparedStatement.executeUpdate();
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }

  }
}
