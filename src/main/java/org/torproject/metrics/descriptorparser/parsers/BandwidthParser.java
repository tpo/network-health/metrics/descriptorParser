package org.torproject.metrics.descriptorparser.parsers;

import org.torproject.descriptor.BandwidthFile;
import org.torproject.descriptor.Descriptor;
import org.torproject.descriptor.DescriptorReader;
import org.torproject.descriptor.DescriptorSourceFactory;
import org.torproject.metrics.descriptorparser.utils.DateTimeHelper;
import org.torproject.metrics.descriptorparser.utils.DescriptorUtils;
import org.torproject.metrics.descriptorparser.utils.Gauge;
import org.torproject.metrics.descriptorparser.utils.OpenMetricsWriter;

import io.prometheus.client.CollectorRegistry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;

public class BandwidthParser {
  private static final Logger logger = LoggerFactory.getLogger(
      BandwidthParser.class);

  private static final String INSERT_BANDWIDTH_FILE_SQL
      = "INSERT INTO"
      + " bandwidth_file (header, published, destination_countries,"
      + " earliest_bandwidth, file_created, generator_started,"
      + " latest_bandwidth, minimum_number_eligible_relays,"
      + " minimum_percent_eligible_relays, number_consensus_relays,"
      + " number_eligible_relays, percent_eligible_relays,"
      + " recent_consensus_count, recent_measurement_attempt_count,"
      + " recent_measurement_failure_count,"
      + " recent_measurements_excluded_error_count,"
      + " recent_measurements_excluded_few_count,"
      + " recent_measurements_excluded_near_count,"
      + " recent_measurements_excluded_old_count,"
      + " recent_priority_list_count, recent_priority_relay_count,"
      + " scanner_country, software, software_version, tor_version,"
      + " time_to_report_half_network, spec_version, digest, mu, muf,"
      + " dirauth_nickname)"
      + " VALUES "
      + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final String INSERT_BANDWIDTH_RECORD_SQL
      = "INSERT INTO"
      + " bandwidth_record (digest, bw, bw_mean, bw_median,"
      + " consensus_bandwidth,"
      + " consensus_bandwidth_is_unmeasured, desc_bw_avg, desc_bw_bur,"
      + " desc_bw_obs_last, desc_bw_obs_mean, error_circ, error_destination,"
      + " error_misc, error_second_relay,"
      + " error_stream, master_key_ed25519, nick,"
      + " node_id, rtt, relay_in_recent_consensus_count,"
      + " relay_recent_measurement_attempt_count,"
      + " relay_recent_measurements_excluded_error_count,"
      + " relay_recent_measurement_failure_count,"
      + " relay_recent_measurements_excluded_near_count,"
      + " relay_recent_measurements_excluded_old_count,"
      + " relay_recent_measurements_excluded_few_count,"
      + " relay_recent_priority_list_count, under_min_report, unmeasured, vote,"
      + " xoff_recv, xoff_sent, success, time,"
      + " r_strm, r_strm_filt, dirauth_nickname, scanner_country,"
      + " published, software_version)"
      + " VALUES "
      + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final String INSERT_BANDWIDTH_FILE_DOCUMENT_SQL
          = "INSERT INTO bandwidth_file_document"
          + " (bandwidth_file_digest, bandwidth_record_digest)"
          + " VALUES "
          + "(?, ?) ON CONFLICT DO NOTHING;";

  private CollectorRegistry registry = new CollectorRegistry();
  private OpenMetricsWriter opWriter = new OpenMetricsWriter();

  private Gauge bwGauge = Gauge.build()
      .name("bw")
      .help("The scaled bandwidth in kilobytes per second.")
      .labelNames("fingerprint", "nickname", "dirauth").register(registry);

  private Gauge bwMeanGauge = Gauge.build()
      .name("bw_mean")
      .help("The measured mean bandwidth in bytes per second.")
      .labelNames("fingerprint", "nickname", "dirauth").register(registry);

  private Gauge bwMedianGauge = Gauge.build()
      .name("bw_median")
      .help("The measured bandwidth median in bytes per second.")
      .labelNames("fingerprint", "nickname", "dirauth").register(registry);

  private Gauge consensusBwGauge = Gauge.build()
      .name("consensus_bandwidth")
      .help("The consensus bandwidth in bytes per second.")
      .labelNames("fingerprint", "nickname", "dirauth").register(registry);

  private Gauge descBwAvgGauge = Gauge.build()
      .name("bw_file_desc_bw_avg")
      .help("The descriptor average bandwidth in bytes per second.")
      .labelNames("fingerprint", "nickname", "dirauth").register(registry);

  private Gauge descBwBurGauge = Gauge.build()
      .name("bw_file_desc_bw_bur")
      .help("The descriptor observed bandwidth burst in bytes per second.")
      .labelNames("fingerprint", "nickname", "dirauth").register(registry);

  private Gauge descBwObsLastGauge = Gauge.build()
      .name("bw_file_desc_bw_obs_last")
      .help("The last descriptor observed bandwidth in bytes per second.")
      .labelNames("fingerprint", "nickname", "dirauth").register(registry);

  private Gauge descBwObsMeanGauge = Gauge.build()
      .name("bw_file_desc_bw_obs_mean")
      .help("The descriptor observed bandwidth mean in bytes per second.")
      .labelNames("fingerprint", "nickname", "dirauth").register(registry);

  private Gauge errorCircGauge = Gauge.build()
      .name("error_circ")
      .help("The number of times that the bandwidth measurements failed "
          + "because of circuit failures.")
      .labelNames("fingerprint", "nickname", "dirauth").register(registry);

  private Gauge errorDestinationGauge = Gauge.build()
      .name("error_destination")
      .help("The number of times that the measurement "
          + "failed because the destination web server was not available.")
      .labelNames("fingerprint", "nickname", "dirauth").register(registry);

  private Gauge errorMiscGauge = Gauge.build()
      .name("error_misc")
      .help("The number of times that the bandwidth measurements "
          + "failed because of other reasons.")
      .labelNames("fingerprint", "nickname", "dirauth").register(registry);

  private Gauge errorSecondRelayGauge = Gauge.build()
      .name("error_second_relay")
      .help("The number of times that the bandwidth measurements for this "
          + "relay failed because sbws could not find a second relay for the "
          + "test circuit.")
      .labelNames("fingerprint", "nickname", "dirauth").register(registry);

  private Gauge errorStreamGauge = Gauge.build()
      .name("error_stream")
      .help("The number of times that the bandwidth measurements "
          + "failed because of stream failures.")
      .labelNames("fingerprint", "nickname", "dirauth").register(registry);

  private Gauge ratioStreamGauge = Gauge.build()
      .name("r_strm")
      .help("The stream ratio.")
      .labelNames("fingerprint", "nickname", "dirauth").register(registry);

  private Gauge ratioStreamFilteredGauge = Gauge.build()
      .name("r_strm_filt")
      .help("The filtered stream ratio.")
      .labelNames("fingerprint", "nickname", "dirauth").register(registry);

  /**
   * Parse bandwidth files and add fields to the database.
   */
  public void run(String path, Connection conn) throws Exception {
    // Read descriptors from disk.
    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();
    for (Descriptor descriptor : descriptorReader.readDescriptors(
        new File(path))) {
      if (descriptor instanceof BandwidthFile) {
        BandwidthFile desc = (BandwidthFile) descriptor;

        this.addBandwidthFile(desc, conn);
        String digest = desc.digestSha256Base64();
        this.addRelayLines(desc, digest, conn);

      } else {
        continue;
      }
    }
  }

  private void addRelayLines(BandwidthFile desc,
      String digest, Connection conn) {
    DescriptorUtils descUtils = new DescriptorUtils();
    String dirauth = null;
    if (desc.dirauthNickname().isPresent()) {
      dirauth = desc.dirauthNickname().get();
    }
    try (
      PreparedStatement preparedStatement = conn.prepareStatement(
          INSERT_BANDWIDTH_RECORD_SQL);
      PreparedStatement preparedDocStatement = conn.prepareStatement(
          INSERT_BANDWIDTH_FILE_DOCUMENT_SQL);
    ) {
      for (BandwidthFile.RelayLine relayLine : desc.relayLines()) {
        String nodeId = relayLine.nodeId().get();
        // Check if the string starts with '$'
        if (nodeId.startsWith("$")) {
          // Remove the '$' from the start
          nodeId = nodeId.substring(1);
        }
        int bw = relayLine.bw();
        // Both nodeId and bw are required at the moment but master_key_ed25519
        // is optional, so let's check for its presence. Note: metrics-lib
        // treats nodeId optional as well but that's not the case in the current
        // bwfile spec nor in Tor's code in bwauth.c.
        String masterKey = null;
        if (relayLine.masterKeyEd25519().isPresent()) {
          masterKey = relayLine.masterKeyEd25519().get();
        }
        String line = nodeId
            + "\n" + masterKey
            + "\n" + descUtils.fieldAsString(relayLine.additionalKeyValues())
            + "\n" + String.format("%d", bw);
        String lineDigest = descUtils.calculateDigestSha256Base64(
                line.getBytes());
        preparedStatement.setString(1, lineDigest);
        preparedDocStatement.setString(1, digest);
        preparedDocStatement.setString(2, lineDigest);
        preparedDocStatement.addBatch();
        String nick = null;
        if (relayLine.additionalKeyValues().get("nick") != null) {
          nick = relayLine.additionalKeyValues().get("nick");
        }
        String relayTime = null;
        if (relayLine.additionalKeyValues().get("time") != null) {
          relayTime = relayLine.additionalKeyValues().get("time");
        } else {
          logger.warn("Time in bw record is null {} {}".format(nick, nodeId));
        }
        String dateFormat = "yyyy-MM-dd'T'HH:mm:ss";
        long rt = DateTimeHelper.parse(relayTime, dateFormat);
        if (rt == -1L) {
          logger.warn("Time in bw record is null {} {}".format(nick, nodeId));
        }
        opWriter.processMetrics(bwGauge, rt, bw, nodeId, nick, dirauth);
        preparedStatement.setInt(2, bw);
        long bwMean = -1L;
        if (relayLine.additionalKeyValues().get("bw_mean") != null) {
          bwMean = Long.parseLong(
              relayLine.additionalKeyValues().get("bw_mean"));
          opWriter.processMetrics(bwMeanGauge, rt, bwMean,
              nodeId, nick, dirauth);
        }
        preparedStatement.setLong(3, bwMean);
        long bwMedian = -1L;
        if (relayLine.additionalKeyValues().get("bw_median") != null) {
          bwMedian = Long.parseLong(
              relayLine.additionalKeyValues().get("bw_median"));
          opWriter.processMetrics(bwMedianGauge, rt, bwMedian,
              nodeId, nick, dirauth);
        }
        preparedStatement.setLong(4, bwMedian);
        long consensusBw = -1L;
        if (relayLine.additionalKeyValues()
            .get("consensus_bandwidth") != null) {
          consensusBw = Long.parseLong(
              relayLine.additionalKeyValues().get("consensus_bandwidth"));
          opWriter.processMetrics(consensusBwGauge, rt, consensusBw,
              nodeId, nick, dirauth);
        }
        preparedStatement.setLong(5, consensusBw);
        boolean consensusBwUnmeasured = false;
        if (relayLine.additionalKeyValues()
            .get("consensus_bandwidth_is_unmeasured") != null) {
          consensusBwUnmeasured = Boolean.parseBoolean(
              relayLine.additionalKeyValues()
              .get("consensus_bandwidth_is_unmeasured"));
        }
        preparedStatement.setBoolean(6, consensusBwUnmeasured);
        long descBwAvg = -1L;
        if (relayLine.additionalKeyValues().get("desc_bw_avg") != null) {
          descBwAvg = Long.parseLong(
              relayLine.additionalKeyValues().get("desc_bw_avg"));
          opWriter.processMetrics(descBwAvgGauge, rt, descBwAvg,
              nodeId, nick, dirauth);
        }
        preparedStatement.setLong(7, descBwAvg);
        long descBwBur = -1L;
        if (relayLine.additionalKeyValues().get("desc_bw_bur") != null) {
          descBwBur = Long.parseLong(
              relayLine.additionalKeyValues().get("desc_bw_bur"));
          opWriter.processMetrics(descBwBurGauge, rt, descBwBur,
              nodeId, nick, dirauth);
        }
        preparedStatement.setLong(8, descBwBur);
        long descBwObsLast = -1L;
        if (relayLine.additionalKeyValues().get("desc_bw_obs_last") != null) {
          descBwObsLast = Long.parseLong(
              relayLine.additionalKeyValues().get("desc_bw_obs_last"));
          opWriter.processMetrics(descBwObsLastGauge, rt, descBwObsLast,
              nodeId, nick, dirauth);
        }
        preparedStatement.setLong(9, descBwObsLast);
        long descBwObsMean = -1L;
        if (relayLine.additionalKeyValues().get("desc_bw_obs_mean") != null) {
          descBwObsMean = Long.parseLong(
              relayLine.additionalKeyValues().get("desc_bw_obs_mean"));
          opWriter.processMetrics(descBwObsMeanGauge, rt, descBwObsMean,
              nodeId, nick, dirauth);
        }
        preparedStatement.setLong(10, descBwObsMean);
        int errorCirc = 0;
        if (relayLine.additionalKeyValues().get("error_circ") != null) {
          errorCirc = Integer.parseInt(
              relayLine.additionalKeyValues().get("error_circ"));
          opWriter.processMetrics(errorCircGauge, rt, errorCirc,
              nodeId, nick, dirauth);
        }
        preparedStatement.setInt(11, errorCirc);
        int errorDestination = 0;
        if (relayLine.additionalKeyValues().get("error_destination") != null) {
          errorDestination = Integer.parseInt(
              relayLine.additionalKeyValues().get("error_destination"));
          opWriter.processMetrics(errorDestinationGauge, rt, errorDestination,
              nodeId, nick, dirauth);
        }
        preparedStatement.setInt(12, errorDestination);
        int errorMisc = 0;
        if (relayLine.additionalKeyValues().get("error_misc") != null) {
          errorMisc = Integer.parseInt(
              relayLine.additionalKeyValues().get("error_misc"));
          opWriter.processMetrics(errorMiscGauge, rt, errorMisc,
              nodeId, nick, dirauth);
        }
        preparedStatement.setInt(13, errorMisc);
        int errorNdRelay = 0;
        if (relayLine.additionalKeyValues().get("error_second_relay") != null) {
          errorNdRelay = Integer.parseInt(
              relayLine.additionalKeyValues().get("error_second_relay"));
          opWriter.processMetrics(errorSecondRelayGauge, rt, errorNdRelay,
              nodeId, nick, dirauth);
        }
        preparedStatement.setInt(14, errorNdRelay);
        int errorStream = 0;
        if (relayLine.additionalKeyValues().get("error_stream") != null) {
          errorStream = Integer.parseInt(
              relayLine.additionalKeyValues().get("error_stream"));
          opWriter.processMetrics(errorStreamGauge, rt, errorStream,
              nodeId, nick, dirauth);
        }
        preparedStatement.setInt(15, errorStream);
        preparedStatement.setString(16, masterKey);
        preparedStatement.setString(17, nick);
        preparedStatement.setString(18, nodeId);
        int rtt = 0;
        if (relayLine.additionalKeyValues().get("rtt") != null) {
          rtt = Integer.parseInt(relayLine.additionalKeyValues().get("rtt"));
        }
        preparedStatement.setInt(19, rtt);
        int relayConsensusCount = 0;
        if (relayLine.additionalKeyValues()
            .get("relay_in_recent_consensus_count") != null) {
          relayConsensusCount = Integer.parseInt(
              relayLine.additionalKeyValues()
              .get("relay_in_recent_consensus_count"));
        }
        preparedStatement.setInt(20, relayConsensusCount);
        int relayAttemptCount = 0;
        if (relayLine.additionalKeyValues()
            .get("relay_recent_measurement_attempt_count") != null) {
          relayAttemptCount = Integer.parseInt(
              relayLine.additionalKeyValues()
              .get("relay_recent_measurement_attempt_count"));
        }
        preparedStatement.setInt(21, relayAttemptCount);
        int relayExcludedCount = 0;
        if (relayLine.additionalKeyValues()
            .get("relay_recent_measurements_excluded_error_count") != null) {
          relayExcludedCount = Integer.parseInt(
              relayLine.additionalKeyValues()
              .get("relay_recent_measurements_excluded_error_count"));
        }
        preparedStatement.setInt(22, relayExcludedCount);
        int relayFailureCount = 0;
        if (relayLine.additionalKeyValues()
            .get("relay_recent_measurement_failure_count") != null) {
          relayFailureCount = Integer.parseInt(
              relayLine.additionalKeyValues()
              .get("relay_recent_measurement_failure_count"));
        }
        preparedStatement.setInt(23, relayFailureCount);
        int relayExcludedNearCount = 0;
        if (relayLine.additionalKeyValues()
            .get("relay_recent_measurements_excluded_near_count") != null) {
          relayExcludedNearCount = Integer.parseInt(
              relayLine.additionalKeyValues()
              .get("relay_recent_measurements_excluded_near_count"));
        }
        preparedStatement.setInt(24, relayExcludedNearCount);
        int relayExcludedOldCount = 0;
        if (relayLine.additionalKeyValues()
            .get("relay_recent_measurements_excluded_old_count") != null) {
          relayExcludedOldCount = Integer.parseInt(
              relayLine.additionalKeyValues()
              .get("relay_recent_measurements_excluded_old_count"));
        }
        preparedStatement.setInt(25, relayExcludedOldCount);
        int relayExcludedFewCount = 0;
        if (relayLine.additionalKeyValues()
            .get("relay_recent_measurements_excluded_few_count") != null) {
          relayExcludedFewCount = Integer.parseInt(
              relayLine.additionalKeyValues()
              .get("relay_recent_measurements_excluded_few_count"));
        }
        preparedStatement.setInt(26, relayExcludedFewCount);
        int relayPriorityCount = 0;
        if (relayLine.additionalKeyValues()
            .get("relay_recent_priority_list_count") != null) {
          relayPriorityCount = Integer.parseInt(
              relayLine.additionalKeyValues()
              .get("relay_recent_priority_list_count"));
        }
        preparedStatement.setInt(27, relayPriorityCount);
        boolean underMinReport = false;
        if (relayLine.additionalKeyValues().get("under_min_report") != null) {
          underMinReport = Boolean.parseBoolean(
              relayLine.additionalKeyValues()
              .get("under_min_report"));
        }
        preparedStatement.setBoolean(28, underMinReport);
        boolean unmeasured = false;
        if (relayLine.additionalKeyValues().get("unmeasured") != null) {
          unmeasured = Boolean.parseBoolean(
              relayLine.additionalKeyValues()
              .get("unmeasured"));
        }
        preparedStatement.setBoolean(29, unmeasured);
        boolean vote = true;
        if (relayLine.additionalKeyValues().get("vote") != null) {
          vote = Boolean.parseBoolean(relayLine.additionalKeyValues()
              .get("vote"));
        }
        preparedStatement.setBoolean(30, vote);
        int xoffRecv = 0;
        if (relayLine.additionalKeyValues().get("xoff_recv") != null) {
          xoffRecv = Integer.parseInt(
              relayLine.additionalKeyValues().get("xoff_recv"));
        }
        preparedStatement.setInt(31, xoffRecv);
        int xoffSent = 0;
        if (relayLine.additionalKeyValues().get("xoff_sent") != null) {
          xoffSent = Integer.parseInt(
              relayLine.additionalKeyValues().get("xoff_sent"));
        }
        preparedStatement.setInt(32, xoffSent);
        int success = 0;
        if (relayLine.additionalKeyValues().get("success") != null) {
          success = Integer.parseInt(
              relayLine.additionalKeyValues().get("success"));
        }
        preparedStatement.setInt(33, success);
        preparedStatement.setTimestamp(34, new Timestamp(rt));
        double ratioStream = 0.0;
        if (relayLine.additionalKeyValues().get("r_strm") != null) {
          ratioStream = Double.parseDouble(
              relayLine.additionalKeyValues().get("r_strm"));
          opWriter.processMetrics(ratioStreamGauge, rt, ratioStream,
              nodeId, nick, dirauth);
        }
        preparedStatement.setDouble(35, ratioStream);
        double ratioStreamFiltered = 0.0;
        if (relayLine.additionalKeyValues().get("r_strm_filt") != null) {
          ratioStreamFiltered = Double.parseDouble(
              relayLine.additionalKeyValues().get("r_strm_filt"));
          opWriter.processMetrics(ratioStreamFilteredGauge,
              rt, ratioStreamFiltered,
              nodeId, nick, dirauth);
        }
        preparedStatement.setDouble(36, ratioStreamFiltered);
        if (desc.dirauthNickname().isPresent()) {
          preparedStatement.setString(37, desc.dirauthNickname().get());
        } else {
          preparedStatement.setString(37, null);
        }
        if (desc.scannerCountry().isPresent()) {
          preparedStatement.setString(38, desc.scannerCountry().get());
        } else {
          preparedStatement.setString(38, null);
        }
        Timestamp timestamp = Timestamp.valueOf(desc.timestamp());
        preparedStatement.setTimestamp(39, timestamp);
        if (desc.softwareVersion().isPresent()) {
          preparedStatement.setString(40, desc.softwareVersion().get());
        } else {
          preparedStatement.setString(40, null);
        }
        preparedStatement.addBatch();
      }
      preparedStatement.executeBatch();
      preparedDocStatement.executeBatch();
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
    opWriter.pushToGateway(registry);
  }

  private void addBandwidthFile(BandwidthFile desc, Connection conn) {
    try (
      PreparedStatement preparedStatement = conn.prepareStatement(
          INSERT_BANDWIDTH_FILE_SQL);
    ) {
      preparedStatement.setString(1, "@type bandwidth-file 1.0");
      Timestamp timestamp = Timestamp.valueOf(desc.timestamp());
      preparedStatement.setTimestamp(2, timestamp);
      String destinationsCountries =
          String.join(", ", desc.destinationsCountries().get());
      preparedStatement.setString(3, destinationsCountries);
      Timestamp earliestBandwidth =
          Timestamp.valueOf(desc.earliestBandwidth().get());
      preparedStatement.setTimestamp(4, earliestBandwidth);
      Timestamp fileCreated =
          Timestamp.valueOf(desc.fileCreated().get());
      preparedStatement.setTimestamp(5, fileCreated);
      Timestamp generatorStarted =
          Timestamp.valueOf(desc.generatorStarted().get());
      preparedStatement.setTimestamp(6, generatorStarted);
      Timestamp latestBandwidth =
          Timestamp.valueOf(desc.latestBandwidth().get());
      preparedStatement.setTimestamp(7, latestBandwidth);
      preparedStatement.setInt(8, desc.minimumNumberEligibleRelays().get());
      preparedStatement.setInt(9,
          desc.minimumPercentEligibleRelays().get());
      preparedStatement.setInt(10, desc.numberConsensusRelays().get());
      preparedStatement.setInt(11, desc.numberEligibleRelays().get());
      preparedStatement.setInt(12, desc.percentEligibleRelays().get());
      preparedStatement.setInt(13, desc.recentConsensusCount().get());
      if (desc.recentMeasurementAttemptCount().isPresent()) {
        preparedStatement.setInt(14,
            desc.recentMeasurementAttemptCount().get());
      } else {
        preparedStatement.setInt(14, 0);
      }
      if (desc.recentMeasurementFailureCount().isPresent()) {
        preparedStatement.setInt(15,
            desc.recentMeasurementFailureCount().get());
      } else {
        preparedStatement.setInt(15, 0);
      }
      preparedStatement.setInt(16,
          desc.recentMeasurementsExcludedErrorCount().get());
      preparedStatement.setInt(17,
          desc.recentMeasurementsExcludedFewCount().get());
      preparedStatement.setInt(18,
          desc.recentMeasurementsExcludedNearCount().get());
      preparedStatement.setInt(19,
          desc.recentMeasurementsExcludedOldCount().get());
      preparedStatement.setInt(20,
          desc.recentPriorityListCount().get());
      preparedStatement.setInt(21,
          desc.recentPriorityRelayCount().get());
      if (desc.scannerCountry().isPresent()) {
        preparedStatement.setString(22, desc.scannerCountry().get());
      } else {
        preparedStatement.setString(22, null);
      }
      preparedStatement.setString(23, desc.software());
      if (desc.softwareVersion().isPresent()) {
        preparedStatement.setString(24, desc.softwareVersion().get());
      } else {
        preparedStatement.setString(24, null);
      }
      if (desc.torVersion().isPresent()) {
        preparedStatement.setString(25, desc.torVersion().get());
      } else {
        preparedStatement.setString(25, null);
      }
      preparedStatement.setLong(26,
          desc.timeToReportHalfNetwork().get().getSeconds());
      preparedStatement.setString(27, desc.version());
      preparedStatement.setString(28, desc.digestSha256Base64());
      preparedStatement.setInt(29, desc.mu().get());
      preparedStatement.setInt(30, desc.muf().get());
      if (desc.dirauthNickname().isPresent()) {
        preparedStatement.setString(31, desc.dirauthNickname().get());
      } else {
        preparedStatement.setString(31, null);
      }
      preparedStatement.executeUpdate();
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }

}
