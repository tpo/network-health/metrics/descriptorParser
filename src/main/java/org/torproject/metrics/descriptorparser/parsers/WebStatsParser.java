package org.torproject.metrics.descriptorparser.parsers;

import static java.util.stream.Collectors.toList;

import org.torproject.descriptor.Descriptor;
import org.torproject.descriptor.DescriptorReader;
import org.torproject.descriptor.DescriptorSourceFactory;
import org.torproject.descriptor.WebServerAccessLog;
import org.torproject.metrics.descriptorparser.utils.OpenMetricsWriter;

import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.Counter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;

public class WebStatsParser {

  private static final Logger logger = LoggerFactory.getLogger(
      WebStatsParser.class);

  private static OpenMetricsWriter opWriter = new OpenMetricsWriter();
  private static CollectorRegistry registry = new CollectorRegistry();

  private static Counter requestUrlCounter = Counter.build()
      .name("http_requests_total")
      .help("Total http requests")
      .labelNames("request", "method",
          "protocol", "virtual_host",
          "physical_host", "response").register(registry);

  /**
   * Read votes from disk and add data to the DB.
   */
  public void run(String path) throws Exception {

    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();

    for (Descriptor descriptor :
        descriptorReader.readDescriptors(new File(path))) {
      if (descriptor instanceof WebServerAccessLog) {
        WebServerAccessLog desc;
        desc = (WebServerAccessLog) descriptor;
        List<? extends WebServerAccessLog.Line> lines
            = desc.logLines().collect(toList());
        int index = 0;
        for (WebServerAccessLog.Line line: lines) {
          this.addLogLine(line, desc.getPhysicalHost(), desc.getVirtualHost());
        }

      } else {
        logger.warn("Invalid descriptor. Continuing...");
      }
      opWriter.pushToGateway(registry);
    }

  }

  private void addLogLine(WebServerAccessLog.Line line,
      String physicalHost, String virtualHost) {
    try {
      requestUrlCounter.labels(
          line.getRequest(),
          line.getMethod().toString(),
          line.getProtocol(),
          virtualHost,
          physicalHost,
          String.valueOf(line.getResponse())).inc();
    } catch (Exception ex) {
      logger.warn("Exception in addLogLine(). {}".format(ex.getMessage()));
    }
  }

}
