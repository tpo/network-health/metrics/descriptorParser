package org.torproject.metrics.descriptorparser.parsers;

import org.torproject.descriptor.BridgePoolAssignment;
import org.torproject.descriptor.Descriptor;
import org.torproject.descriptor.DescriptorReader;
import org.torproject.descriptor.DescriptorSourceFactory;
import org.torproject.metrics.descriptorparser.utils.DescriptorUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Map;

public class BridgePoolAssignmentsParser {
  private static final String INSERT_BRIDGE_POOL_ASSIGNMENTS_FILE_SQL
      = "INSERT INTO bridge_pool_assignments_file (published, header, digest)"
      + " VALUES (?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final String INSERT_BRIDGE_POOL_ASSIGNMENT_SQL
      = "INSERT INTO bridge_pool_assignment (published, digest, fingerprint,"
      + " distribution_method, transport, ip, blocklist,"
      + " bridge_pool_assignments, distributed, state,"
      + " bandwidth, ratio) VALUES"
      + " (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final Logger logger = LoggerFactory.getLogger(
      BridgePoolAssignmentsParser.class);

  /**
   * Parse bridge pool assignments and add fields to the database.
   */
  public void run(String path, Connection conn) throws Exception {
    DescriptorUtils descUtils = new DescriptorUtils();

    // Read descriptors from disk.
    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();

    for (Descriptor descriptor : descriptorReader.readDescriptors(
        new File(path))) {
      if (descriptor instanceof BridgePoolAssignment) {
        BridgePoolAssignment desc = (BridgePoolAssignment) descriptor;
        String digest = descUtils.calculateDigestSha256Base64(
            desc.getRawDescriptorBytes());

        this.addPoolAssignments(desc, digest, conn);

        try (
          PreparedStatement preparedStatement = conn.prepareStatement(
              INSERT_BRIDGE_POOL_ASSIGNMENT_SQL);
        ) {
          for (Map.Entry<String, String> e :
              desc.getEntries().entrySet()) {
            String fingerprint = e.getKey();
            String assignment = e.getValue();
            long timestamp = desc.getPublishedMillis();
            preparedStatement.setTimestamp(1,
                new Timestamp(timestamp));
            String entity = fingerprint + "," + assignment;
            preparedStatement.setString(2,
                descUtils.calculateDigestSha256Base64(
                entity.getBytes()));
            preparedStatement.setString(3, fingerprint);
            String bridgedbDistributor = assignment.split(" ")[0];
            preparedStatement.setString(4, bridgedbDistributor);
            String[] transportsParts = assignment.split("transport=");
            if (transportsParts != null && transportsParts.length > 1) {
              String transport = transportsParts[1].split(" ")[0];
              preparedStatement.setString(5, transport);
            } else {
              preparedStatement.setString(5, null);
            }
            String[] ipParts = assignment.split("ip=");
            if (ipParts != null && ipParts.length > 1) {
              String ip = ipParts[1].split(" ")[0];
              preparedStatement.setString(6, ip);
            } else {
              preparedStatement.setString(6, null);
            }
            String[] blocklistParts = assignment.split("blocklist=");
            if (blocklistParts != null && blocklistParts.length > 1) {
              String blocklist = blocklistParts[1].split(" ")[0];
              preparedStatement.setString(7, blocklist);
            } else {
              preparedStatement.setString(7, null);
            }
            preparedStatement.setString(8, digest);
            String[] distributedParts = assignment.split("distributed=");
            if (distributedParts != null && distributedParts.length > 1) {
              String distributed = distributedParts[1].split(" ")[0];
              preparedStatement.setBoolean(9,
                  Boolean.parseBoolean(distributed));
            } else {
              preparedStatement.setBoolean(9, false);

            }
            String[] stateParts = assignment.split("state=");
            if (stateParts != null && stateParts.length > 1) {
              String state = stateParts[1].split(" ")[0];
              preparedStatement.setString(10, state);
            } else {
              preparedStatement.setString(10, null);
            }
            String[] bwParts = assignment.split("bandwidth=");
            if (bwParts != null && bwParts.length > 1) {
              String bandwidth = bwParts[1].split(" ")[0];
              preparedStatement.setString(11, bandwidth);
            } else {
              preparedStatement.setString(11, null);
            }
            String[] ratioParts = assignment.split("ratio=");
            if (ratioParts != null && ratioParts.length > 1) {
              String ratio = ratioParts[1].split(" ")[0];
              preparedStatement.setFloat(12, Float.parseFloat(ratio));
            } else {
              preparedStatement.setFloat(12, Float.parseFloat("0.0"));
            }

            preparedStatement.addBatch();
          }
          preparedStatement.executeBatch();
        } catch (Exception ex) {
          logger.warn("Exception. {}".format(ex.getMessage()));
        }

      } else {
        continue;
      }
    }
  }

  private void addPoolAssignments(BridgePoolAssignment desc, String digest,
      Connection conn) {
    try (
      PreparedStatement preparedStatement = conn.prepareStatement(
          INSERT_BRIDGE_POOL_ASSIGNMENTS_FILE_SQL);
    ) {
      preparedStatement.setTimestamp(1,
          new Timestamp(desc.getPublishedMillis()));
      /*
       * XXX: We can hardcode the header for now, but it would be better to add
       * the parsing logic to metrics lib in case this changes.
       **/
      preparedStatement.setString(2, "@type bridge-pool-assignment 1.1");
      preparedStatement.setString(3, digest);
      preparedStatement.executeUpdate();
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }
}
