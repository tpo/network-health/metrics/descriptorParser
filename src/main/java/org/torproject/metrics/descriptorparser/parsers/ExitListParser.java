package org.torproject.metrics.descriptorparser.parsers;

import org.torproject.descriptor.Descriptor;
import org.torproject.descriptor.DescriptorReader;
import org.torproject.descriptor.DescriptorSourceFactory;
import org.torproject.descriptor.ExitList;
import org.torproject.metrics.descriptorparser.utils.DescriptorUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ExitListParser {

  private static final String INSERT_EXIT_LISTS_SQL
      = "INSERT INTO"
      + " exit_list (downloaded, header, digest)"
      + " VALUES (?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final String INSERT_EXIT_NODES_SQL
      = "INSERT INTO"
      + " exit_node (digest, published, last_status, fingerprint,"
      + " exit_addresses, exit_addresses_timestamps, exit_list)"
      + " VALUES (?, ?, ?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";


  private static final Logger logger = LoggerFactory.getLogger(
      ExitListParser.class);

  /**
   * Parse exit lists and add fields to the database.
   */
  public void run(String path, Connection conn) throws Exception {
    DescriptorUtils descUtils = new DescriptorUtils();
    // Read descriptors from disk.
    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();

    for (Descriptor descriptor : descriptorReader.readDescriptors(
        new File(path))) {
      if (descriptor instanceof ExitList) {
        ExitList desc = (ExitList) descriptor;
        String digest = descUtils.calculateDigestSha256Base64(
            desc.getRawDescriptorBytes());

        // add descriptor to DB
        this.addExitList(desc, digest, conn);
        try (
          PreparedStatement preparedStatement = conn.prepareStatement(
              INSERT_EXIT_NODES_SQL);
        ) {
          for (ExitList.Entry exitEntry : desc.getEntries()) {
            String digestLine = descUtils.fieldAsString(exitEntry);
            preparedStatement.setString(1,
                descUtils.calculateDigestSha256Base64(digestLine.getBytes()));
            preparedStatement.setTimestamp(2,
                new Timestamp(exitEntry.getPublishedMillis()));
            preparedStatement.setTimestamp(3,
                new Timestamp(exitEntry.getLastStatusMillis()));
            preparedStatement.setString(4, exitEntry.getFingerprint());
            List<String> addresses = new ArrayList<String>();
            List<Long> timestamps = new ArrayList<Long>();
            for (Map.Entry<String, Long> e :
                exitEntry.getExitAddresses().entrySet()) {
              String address = e.getKey();
              Long timestamp = e.getValue();
              addresses.add(address);
              timestamps.add(timestamp);
            }
            preparedStatement.setString(5, String.join(", ", addresses));
            preparedStatement.setString(6, String.join(", ",
                timestamps.toString()));
            preparedStatement.setString(7, digest);
            preparedStatement.addBatch();
          }
          preparedStatement.executeBatch();
        } catch (Exception ex) {
          logger.warn("Exception. {}".format(ex.getMessage()));
        }
      } else {
        continue;
      }
    }
  }

  private void addExitList(ExitList desc, String digest, Connection conn) {
    DescriptorUtils descUtils = new DescriptorUtils();

    try (
      PreparedStatement preparedStatement = conn.prepareStatement(
          INSERT_EXIT_LISTS_SQL);
    ) {
      preparedStatement.setTimestamp(1,
          new Timestamp(desc.getDownloadedMillis()));
      preparedStatement.setString(2, "@type tordnsel 1.0");
      preparedStatement.setString(3, digest);
      preparedStatement.executeUpdate();
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
      ex.printStackTrace();
    }
  }
}
