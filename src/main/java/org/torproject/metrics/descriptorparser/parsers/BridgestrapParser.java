package org.torproject.metrics.descriptorparser.parsers;

import org.torproject.descriptor.BridgestrapStats;
import org.torproject.descriptor.BridgestrapTestResult;
import org.torproject.descriptor.Descriptor;
import org.torproject.descriptor.DescriptorReader;
import org.torproject.descriptor.DescriptorSourceFactory;
import org.torproject.metrics.descriptorparser.utils.DescriptorUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.time.LocalDateTime;

public class BridgestrapParser {
  private static final String INSERT_BRIDGESTRAP_STATS_SQL
      = "INSERT INTO"
      + " bridgestrap_stats (bridgestrap_stats_end, interval, digest,"
      + " cached_requests, header)"
      + " VALUES (?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final String INSERT_BRIDGESTRAP_TEST_SQL
      = "INSERT INTO"
      + " bridgestrap_test (digest, published, result, fingerprint,"
      + " bridgestrap_stats)"
      + " VALUES (?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final Logger logger = LoggerFactory.getLogger(
      BridgestrapParser.class);

  /**
   * Parse bridgestrap statistics and add fields to the database.
   */
  public void run(String path, Connection conn) throws Exception {
    DescriptorUtils descUtils = new DescriptorUtils();
    // Read descriptors from disk.
    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();

    for (Descriptor descriptor : descriptorReader.readDescriptors(
        new File(path))) {
      if (descriptor instanceof BridgestrapStats) {
        BridgestrapStats desc = (BridgestrapStats) descriptor;
        String digest = descUtils.calculateDigestSha256Base64(
            desc.getRawDescriptorBytes());
        // add descriptor to DB
        this.addBridgestrapStats(desc, digest, conn);
        if (desc.bridgestrapTests().isPresent()) {
          try (
            PreparedStatement preparedStatement = conn.prepareStatement(
                INSERT_BRIDGESTRAP_TEST_SQL);
          ) {
            for (BridgestrapTestResult testResult :
                desc.bridgestrapTests().get()) {
              LocalDateTime bridgestrapStatsEnd = desc.bridgestrapStatsEnd();
              Timestamp timestamp = Timestamp.valueOf(bridgestrapStatsEnd);
              String digestLine = testResult.hashedFingerprint().get() + ","
                      + descUtils.fieldAsString(testResult);
              preparedStatement.setString(1,
                  descUtils.calculateDigestSha256Base64(digestLine.getBytes()));
              preparedStatement.setTimestamp(2, timestamp);
              preparedStatement.setBoolean(3, testResult.isReachable());
              preparedStatement.setString(4,
                  testResult.hashedFingerprint().get());
              preparedStatement.setString(5, digest);
              preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
          } catch (Exception ex) {
            logger.warn("Exception. {}".format(ex.getMessage()));
          }
        }
      } else {
        continue;
      }
    }
  }

  private void addBridgestrapStats(BridgestrapStats desc, String digest,
      Connection conn) {
    try (
      PreparedStatement preparedStatement = conn.prepareStatement(
          INSERT_BRIDGESTRAP_STATS_SQL);
    ) {
      Timestamp timestamp = Timestamp.valueOf(desc.bridgestrapStatsEnd());
      preparedStatement.setTimestamp(1, timestamp);
      preparedStatement.setLong(2,
          desc.bridgestrapStatsIntervalLength().getSeconds());
      preparedStatement.setString(3, digest);
      preparedStatement.setInt(4, desc.bridgestrapCachedRequests());
      preparedStatement.setString(5, "@type bridgestrap-stats 1.0");
      preparedStatement.executeUpdate();
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }
}
