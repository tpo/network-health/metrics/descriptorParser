DescriptorParser is a small standalone java app to import Tor network descriptors
into a postgresql DB and a VictoriaMetrics time series DB.

It assumes the same DB schema as defined in [src/main/sql](https://gitlab.torproject.org/tpo/network-health/metrics/metrics-sql-tables)


## Installation

In Debian bookworkm, you can install java 17 from adoptium:

```
wget -O - https://packages.adoptium.net/artifactory/api/gpg/key/public | tee /usr/share/keyrings/adoptium.asc
echo "deb [signed-by=/usr/share/keyrings/adoptium.asc] https://packages.adoptium.net/artifactory/deb $(awk -F= '/^VERSION_CODENAME/{print$2}' /etc/os-release) main" | tee /etc/apt/sources.list.d/adoptium.list
apt-get update
apt-get install -y temurin-17-jdk ant ivy
```

Clone the sources and submodules:

```
git clone --recursive https://gitlab.torproject.org/tpo/network-health/metrics/descriptorParser

```

If you already cloned the repo, you can update the submodulse running:

```
git submodule init
git submodule update
```

To build the project and install the rest of the dependencies:

```
cd descriptorParser
mkdir lib
ant resolve  # (to fetch dependencies)
ant fetch-metrics-lib  # (to fetch metrics library)
ant jar # (to build the project jar)
```

## Running

To run the tests and parse descriptors, you also need postgres.

In Debian bookworm, you can install postgres client:
```
apt -y install postgresql postgresql-client
```

And then run postgres and VictoriaMetrics from docker:

```
./src/test/resources/bin/runDB
./src/test/resources/bin/createDB
```

Depending
on your setup you might have to sudo in order to run the docker daemon.

You might have to run the above script everytime you run the tests.

To run the tests locally, you might need to change `src/test/resources/config.properties.test` and the
previous 2 scripts to use `localhost` instead of `postgresdb` as host.

You can now run the tests:

```
ant test
```

To run the app and connect to the database you'll need to copy `config.properties.example` to `config.properties` and change the postgres credentials.

You can now launch ``bin/run`` script, which runs the jar for you.

The app expects descriptors to be place in the following folders under the
project home. Ex:
- descriptors/relay-descriptors/extra-info
- descriptors/relay-descriptors/server
- logs

If you don't have `descriptors` directories, then it will attemp to download
the descriptors to `/srv/parser.torproject.org/parser/`

Descriptors can be individual text files as downloaded from:
- https://collector.torproject.org/recent/

Or archive tarballs that don't have to be extracted before using (the parser
will just do the heavy lifting for you).

DescriptorParser is still extremely hackish, use at your own risk! xD

## Developing

There's this [Java guide common to all metrics projects](https://gitlab.torproject.org/tpo/network-health/team/-/wikis/metrics/development/Java)

If you would like to run the CI in your TPI Gitlab clone, you would need to add
the following variables to your cloned project Settings -> CI/CD:

```bash
POSTGRES_DB
POSTGRES_PASSWORD
POSTGRES_USER
PROJECT_ACCESS_TOKEN
```

(An alternative would be to add these environment variables to the CI, since
the CI just needs a DB for testing)
